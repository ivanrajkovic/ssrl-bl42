#import bitstring
import binascii
import os
import numpy
from matplotlib import pyplot as plt

os.chdir('c:/Users/rajkovic/Documents/misc/camtest')

def hex2bin(chain):
    return ''.join((bin(int(chain[i:i+2], 16))[2:].zfill(8) for i in range(0, len(chain), 2)))

with open('12bitpacked1.bin', 'rb') as f:
    data = f.read()


ii=numpy.empty(2*len(data)/3)
ic = 0


for oo in range(0,len(data)/3):
    #aa = bitstring.Bits(bytes=data[3*oo:3*oo+3], length=24)
    #ii[ic] = int(aa.bin[0:8],2)*16+int(aa.bin[12:16],2)
    #ii[ic+1] = int(aa.bin[16:24],2)*16+int(aa.bin[8:12],2)
    bb = hex2bin(binascii.b2a_hex(data[3*oo:3*oo+3]))
    ii[ic] = int(bb[0:8],2)*16+int(bb[12:16],2)
    ii[ic+1] = int(bb[16:24],2)*16+int(bb[8:12],2)
    ic=ic+2


b = numpy.reshape(ii,(484,644))

plt.imshow(b,cmap='gray')
plt.show()