import os
from pymba import *
import numpy as np
import time
import warnings
import binascii

from skimage import io, img_as_uint
io.use_plugin('freeimage')


################################
##### Change these variables ###
################################


base_folder = 'c:\\users\\b_tsuruta.ssrlad\\Desktop\\camera\\20180326'
#base_folder = 'r:'

img_folder = 'run06_with_PF'

file_name = 'ML_slits_open'

total_frames = 2300   # max 2300 for Mono8

exp_time = 250 # in microseconds

cam_gain=0

px_format = 'Mono8'  # Mono8, Mono12 or Mono12Packed



############################################
###Don't change below this line ############
############################################


if px_format == 'Mono12':
    img_type = np.uint16
else:
    img_type = np.uint8

dig = len(str(total_frames))    

try:
    os.stat(os.path.join(base_folder,img_folder))
except:
    os.mkdir(os.path.join(base_folder,img_folder)) 

ts = []


with Vimba() as vimba:
    system = vimba.getSystem()

    system.runFeatureCommand("GeVDiscoveryAllOnce")
    time.sleep(0.2)

    camera_ids = vimba.getCameraIds()

    for cam_id in camera_ids:
        print "Camera found: ", cam_id
        
    c0 = vimba.getCamera(camera_ids[0])
    c0.openCamera()

    #set packet size and network speed
    c0.GVSPPacketSize = 9000
    c0.StreamBytesPerSecond = 124000000 #(bytes per second)
    
    #c0.AcquisitionMode='Continuous'
    c0.AcquisitionMode='SingleFrame'
    #c0.AcquisitionMode='MultiFrame'
    #c0.AcquisitionFrameCount=total_frames
    
    #set pixel format and max frame rate
    c0.PixelFormat=px_format
    c0.AcquisitionFrameRateAbs=c0.AcquisitionFrameRateLimit
    #print(c0.AcquisitionFrameRateLimit)
    c0.Gain=cam_gain
    c0.ReverseY='True'
    
    # set exposure time
    c0.ExposureTimeAbs=exp_time
    c0.TriggerSelector='AcquisitionStart'
    c0.TriggerSource='Line1'
    c0.TriggerMode='Off'
    
    
    frame = c0.getFrame()
    if px_format == 'Mono12Packed':
        f_h=int(frame.height*frame.width*1.5)
        f_w=1
    else:
        f_h=frame.height
        f_w=frame.width
    
    aaa=np.zeros([total_frames,f_h,f_w],dtype=img_type)
    frame.announceFrame()
    c0.startCapture()
    
    bbb = []
    t=time.time()
    
    for ff in xrange(total_frames):
        frame.queueFrameCapture()
        ts.append(time.clock())
        c0.runFeatureCommand("AcquisitionStart")
        frame.waitFrameCapture()
        hhh = frame.getBufferByteData()
        aaa[ff,:,:]=np.ndarray(buffer=hhh, dtype=img_type, shape=(f_h,f_w))
    ts.append(time.clock())

    print(time.time()-t)
    
    
    c0.endCapture()
    c0.revokeAllFrames()
    
    # settings for Vimba Viewer
    try:
        c0.AcquisitionMode='Continuous'
        c0.Gain=0
        c0.ExposureTimeAbs=250
        c0.PixelFormat='Mono8'
        c0.TriggerSelector='FrameStart'
        c0.TriggerSource='Freerun'
    except:
        print('Settings not changed back.')
    
    c0.closeCamera()

def hex2bin(chain):
    return ''.join((bin(int(chain[i:i+2], 16))[2:].zfill(8) for i in range(0, len(chain), 2)))

qq = open(os.path.join(base_folder,img_folder,file_name+'timing.txt'),'w')
qq.write('exposure: {}us\t\tgain: {}\n'.format(exp_time,cam_gain))
qq.write('img\trelative time\ttime delta\timage name\n')
for ff in xrange(total_frames):
    if px_format == 'Mono12Packed':
        #data = np.ndarray(buffer=bbb[ff], dtype=np.uint8, shape=(3*frame.height*frame.width/2))
        data=aaa[ff].flatten()
        ii=np.empty(2*len(data)/3)
        ic = 0
        for oo in range(0,len(data)/3):
            bb = hex2bin(binascii.b2a_hex(data[3*oo:3*oo+3]))
            ii[ic] = int(bb[0:8],2)*16+int(bb[12:16],2)
            ii[ic+1] = int(bb[16:24],2)*16+int(bb[8:12],2)
            ic=ic+2
        img = (np.reshape(ii.astype(np.uint16),(frame.height,frame.width)))
    elif px_format == 'Mono12':
        img=(aaa[ff].astype(np.uint16))
    else:
        #img = np.ndarray(buffer=bbb[ff], dtype=img_type, shape=(frame.height,frame.width))
        img=(aaa[ff].astype(np.uint8))
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        io.imsave(os.path.join(base_folder,img_folder,file_name+str(ff+1).zfill(dig)+'.tif'), img)
    
    
    qq.write('{:d}'.format(ff)+'\t'+'{:f}'.format(ts[ff]-ts[0])+'\t'+'{:f}'.format(ts[ff+1]-ts[ff])+'\t'+file_name+str(ff+1).zfill(dig)+'.tif'+'\n')
   
    #qq.write(str(ff)+'\t'+str(ts[ff]-ts[0])+'\t'+str(ts[ff]-ts[max(0,ff-1)])+'\t'+file_name+str(ff+1).zfill(dig)+'.tif'+'\n')
    
    #with warnings.catch_warnings():
    #    warnings.simplefilter("ignore")
    #    io.imsave(os.path.join(base_folder,img_folder,file_name+str(ff+1).zfill(dig)+'.tif'), img)
    
qq.close()
del aaa