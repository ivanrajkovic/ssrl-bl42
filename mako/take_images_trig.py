import os
from pymba import *
import numpy as np
import time
import warnings
import binascii

from skimage import io, img_as_uint
io.use_plugin('freeimage')


################################
##### Change these variables ###
################################



base_folder = 'c:\\users\\b_tsuruta.ssrlad\\Desktop\\camera\\20180208'
#base_folder = 'r:\\'

img_folder = 'mono_vert_0p4_upper_peak'
#img_folder='test'

file_name = 'img_'
total_frames = 1080

exp_time = 250 # in microseconds

px_format = 'Mono8'  # Mono8 or Mono12 (Mono12Packed doesn't work)

cam_gain=15

############################################
###Don't change below this line ############
############################################


if px_format == 'Mono12':
    img_type = np.uint16
else:
    img_type = np.uint8

dig = len(str(total_frames))    

try:
    os.stat(os.path.join(base_folder,img_folder))
except:
    os.mkdir(os.path.join(base_folder,img_folder)) 

ts = []


with Vimba() as vimba:
    system = vimba.getSystem()

    system.runFeatureCommand("GeVDiscoveryAllOnce")
    time.sleep(0.2)

    camera_ids = vimba.getCameraIds()

    for cam_id in camera_ids:
        print "Camera found: ", cam_id
        
    c0 = vimba.getCamera(camera_ids[0])
    c0.openCamera()

    #set packet size and network speed
    c0.GVSPPacketSize = 1500
    c0.StreamBytesPerSecond = 124000000 #(bytes per second)
    
    #c0.AcquisitionMode='Continuous'
    c0.AcquisitionMode='SingleFrame'
    #c0.AcquisitionMode='MultiFrame'
    #c0.AcquisitionFrameCount=total_frames
    
    
    #set pixel format
    c0.PixelFormat=px_format
    c0.Gain=cam_gain
    c0.AcquisitionFrameRateAbs=c0.AcquisitionFrameRateLimit
    c0.ReverseY='True'
    
    # set exposure time
    c0.ExposureTimeAbs=exp_time
    c0.TriggerSelector='FrameStart'
    #c0.TriggerSelector='AcquisitionStart'
    c0.TriggerSource='Line1'
    c0.TriggerMode='On'
    c0.TriggerActivation='RisingEdge'
    #c0.TriggerActivation='AnyEdge'
    
    frame = c0.getFrame()
    frame.announceFrame()
    c0.startCapture()
    
    aaa=np.zeros([frame.height,frame.width])
    bbb = []
    t=time.time()
    ff=0
    # There is one image in the buffer already, just read it out
    frame.queueFrameCapture()
    c0.runFeatureCommand("AcquisitionStart")
    frame.waitFrameCapture()
    hhh = frame.getBufferByteData()
    
    while ff<total_frames:
        #print('waiting')
        #bbb.append(frame.getBufferByteData())
        #bbb.append(frame.getImage())
        #img = frame.getImage()
        #io.imsave(os.path.join(base_folder,img_folder,file_name+str(ff+1).zfill(dig)+'.tif'), img)
        #aaa[ff,:,:]=(frame.getImage())
        try:
            frame.queueFrameCapture()
            c0.runFeatureCommand("AcquisitionStart")
            frame.waitFrameCapture()
            hhh = frame.getBufferByteData()
            ts.append(time.clock())
            aaa[:,:]=np.ndarray(buffer=hhh, dtype=img_type, shape=(frame.height,frame.width))
            if px_format == 'Mono12':
                img=(aaa.astype(np.uint16))
            else:
                img=(aaa.astype(np.uint8))

            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                io.imsave(os.path.join(base_folder,img_folder,file_name+str(ff+1).zfill(dig)+'.tif'), img)
                
            ff=ff+1
            aaa=aaa*0
            print('trigger\t\t'+str(ff)+'\t\t '+str((time.clock()-ts[max(-2,-1*ff)])/60))
        except KeyboardInterrupt:
            ff=total_frames
        except:
            pass
    ts.append(time.clock())

    print(time.time()-t)
    c0.endCapture()
    c0.revokeAllFrames()
    
    # settings for Vimba Viewer
    try:
        c0.AcquisitionMode='Continuous'
        c0.Gain=0
        c0.ExposureTimeAbs=250
        c0.PixelFormat='Mono8'
        c0.TriggerSelector='FrameStart'
        c0.TriggerSource='Freerun'
    except:
        print('Settings not changed back.')
    
    c0.closeCamera()

#def hex2bin(chain):
#    return ''.join((bin(int(chain[i:i+2], 16))[2:].zfill(8) for i in range(0, len(chain), 2)))

qq = open(os.path.join(base_folder,img_folder,file_name+'timing.txt'),'w')
qq.write('exposure: {}us\t\tgain: {}\n'.format(exp_time,cam_gain))
qq.write('img\trelative time\ttime delta\timage name\n')
#for ff in xrange(total_frames):
    # if px_format == 'Mono12Packed':
    #     #data = np.ndarray(buffer=bbb[ff], dtype=np.uint8, shape=(3*frame.height*frame.width/2))
    #     data=aaa[ff].flatten()
    #     ii=np.empty(2*len(data)/3)
    #     ic = 0
    #     for oo in range(0,len(data)/3):
    #         bb = hex2bin(binascii.b2a_hex(data[3*oo:3*oo+3]))
    #         ii[ic] = int(bb[0:8],2)*16+int(bb[12:16],2)
    #         ii[ic+1] = int(bb[16:24],2)*16+int(bb[8:12],2)
    #         ic=ic+2
    #     img = (np.reshape(ii.astype(np.uint16),(484,644)))
    # elif px_format == 'Mono12':
    #     img=(aaa[ff].astype(np.uint16))
    # else:
    #     #img = np.ndarray(buffer=bbb[ff], dtype=img_type, shape=(frame.height,frame.width))
    #     img=(aaa[ff].astype(np.uint8))
    # io.imsave(os.path.join(base_folder,img_folder,file_name+str(ff+1).zfill(dig)+'.tif'), img)
    
for ff in range(len(ts)-1):
    qq.write('{:d}'.format(ff+1)+'\t'+'{:f}'.format(ts[ff]-ts[0])+'\t'+'{:f}'.format(ts[ff+1]-ts[ff])+'\t'+file_name+str(ff+1).zfill(dig)+'.tif'+'\n')
   
    #qq.write(str(ff)+'\t'+str(ts[ff]-ts[0])+'\t'+str(ts[ff]-ts[max(0,ff-1)])+'\t'+file_name+str(ff+1).zfill(dig)+'.tif'+'\n')
    
    #with warnings.catch_warnings():
    #    warnings.simplefilter("ignore")
    #    io.imsave(os.path.join(base_folder,img_folder,file_name+str(ff+1).zfill(dig)+'.tif'), img)
    
qq.close()