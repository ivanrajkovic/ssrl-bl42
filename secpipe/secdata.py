#!/usr/bin/env python
# 
# secdata.py <path to first file> <num_buf> [<shut_cl_start> <shut_cl_end>]


'''
@author: rajkovic
'''

from __future__ import division

import sys
import os
import re
import shutil
import time
import distutils.spawn
from distutils.dir_util import mkpath
import subprocess
import glob
import numpy
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from heapq import nlargest
import fileinput


if __name__ == "__main__":
    
    if not (len(sys.argv)==5 or len(sys.argv)==3):
         
        sys.exit("""\n\n\tHow-to:
        
        {0} <path to the first tif file> <number of buffers> [<shut_cl_start> <shut_cl_end>]
        
        """.format(os.path.basename(sys.argv[0])))
        
    # set paths
    atsas_path='/mnt/home/staff/SW/ATSAS'
    exe_path='/mnt/home/sw/bin:{}'.format(os.path.join(atsas_path,'bin'))
    ld_path=os.path.join(atsas_path,'lib64/atsas')
    
    try:
        os.environ['LD_LIBRARY_PATH']=os.environ['LD_LIBRARY_PATH']+':'+ld_path
    except:
        os.environ['LD_LIBRARY_PATH']=ld_path
    
    try:
        os.environ['PATH']=os.environ['PATH']+':'+exe_path
    except:
        os.environ['PATH']=exe_path
        
    # set some variables from arguments 
    first_sample_path = sys.argv[1]
    num_buf = int(sys.argv[2])
    
    first_sample = os.path.basename(first_sample_path)
    data_fol = os.path.abspath(os.path.dirname(first_sample_path))
    sample_name_ser = '_'.join(first_sample.split('_')[:-2])
    sample_name = "_".join(sample_name_ser.split('_')[:-1])
    sample_ser = sample_name_ser.split('_')[-1]
    
    # prepare folders
    #analysis_fol = os.path.abspath(os.path.join(data_fol,'../analysis/'))
    # use current folder as analysis folder
    analysis_fol = os.path.abspath('.')
    sample_sfol = os.path.join(analysis_fol,sample_name_ser)
    sastool_sfol = os.path.join(sample_sfol,'sastool')
    plots_sfol = os.path.join(sample_sfol,'plots')
    avg_sfol = os.path.join(sample_sfol,'average')
    
    mkpath(analysis_fol)
    mkpath(sastool_sfol)
    mkpath(plots_sfol)
    mkpath(avg_sfol)
    
    files_list = sorted(glob.glob(os.path.join(data_fol,'{}*.tif'.format(sample_name_ser))))
    # find the last numer of the series
    end_num=int(files_list[-1].split('.')[0].split('_')[-1])
    if len(sys.argv) == 5:
        shut_cl_start = int(sys.argv[3])
        shut_cl_end = int(sys.argv[4])
    else:
        shut_cl_start = end_num+1
        shut_cl_end = end_num+2
        
    buf_name = os.path.join(data_fol,first_sample)
    # number of digits in the file number
    num_length = len(buf_name.split('_')[-1].split('.')[0])
    # first file after the buffer
    after_buffer = '_'.join(buf_name.split('_')[:-1])+'_{}.tif'.format(str(num_buf+1).zfill(num_length))
    # location of the integ.mpp
    integ_mpp = os.path.join(analysis_fol,'integ.mpp')
    
    # set sastool and autorg paths
    autorg = distutils.spawn.find_executable('autorg')
    sastool = distutils.spawn.find_executable('sastool')
    
    if autorg == None:
        sys.exit('\'autorg\' not found, please add it to the path.')
    if sastool == None:
        sys.exit('\'sastool\' not found, please add it to the path.')
        
    spmpp = os.path.join(sastool_sfol,'secdata.mpp')
    
    # prepare secpipe.mpp
    spmpp_text=[]
    t_switch = 0
    
    os.chdir(analysis_fol)
    with open(integ_mpp, "r") as impp:
        for line in impp:
            if not line.startswith('#') and not line.startswith('-f') and not re.match(r'^\s*$', line):
                if line.startswith('-m'):
                    line='-m yes {}'.format(os.path.abspath(line.split()[-1]))
                if not line.endswith('\n'):
                    line=line+'\n'
                spmpp_text.append(line)

    # Don't add -t line if not present
    #         if line.startswith('-t'):
    #             t_switch = 1
    # 
    # # append '-t yes' line if missing in integ.mpp
    # if t_switch==0:
    #     spmpp_text.append('-t yes\n')
    
    os.chdir(sastool_sfol)
    
    # analyze the data
    
    with open(spmpp, 'w') as mpp_file:
        for item in spmpp_text:
            mpp_file.write("{}".format(item))
        mpp_file.write('-f {} {} {}'.format(after_buffer, buf_name, num_buf))
    
    # run sastool
    subprocess.call([sastool,spmpp])
    
    # empty arrays, used for plotting later
    im_num_ar = []
    rg_ar = []
    i0_ar = []
    iqmin_ar = []
    
    for ii in numpy.arange(num_buf+1,end_num+1):
        #delete zeros from dat file:
        dat_file = os.path.join(sastool_sfol,sample_name_ser+"_0_"+str(int(ii)).zfill(num_length)+".dat")
        for line in fileinput.input(dat_file, inplace = 1):
            if (not re.match('.* 0 0',line) or fileinput.filelineno() > 30):
                sys.stdout.write(line)
        
        # run autorg
        try:
            #rg_out = subprocess.check_output([autorg,'--sminrg=1','--smaxrg=1.3',dat_file])
            rg_out = subprocess.check_output([autorg,dat_file])
            rg=float(rg_out.split()[2])
            #rg_stdev=rg_out.split()[4])
            i0=float(rg_out.split()[8])
            #i0_stdev=rg_out.split()[10]
        except:
            rg = 0
            i0 = 0
        
        if rg<0:
            rg = 0
        if i0<0:
            i0=0
        
        # read i_qmin value
        dat_d = numpy.genfromtxt(dat_file)
        iqmin = max(dat_d[5,1],0)

        # put the values in arrayes for printing later
        im_num_ar.append(ii)
        rg_ar.append(rg)
        i0_ar.append(i0)
        iqmin_ar.append(iqmin)
    
    # save data
    numpy.savetxt(os.path.join(plots_sfol,'{}_plot_data.txt'.format(sample_name)),numpy.transpose([im_num_ar,rg_ar,i0_ar,iqmin_ar]),fmt='%.8G',delimiter='\t',header='im.num. \tRg \tI0 \tIqmin')
    # read UV data, if exists
    uv_bip = os.path.join(data_fol,sample_name_ser+'uv.bip')
    
    try:
        uv_data = numpy.genfromtxt(uv_bip,skip_header=27)
        numpy.savetxt(os.path.join(plots_sfol,'{}_UV_data.txt'.format(sample_name)),uv_data,fmt='%.8G',delimiter='\t',header='im.num. \tUV1 \tUV2')
    except:
        print('No UV data')
        uv_data=[]
        
    # make graphs
    
    # I0 and Iqmin graph
    print('Making plots')
    plt.figure(num=None)
    plt.title('{}    S{}'.format(sample_name,sample_ser),fontsize=22)
    plt.grid(axis='both')
    plt.plot(im_num_ar,i0_ar,'o',label='I$_0$')
    plt.plot(im_num_ar,iqmin_ar,'o',label='I$_{qmin}$')
    plt.xlabel('Image number',fontsize=18)
    plt.ylabel('I$_0$, I$_{qmin}$',fontsize=18)
    yy_max=nlargest(10,i0_ar)[-1]
    plt.ylim(0,yy_max*1.2)
    try:
        plt.yticks(numpy.arange(0,yy_max*1.2,1.2*yy_max/6)," ")
    except:
        pass
    if shut_cl_end < end_num:
        plt.xlim(xmin=shut_cl_end-20)
    plt.xlim(xmax=end_num+7)
    plt.legend(numpoints=1,loc=2,fontsize=14, frameon=False, framealpha=0.5,labelspacing = 0.1)
    plt.savefig(os.path.join(plots_sfol,'i0_iqmin_{}.png'.format(sample_name)))
    #plt.savefig(os.path.join(plots_sfol,'i0_iqmin_{}.pdf'.format(sample_name)))
    # try to make zoomed-in graphs
    peak_pos=numpy.argmax(iqmin_ar)
    try:
        plt.xlim(im_num_ar[peak_pos]-70,im_num_ar[peak_pos]+70)
        plt.savefig(os.path.join(plots_sfol,'zoom1_i0_iqmin_{}.png'.format(sample_name)))
    except:
        pass
    #plt.savefig(os.path.join(plots_sfol,'zoom1_i0_iqmin_{}.pdf'.format(sample_name)))
    # zoom-in part two
    try:
        xxmin = im_num_ar[numpy.where(numpy.array(i0_ar)>(0.2*max(i0_ar)))[0][0]]
        xxmax = im_num_ar[len(i0_ar)-1-numpy.where(numpy.array(i0_ar)[::-1]>(0.2*max(i0_ar)))[0][0]]
        plt.xlim(xxmin-20,xxmax+10)
        plt.savefig(os.path.join(plots_sfol,'zoom2_i0_iqmin_{}.png'.format(sample_name)))
    except:
        pass
    #plt.savefig(os.path.join(plots_sfol,'zoom2_i0_iqmin_{}.pdf'.format(sample_name)))
    
    # Rg graph
    
    plt.figure(num=None)
    plt.title('{}    S{}'.format(sample_name,sample_ser),fontsize=22)
    plt.grid(axis='both')
    plt.plot(im_num_ar,rg_ar,'ro',label='R$_g$')
    plt.xlabel('Image number',fontsize=18)
    plt.ylabel('R$_g$ [$\AA$]',fontsize=18)
    plt.ylim(0,1.4*nlargest(10,rg_ar)[-1])
    if shut_cl_end < end_num:
        plt.xlim(xmin=shut_cl_end-20)
    plt.xlim(xmax=end_num+7)
    plt.legend(numpoints=1,loc=2, fontsize=14, frameon=False, framealpha=0.5,labelspacing = 0.1)
    plt.savefig(os.path.join(plots_sfol,'rg_{}.png'.format(sample_name)))
    #plt.savefig(os.path.join(plots_sfol,'rg_{}.pdf'.format(sample_name)))
    # try to make zoomed-in graphs
    try:
        plt.xlim(im_num_ar[peak_pos]-70,im_num_ar[peak_pos]+70)
        plt.savefig(os.path.join(plots_sfol,'zoom1_rg_{}.png'.format(sample_name)))
    except:
        pass
    #plt.savefig(os.path.join(plots_sfol,'zoom1_rg_{}.pdf'.format(sample_name)))
    # zoom-in part two
    try:
        xxmin = im_num_ar[numpy.where(numpy.array(i0_ar)>(0.2*max(i0_ar)))[0][0]]
        xxmax = im_num_ar[len(i0_ar)-1-numpy.where(numpy.array(i0_ar)[::-1]>(0.2*max(i0_ar)))[0][0]]
        plt.xlim(xxmin-20,xxmax+10)
        plt.savefig(os.path.join(plots_sfol,'zoom2_rg_{}.png'.format(sample_name)))
    except:
        pass
    #plt.savefig(os.path.join(plots_sfol,'zoom2_rg_{}.pdf'.format(sample_name)))
    
    # I0, Iqmin and Rg graph
    
    fig = plt.figure(num=None)
    plt.title('{}    S{}'.format(sample_name,sample_ser),fontsize=22)
    plt.xlabel('Image number',fontsize=18)
    plt.grid(axis='x')
    ax1 = fig.add_subplot(111)
    sc1 = ax1.plot(im_num_ar,i0_ar,'o',label='I$_0$')
    sc2 = ax1.plot(im_num_ar,iqmin_ar,'o',label='I$_{qmin}$')
    ax1.set_ylabel('I$_{0}$, I$_{qmin}$',fontsize=18)
    ax1.set_yticklabels([])
    ax2 = ax1.twinx()
    sc3 = ax2.plot(im_num_ar,rg_ar,'ro',label='R$_g$')
    ax2.set_ylabel('R$_g$ [$\AA$]',color='r',fontsize=18)
    ax2_max = int(1.4*nlargest(10,rg_ar)[-1])
    ax2.set_ylim(0,ax2_max)
    # ticks for the Rg y-axis
    try:
        rg_ticks = max(5,int(ax2_max/6)-numpy.mod(int(ax2_max/6),5))
        ax2.yaxis.set_ticks(numpy.arange(0,ax2_max,rg_ticks))
    except:
        pass
    ax2.grid(axis='y')
    try:
        ax1_max = 1.2*nlargest(10,i0_ar)[-1]
        ax1.set_ylim(0,ax1_max)
        # try to make y-ticks at the same place on both sides
        ax1.yaxis.set_ticks(numpy.arange(0,ax1_max,(ax1_max/ax2_max)*rg_ticks))
    except:
        pass
    for tl in ax2.get_yticklabels():
        tl.set_color('r')
    if shut_cl_end < end_num:
        plt.xlim(xmin=shut_cl_end-20)
    plt.xlim(xmax=end_num+7)
    # put labels together
    sc = sc1+sc2+sc3
    labels = [l.get_label() for l in sc]
    ax2.legend(sc,labels,numpoints=1,loc=2, fontsize=14, frameon=False, framealpha=0.5,labelspacing = 0.1)
    plt.savefig(os.path.join(plots_sfol,'i0_iqmin_rg_{}.png'.format(sample_name)))
    #plt.savefig(os.path.join(plots_sfol,'i0_iqmin_rg_{}.pdf'.format(sample_name)))
    # try to make zoomed-in graphs
    try:
        plt.xlim(im_num_ar[peak_pos]-70,im_num_ar[peak_pos]+70)
        plt.savefig(os.path.join(plots_sfol,'zoom1_i0_iqmin_rg_{}.png'.format(sample_name)))
    except:
        pass
    #plt.savefig(os.path.join(plots_sfol,'zoom1_i0_iqmin_rg_{}.pdf'.format(sample_name)))
    # zoom-in part two
    try:
        xxmin = im_num_ar[numpy.where(numpy.array(i0_ar)>(0.2*max(i0_ar)))[0][0]]
        xxmax = im_num_ar[len(i0_ar)-1-numpy.where(numpy.array(i0_ar)[::-1]>(0.2*max(i0_ar)))[0][0]]
        plt.xlim(xxmin-20,xxmax+10)
        plt.savefig(os.path.join(plots_sfol,'zoom2_i0_iqmin_rg_{}.png'.format(sample_name)))
    except:
        pass
    #plt.savefig(os.path.join(plots_sfol,'zoom2_i0_iqmin_rg_{}.pdf'.format(sample_name)))
    
    
    
    if uv_data!=[]:
        
        # UV and rg graph
        
        fig=plt.figure(num=None)
        plt.title('{}    S{}'.format(sample_name,sample_ser),fontsize=22)
        plt.xlabel('Image number',fontsize=18)
        plt.grid(axis='x')
        ax1 = fig.add_subplot(111)
        sc1 = ax1.plot(uv_data[:,0],uv_data[:,1]/uv_data[:,2],'o',label='UV$_1$/UV$_2$')
        ax1.set_ylabel('UV$_1$/UV$_2$',fontsize=18)
        ax1.set_ylim(max(0,-0.5+min(uv_data[:,1]/uv_data[:,2])),min(5,0.5+max(uv_data[:,1]/uv_data[:,2])))
        ax2 = ax1.twinx()
        sc2 = ax2.plot(im_num_ar,rg_ar,'ro',label='R$_g$')
        ax2.set_ylabel('R$_g$ [$\AA$]',color='r',fontsize=18)
        ax2_max = int(1.4*nlargest(10,rg_ar)[-1])
        ax2.set_ylim(0,ax2_max)
        sc = sc1+sc2
        labels = [l.get_label() for l in sc]
        ax2.legend(sc,labels,numpoints=1,loc=2, fontsize=14, frameon=False, framealpha=0.5,labelspacing = 0.1)
        ax2.grid(axis='y')
        #ax1.yaxis.set_ticks((10*numpy.arange(0,5.,(5./ax2_max)*rg_ticks)).astype('int')/10.)
        plt.savefig(os.path.join(plots_sfol,'uv_rg_{}.png'.format(sample_name)))
        #plt.savefig(os.path.join(plots_sfol,'uv_rg_{}.pdf'.format(sample_name)))
        # zoomed-in graph
        try:
            plt.xlim(im_num_ar[peak_pos]-70,im_num_ar[peak_pos]+70)
            plt.savefig(os.path.join(plots_sfol,'zoom1_uv_rg_{}.png'.format(sample_name)))
        except:
            pass
        # zoom-in part two
        try:
            xxmin = im_num_ar[numpy.where(numpy.array(i0_ar)>(0.2*max(i0_ar)))[0][0]]
            xxmax = im_num_ar[len(i0_ar)-1-numpy.where(numpy.array(i0_ar)[::-1]>(0.2*max(i0_ar)))[0][0]]
            plt.xlim(xxmin-20,xxmax+10)
            plt.savefig(os.path.join(plots_sfol,'zoom2_uv_rg_{}.png'.format(sample_name)))
        except:
            pass
    
    # avergae dat files
    print('Averaging data')
    if shut_cl_start > end_num:
        av_start = numpy.ceil((num_buf+1)/5.)*5
    else:
        av_start=numpy.ceil((shut_cl_end+1)/5.)*5
    
    for aa in numpy.arange(av_start,end_num-4,5):
        try:
            dat_file = os.path.join(sastool_sfol,sample_name_ser+"_0_"+str(int(aa)).zfill(num_length)+".dat")
            avg_data = numpy.genfromtxt(dat_file)
            for zz in numpy.arange(1,5):
                dat_file = os.path.join(sastool_sfol,sample_name_ser+"_0_"+str(int(aa+zz)).zfill(num_length)+".dat")
                dat_data = numpy.genfromtxt(dat_file)
                avg_data[:,1]=avg_data[:,1]+dat_data[:,1]
                avg_data[:,2]=avg_data[:,2]+dat_data[:,2]
            
            avg_data[:,1] = avg_data[:,1]/5
            avg_data[:,2] = avg_data[:,2]/5
                
            numpy.savetxt(os.path.join(avg_sfol,"Ave_"+sample_name_ser+"_0_"+str(int(aa)).zfill(num_length)+"-"+str(int(aa+4)).zfill(num_length)+'.dat'),avg_data,fmt='%.8g')
        except:
            print("Problem averaging data!")
