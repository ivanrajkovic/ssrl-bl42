#!/usr/bin/env python
# 
# secpipe.py <path to first file> <num_buf> <end_num> <wait_time[s]> [<shut_cl_start> <shut_cl_end>]


'''
@author: rajkovic
'''

from __future__ import division

import sys
import os
import re
import shutil
import time
import distutils.spawn
from distutils.dir_util import mkpath
#import asyncore
#import pyinotify
import subprocess
import glob
import numpy
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from heapq import nlargest
import fileinput

def finish_analysis():
    os.remove(log_file_tmp)
    
    # save data
    numpy.savetxt(os.path.join(plots_sfol,'{}_plot_data.txt'.format(sample_name)),numpy.transpose([im_num_ar,rg_ar,i0_ar,iqmin_ar]),fmt='%.8G',delimiter='\t',header='im.num. \tRg \tI0 \tIqmin')
    # read UV data, if exists
    uv_bip = os.path.join(data_fol,sample_name_ser+'uv.bip')
    try:
        uv_data = numpy.genfromtxt(uv_bip,skip_header=27)
        numpy.savetxt(os.path.join(plots_sfol,'{}_UV_data.txt'.format(sample_name)),uv_data,fmt='%.8G',delimiter='\t',header='im.num. \tUV1 \tUV2')
    except:
        print('No UV data')
        uv_data=[]
    
    # make graphs
    
    # I0 and Iqmin graph
    
    plt.figure(num=None)
    plt.title('{}    S{}'.format(sample_name,sample_ser),fontsize=22)
    plt.grid(axis='both')
    plt.plot(im_num_ar,i0_ar,'o',label='I$_0$')
    plt.plot(im_num_ar,iqmin_ar,'o',label='I$_{qmin}$')
    plt.xlabel('Image number',fontsize=18)
    plt.ylabel('I$_0$, I$_{qmin}$',fontsize=18)
    yy_max=nlargest(10,i0_ar)[-1]
    try:
        plt.ylim(0,yy_max*1.2)
        plt.yticks(numpy.arange(0,yy_max*1.2,1.2*yy_max/6)," ")
    except:
        pass
    if shut_cl_end < end_num:
        plt.xlim(xmin=shut_cl_end-20)
    plt.xlim(xmax=end_num+7)
    
    plt.legend(numpoints=1,loc=2,fontsize=14, frameon=False, framealpha=0.5,labelspacing = 0.1)
    plt.savefig(os.path.join(plots_sfol,'i0_iqmin_{}.png'.format(sample_name)))
    #plt.savefig(os.path.join(plots_sfol,'i0_iqmin_{}.pdf'.format(sample_name)))
    # try to make zoomed-in graphs
    peak_pos=numpy.argmax(iqmin_ar)
    try:
        plt.xlim(im_num_ar[peak_pos]-70,im_num_ar[peak_pos]+70)
        plt.savefig(os.path.join(plots_sfol,'zoom1_i0_iqmin_{}.png'.format(sample_name)))
        #plt.savefig(os.path.join(plots_sfol,'zoom1_i0_iqmin_{}.pdf'.format(sample_name)))
    except:
        print('Could not make zoomed-in graph')
    # zoom-in part two
    try:
        xxmin = im_num_ar[numpy.where(numpy.array(i0_ar)>(0.2*max(i0_ar)))[0][0]]
        xxmax = im_num_ar[len(i0_ar)-1-numpy.where(numpy.array(i0_ar)[::-1]>(0.2*max(i0_ar)))[0][0]]
        plt.xlim(xxmin-20,xxmax+10)
        plt.savefig(os.path.join(plots_sfol,'zoom2_i0_iqmin_{}.png'.format(sample_name)))
        #plt.savefig(os.path.join(plots_sfol,'zoom2_i0_iqmin_{}.pdf'.format(sample_name)))
    except:
        print('Could not make zoomed-in graph')
    
    # Rg graph
    
    plt.figure(num=None)
    plt.title('{}    S{}'.format(sample_name,sample_ser),fontsize=22)
    plt.grid(axis='both')
    plt.plot(im_num_ar,rg_ar,'ro',label='R$_g$')
    plt.xlabel('Image number',fontsize=18)
    plt.ylabel('R$_g$ [$\AA$]',fontsize=18)
    try:
        plt.ylim(0,1.4*nlargest(10,rg_ar)[-1])
    except:
        pass
    if shut_cl_end < end_num:
        plt.xlim(xmin=shut_cl_end-20)
    plt.xlim(xmax=end_num+7)
    plt.legend(numpoints=1,loc=2, fontsize=14, frameon=False, framealpha=0.5,labelspacing = 0.1)
    plt.savefig(os.path.join(plots_sfol,'rg_{}.png'.format(sample_name)))
    #plt.savefig(os.path.join(plots_sfol,'rg_{}.pdf'.format(sample_name)))
    # try to make zoomed-in graphs
    try:
        plt.xlim(im_num_ar[peak_pos]-70,im_num_ar[peak_pos]+70)
        plt.savefig(os.path.join(plots_sfol,'zoom1_rg_{}.png'.format(sample_name)))
        #plt.savefig(os.path.join(plots_sfol,'zoom1_rg_{}.pdf'.format(sample_name)))
    except:
        print('Could not make zoomed-in graph')   
    # zoom-in part two
    try:
        xxmin = im_num_ar[numpy.where(numpy.array(i0_ar)>(0.2*max(i0_ar)))[0][0]]
        xxmax = im_num_ar[len(i0_ar)-1-numpy.where(numpy.array(i0_ar)[::-1]>(0.2*max(i0_ar)))[0][0]]
        plt.xlim(xxmin-20,xxmax+10)
        plt.savefig(os.path.join(plots_sfol,'zoom2_rg_{}.png'.format(sample_name)))
        #plt.savefig(os.path.join(plots_sfol,'zoom2_rg_{}.pdf'.format(sample_name)))
    except:
        print('Could not make zoomed-in graph')  
    # I0, Iqmin and Rg graph
    
    fig = plt.figure(num=None)
    plt.title('{}    S{}'.format(sample_name,sample_ser),fontsize=22)
    plt.xlabel('Image number',fontsize=18)
    plt.grid(axis='x')
    ax1 = fig.add_subplot(111)
    sc1 = ax1.plot(im_num_ar,i0_ar,'o',label='I$_0$')
    sc2 = ax1.plot(im_num_ar,iqmin_ar,'o',label='I$_{qmin}$')
    ax1.set_ylabel('I$_{0}$, I$_{qmin}$',fontsize=18)
    ax1.set_yticklabels([])
    ax2 = ax1.twinx()
    sc3 = ax2.plot(im_num_ar,rg_ar,'ro',label='R$_g$')
    ax2.set_ylabel('R$_g$ [$\AA$]',color='r',fontsize=18)
    try:
        ax2_max = int(1.4*nlargest(10,rg_ar)[-1])
        ax2.set_ylim(0,ax2_max)
        # ticks for the Rg y-axis
        rg_ticks = max(5,int(ax2_max/6)-numpy.mod(int(ax2_max/6),5))
        ax2.yaxis.set_ticks(numpy.arange(0,ax2_max,rg_ticks))
    except:
        pass
    ax2.grid(axis='y')
    try:
        ax1_max = 1.2*nlargest(10,i0_ar)[-1]
        ax1.set_ylim(0,ax1_max)
        # try to make y-ticks at the same place on both sides
        ax1.yaxis.set_ticks(numpy.arange(0,ax1_max,(ax1_max/ax2_max)*rg_ticks))
    except:
        pass
    for tl in ax2.get_yticklabels():
        tl.set_color('r')
    if shut_cl_end < end_num:
        plt.xlim(xmin=shut_cl_end-20)
    plt.xlim(xmax=end_num+7)
    # put labels together
    sc = sc1+sc2+sc3
    labels = [l.get_label() for l in sc]
    ax2.legend(sc,labels,numpoints=1,loc=2, fontsize=14, frameon=False, framealpha=0.5,labelspacing = 0.1)
    plt.savefig(os.path.join(plots_sfol,'i0_iqmin_rg_{}.png'.format(sample_name)))
    #plt.savefig(os.path.join(plots_sfol,'i0_iqmin_rg_{}.pdf'.format(sample_name)))
    # try to make zoomed-in graphs
    try:
    
        plt.xlim(im_num_ar[peak_pos]-70,im_num_ar[peak_pos]+70)
        plt.savefig(os.path.join(plots_sfol,'zoom1_i0_iqmin_rg_{}.png'.format(sample_name)))
        #plt.savefig(os.path.join(plots_sfol,'zoom1_i0_iqmin_rg_{}.pdf'.format(sample_name)))
    except:
        print('Could not make zoomed-in graph')
    # zoom-in part two
    try:
        xxmin = im_num_ar[numpy.where(numpy.array(i0_ar)>(0.2*max(i0_ar)))[0][0]]
        xxmax = im_num_ar[len(i0_ar)-1-numpy.where(numpy.array(i0_ar)[::-1]>(0.2*max(i0_ar)))[0][0]]
        plt.xlim(xxmin-20,xxmax+10)
        plt.savefig(os.path.join(plots_sfol,'zoom2_i0_iqmin_rg_{}.png'.format(sample_name)))
        #plt.savefig(os.path.join(plots_sfol,'zoom2_i0_iqmin_rg_{}.pdf'.format(sample_name)))
    except:
        print('Could not make zoomed-in graph')
    
    
    
    if uv_data!=[]:
        
        # UV and rg graph
        
        fig=plt.figure(num=None)
        plt.title('{}    S{}'.format(sample_name,sample_ser),fontsize=22)
        plt.xlabel('Image number',fontsize=18)
        plt.grid(axis='x')
        ax1 = fig.add_subplot(111)
        sc1 = ax1.plot(uv_data[:,0],uv_data[:,1]/uv_data[:,2],'o',label='UV$_1$/UV$_2$')
        ax1.set_ylabel('UV$_1$/UV$_2$',fontsize=18)
        ax1.set_ylim(max(0,-0.5+min(uv_data[:,1]/uv_data[:,2])),min(5,0.5+max(uv_data[:,1]/uv_data[:,2])))
        ax2 = ax1.twinx()
        sc2 = ax2.plot(im_num_ar,rg_ar,'ro',label='R$_g$')
        ax2.set_ylabel('R$_g$ [$\AA$]',color='r',fontsize=18)
        ax2_max = int(1.4*nlargest(10,rg_ar)[-1])
        ax2.set_ylim(0,ax2_max)
        sc = sc1+sc2
        labels = [l.get_label() for l in sc]
        ax2.legend(sc,labels,numpoints=1,loc=2, fontsize=14, frameon=False, framealpha=0.5,labelspacing = 0.1)
        ax2.grid(axis='y')
        #ax1.yaxis.set_ticks((10*numpy.arange(0,5.,(5./ax2_max)*rg_ticks)).astype('int')/10.)
        plt.savefig(os.path.join(plots_sfol,'uv_rg_{}.png'.format(sample_name)))
        #plt.savefig(os.path.join(plots_sfol,'uv_rg_{}.pdf'.format(sample_name)))
        plt.xlim(im_num_ar[peak_pos]-70,im_num_ar[peak_pos]+70)
        plt.savefig(os.path.join(plots_sfol,'zoom1_uv_rg_{}.png'.format(sample_name)))
        
    # average sub files
    if shut_cl_start > end_num:
        av_start = numpy.ceil((num_buf+1)/5.)*5
    else:
        av_start=numpy.ceil((shut_cl_end+1)/5.)*5
    
    for aa in numpy.arange(av_start,end_num-4,5):
        try:
            sub_file = os.path.join(sastool_sfol,sample_name_ser+"_0_"+str(int(aa)).zfill(num_length)+".sub")
            avg_data = numpy.genfromtxt(sub_file)
            for zz in numpy.arange(1,5):
                sub_file = os.path.join(sastool_sfol,sample_name_ser+"_0_"+str(int(aa+zz)).zfill(num_length)+".sub")
                sub_data = numpy.genfromtxt(sub_file)
                avg_data[:,1]=avg_data[:,1]+sub_data[:,1]
                avg_data[:,2]=avg_data[:,2]+sub_data[:,2]
            
            avg_data[:,1] = avg_data[:,1]/5
            avg_data[:,2] = avg_data[:,2]/5
                
            numpy.savetxt(os.path.join(avg_sfol,"Ave_"+sample_name_ser+"_0_"+str(int(aa)).zfill(num_length)+"-"+str(int(aa+4)).zfill(num_length)+'.dat'),avg_data,fmt='%.8g')
        except:
            print("Problem averaging data!")

    sys.exit('Done!')

## Program start

if __name__ == "__main__":
    
    if not (len(sys.argv)==7 or len(sys.argv)==5):
         
        sys.exit("""\n\n\tHow-to:
        
        {0} <path to the first sample> <num buf> <end num> <wait time [s]> [<shut cl start> <shut cl end>]
        
        """.format(os.path.basename(sys.argv[0])))
    
    # set debug level
    debug = 1
    
    # set paths
    atsas_path='/mnt/home/staff/SW/ATSAS'
    exe_path='/mnt/home/sw/bin:{}'.format(os.path.join(atsas_path,'bin'))
    ld_path=os.path.join(atsas_path,'lib64/atsas')
    
    try:
        os.environ['LD_LIBRARY_PATH']=os.environ['LD_LIBRARY_PATH']+':'+ld_path
    except:
        os.environ['LD_LIBRARY_PATH']=ld_path
    
    try:
        os.environ['PATH']=os.environ['PATH']+':'+exe_path
    except:
        os.environ['PATH']=exe_path
            
    
    # set some variables from arguments 
    first_sample_path = sys.argv[1]
    num_buf = int(sys.argv[2])
    end_num = int(sys.argv[3])
    wait_time = int(sys.argv[4])
    if len(sys.argv) == 7:
        shut_cl_start = int(sys.argv[5])
        shut_cl_end = int(sys.argv[6])
    else:
        shut_cl_start = end_num+1
        shut_cl_end = end_num+2
    
    if debug == 1:
        print('Setting variables.\n')
    
    first_sample = os.path.basename(first_sample_path)
    data_fol = os.path.abspath(os.path.dirname(first_sample_path))
    sample_name_ser = '_'.join(first_sample.split('_')[:-2])
    sample_name = "_".join(sample_name_ser.split('_')[:-1])
    sample_ser = sample_name_ser.split('_')[-1]
    num_length = len(os.path.splitext(first_sample)[0].split('_')[-1])
    
    # prepare folders
    if debug == 1:
        print('Making folders.\n')
    analysis_fol = os.path.abspath(os.path.join(data_fol,'../analysis/'))
    sample_sfol = os.path.join(analysis_fol,sample_name_ser)
    sastool_sfol = os.path.join(sample_sfol,'sastool')
    plots_sfol = os.path.join(sample_sfol,'plots')
    avg_sfol = os.path.join(sample_sfol,'average')
    
    mkpath(analysis_fol)
    mkpath(sastool_sfol)
    mkpath(plots_sfol)
    mkpath(avg_sfol)
    
    buf_name = os.path.join(data_fol,first_sample)
    
    # location of the integ.mpp
    integ_mpp = os.path.join(analysis_fol,'integ.mpp')
    
    # set sastool and autorg paths
    autorg = distutils.spawn.find_executable('autorg')
    sastool = distutils.spawn.find_executable('sastool')
    
    if autorg == None:
        sys.exit('\'autorg\' not found, please add it to the path.')
    if sastool == None:
        sys.exit('\'sastool\' not found, please add it to the path.')
    
    # header for a log file
    log_header = """ _graph_title.text "Rg, I0 and Iqmin vs Image Number"
  
 _graph_axes.xLabel "Image Number"
 _graph_axes.yLabel "Rg"
 _graph_axes.x2Label ""
 _graph_axes.y2Label "I0,Iqmin"
  
 _graph_background.showGrid 1
  
 data_
 _trace.name Data
 _trace.xLabels "{Image Number}"
 _trace.hide 0
  
 loop_
 _sub_trace.name
 _sub_trace.yLabels
 _sub_trace.color
 _sub_trace.width
 _sub_trace.symbol
 _sub_trace.symbolSize
 Rg "{Rg}" blue 0 square 2
 I0 "{I0,Iqmin}" red 0 circle 2
 Iqmin "{I0,Iqmin}" darkgreen 0 circle 2
  
 loop_
 _sub_trace.x
 _sub_trace.y1
 _sub_trace.y2
 _sub_trace.y3
"""
    
    
    # create log file and tmp log file
    if debug == 1:
        print('Creating log file.\n')
    log_file = os.path.join(sample_sfol,sample_name_ser+'.bip')
    log_file_tmp = log_file+'.tmp'
    with open(log_file_tmp,'w') as lg:
        lg.write(log_header)
    
    shutil.copy(log_file_tmp,log_file)
       
    #file_list=[]
    spmpp = os.path.join(sastool_sfol,'secpipe.mpp')
    
    # prepare secpipe.mpp
    spmpp_text=[]
    t_switch = 0
    
    if debug == 1:
        print('Reading integ.mpp.\n')
    
    os.chdir(analysis_fol)
    with open(integ_mpp, "r") as impp:
        for line in impp:
            if not line.startswith('#') and not line.startswith('-f') and not re.match(r'^\s*$', line):
                if line.startswith('-m'):
                    line='-m yes {}'.format(os.path.abspath(line.split()[-1]))
                if not line.endswith('\n'):
                    line=line+'\n'
                spmpp_text.append(line)
            if line.startswith('-t'):
                t_switch = 1
    
    # append '-t yes' line if missing in integ.mpp
    if t_switch==0:
        spmpp_text.append('-t yes\n')
        
    # set last_file_time to 20 mins in future, to allow some time before the first image
    last_file_time = time.time() + 1200
        
# change folder to sastool
    os.chdir(sastool_sfol)
    
    # empty arrays, used for plotting later
    im_num_ar = []
    rg_ar = []
    i0_ar = []
    iqmin_ar = []
    
    # set current image number, start with 1
    im_num = 1
    
    while 1:
        if time.time() - last_file_time > 120+wait_time:
            if debug == 1:
                print('Too much time since last image.\n')
            finish_analysis()
        
        # next image file name to analyze
        next_filename = re.sub('(.*)_(\d+)\.ti(f+)','\\1_{}.ti\\3'.format(str(im_num).zfill(num_length)),buf_name)
        
        if debug == 1:
            print('Looking for {}.\n'.format(next_filename))
        
        # check if the file exists:
        if not os.path.isfile(next_filename):
            if debug == 1:
                print('File not found, trying again in 2 seconds.\n')
            # if not, wait 2 seconds and try again
            time.sleep(2)
        else:
            # set the read time as last image time
            last_file_time = time.time()
            if num_buf < im_num < shut_cl_start or im_num > shut_cl_end-1:
                # write new secpipe.mpp
                with open(spmpp, 'w') as mpp_file:
                    for item in spmpp_text:
                        mpp_file.write("{}".format(item))
                    mpp_file.write('-f {} 1 {} {}'.format(os.path.join(data_fol,next_filename), buf_name, num_buf))
                
                # run sastool, but wait a bit for the int file to be written
                time.sleep(.5)
                subprocess.call([sastool,spmpp])
                
                #delete zeros from sub file:
                sub_file = os.path.join(sastool_sfol,os.path.splitext(os.path.basename(next_filename))[0]+".sub")
                top_zeros = 0
                for line in fileinput.input(sub_file, inplace = 1):
                    if (not re.match('.* 0 0',line) or top_zeros == 1):
                        sys.stdout.write(line)
                        top_zeros = 1
                        
                # run autorg
                try:
                    #rg_out = subprocess.check_output([autorg,'--sminrg=1','--smaxrg=1.3',sub_file])
                    rg_out = subprocess.check_output([autorg,sub_file])
                    rg=float(rg_out.split()[2])
                    #rg_stdev=rg_out.split()[4])
                    i0=float(rg_out.split()[8])
                    #i0_stdev=rg_out.split()[10]
                except:
                    rg = 0
                    i0 = 0
                
                if rg<0:
                    rg = 0
                if i0<0:
                    i0=0

                # read i_qmin value
                sub_d = numpy.genfromtxt(sub_file)
                iqmin = max(sub_d[5,1],0)

                # put the values in arrayes for printing later
                im_num_ar.append(im_num)
                rg_ar.append(rg)
                i0_ar.append(i0)
                iqmin_ar.append(iqmin)
                
                # write temp file with rg and i0, move to permenant one
                with open(log_file_tmp,'a') as lg:
                    lg.write('{} {} {} {}\n'.format(im_num,rg,i0,iqmin))
                
                shutil.copy(log_file_tmp,log_file) 
                
            if im_num == end_num:
                if debug == 1:
                    print('Finished all images.\n')
                finish_analysis()
                
            im_num = im_num+1
            if debug == 1:
                print('\n\nNext image number {}.\n'.format(im_num))
                
            
        
