#!/usr/bin/env python

import os
import subprocess
import sys
import glob
import fileinput
import re

integmpp = './integ.mpp'
data_folder = '../data/'


if len(sys.argv) < 2 or len(sys.argv) > 3:
	sys.exit("\nUsage: "+ sys.argv[0] + " <sample scan no.> [buffer scan no.]\n\nShould be run from the analysis folder.\nIt will add\n '-f <sample> [buffer]'\n lines to integ.mpp and run\n 'sastool integ.mpp'.")

for line in fileinput.input(integmpp, inplace = 1): 
	line = re.sub('^-f','#-f',line)
	line = re.sub('-s no','-s yes',line)
	if len(sys.argv) == 2:
		line = re.sub('^-s','#-s',line)
	if len(sys.argv) == 3:
		line = re.sub('^#-s','-s',line)	
	sys.stdout.write(line)


sn_input = sys.argv[1]

if '-' in sn_input:
	sn_range = range(int(sn_input.partition('-')[0]),int(sn_input.partition('-')[2])+1)
else:
	sn_range = range(int(sn_input),int(sn_input)+1)
	
if len(sys.argv) == 3:
	bn = sys.argv[2]
	bf_list=[]
	rrr = re.compile('.*[SBT]'+str(bn).zfill(3)+'(_0_|_)0+1\.tif')
	
	for file in os.listdir(data_folder):
		if rrr.match(file):
			bf_list.append(os.path.join(data_folder,file))
	
	if len(bf_list) != 1:
		print(bf_list)
		sys.exit("\nThere are "+str(len(bf_list))+" file(s) matching *"+str(bn).zfill(3)+"_0_001.tif")
	buffer_file=bf_list[0]

ff = open(integmpp,'a')
for sn in sn_range:
	
	sp_list=[]
	rrr = re.compile('.*[SBT]'+str(sn).zfill(3)+'(_0_|_)0+1\.tif')
	
	for file in os.listdir(data_folder):
		if rrr.match(file):
			sp_list.append(os.path.join(data_folder,file))
	
	if len(sp_list) != 1:
		print(sp_list)
		sys.exit("\nThere are "+str(len(sp_list))+" file(s) matching *"+str(sn).zfill(3)+"_0_001.tif")
	
	sample_file=sp_list[0]
	
	new_line = '-f '+sample_file

	if len(sys.argv) == 3:
		new_line = new_line+" "+buffer_file
		
	
	ff.write(new_line+"\n")

ff.close()
	



subprocess.call(['sastool',integmpp])

