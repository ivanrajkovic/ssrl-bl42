#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division

import sys
import os
import glob
import re
import getpass
import signal as sg
import time
import fileinput
import shutil
import subprocess
import struct

#from PyQt5 import QtGui,QtWidgets
#from PyQt5.QtWidgets import QFileDialog, QGraphicsPixmapItem, QMainWindow, QApplication, QGraphicsScene
#from PyQt5.QtGui import QPixmap, QColor
#from PyQt5.QtCore import Qt, QRect, QRectF

from PyQt4.QtGui import QFileDialog, QGraphicsPixmapItem, QMainWindow, QApplication, QGraphicsScene, QPixmap, QColor, QStyleFactory, QCursor, QWidget, QMouseEvent, QGraphicsItem, QPen, QBrush,  QPainter, QPainterPath, QPolygonF, QMessageBox, QImage, QGraphicsView, QGridLayout, QTabWidget, QSizePolicy, QFrame, QVBoxLayout, QHBoxLayout, QPushButton, QFormLayout, QLabel, QSlider, QDoubleSpinBox, QSpacerItem, QLayout, QSpinBox, QStatusBar, QLineEdit, QCheckBox, QDialog, QPlainTextEdit, QScrollArea, QScrollBar # QIcon, QStyle
from PyQt4.QtCore import QDir, Qt, QEvent, QPointF, QSize, QMetaObject, QRectF, QString, QRect#, QLineF

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as Canvas
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.ticker import LinearLocator
from matplotlib.ticker import AutoLocator


from matplotlib import cm
from matplotlib import colors
from matplotlib import path
from PIL import Image
from PIL.ImageQt import ImageQt
#import PIL
import numpy
numpy.seterr(divide='ignore',invalid='ignore')
from scipy import signal
from scipy.misc import toimage
from scipy.optimize import curve_fit, leastsq
import heapq

from matplotlib import rcParams
#rcParams.update({'figure.autolayout': True})

from des_tabs4 import Ui_MainWindow
#insert_gui_code_here

# exit on CRTL-C (had to import signal as sg because of signal from scipy)
sg.signal(sg.SIGINT, sg.SIG_DFL)

def circ(sx,sy,rx,ry,rr,inth,intl):
    ''' create matrix filled with intl excpet for a cirle filled with inth
    '''
    ccxx, ccyy = numpy.mgrid[:sx, :sy]
    ccircle = (ccxx - rx) ** 2 + (ccyy - ry) ** 2
    cc=numpy.where(ccircle<rr**2,inth,intl)
    return cc

def gray_to_color(im2d_data,scale_min,scale_max,cm_data=0):
    ''' create color image from grayscale
    '''
    # full range is hard to vsualize, use log
    # rescale to 0<->255 scale
    #im2d_data=numpy.log(numpy.clip(im2d_data,0.01,None))
    #im2d_data = 255*(im2d_data - im2d_data.min())/(im2d_data.max() - im2d_data.min())
    # rescale scale_min<->scale_max to 0<->1
    #if scale_max != scale_min: 
    #    normed_a = numpy.clip((im2d_data - scale_min)/(scale_max - scale_min),0,1)
    #else:
    #    normed_a = numpy.zeros_like(im2d_data)
    # colormaps: jet, gist_stern, gnuplot, gist_ncar, CMRmap, terrain, nipy_spectral, tab20b, cubehelix
    #if cm_data == 0:
    #    mapped_img = cm.nipy_spectral(normed_a)
    #else:
    #    mapped_img = cm.Reds_r(normed_a)
    #mapped_img_u8 = (255.0 * mapped_img).astype('uint8')
    #return(mapped_img_u8)
    
    # new method, also should do numpy.log only once when reading data
    #
    
    if scale_min > scale_max:
        norm = colors.Normalize(scale_max, scale_min, clip=True)
        cmap = cm.nipy_spectral_r
    else:
        norm = colors.Normalize(scale_min, scale_max, clip=True)
        cmap = cm.nipy_spectral
    
    mm = cm.ScalarMappable(norm=norm,cmap=cmap)
    mapped_img = (255 * mm.to_rgba(im2d_data)).astype('uint8')
    return(mapped_img)

def is_ccw(p):
    ''' check if the pth is counter-clockwise
    '''
    v = p.vertices-p.vertices[0,:]
    a = numpy.arctan2(v[1:,1],v[1:,0])
    return (a[1:] >= a[:-1]).astype(int).mean() >= 0.5

def gauss(x, *p):
    ''' gauss function
    '''
    A, mu, sigma = p
    return A*numpy.exp(-(x-mu)**2/(2.*sigma**2))

def fit1dgauss(data,cen,dx,det_bin):
    ''' fit 1d gauss
    '''
    cen = int(cen)
    dx = int(dx)
    pp = [data[(cen-dx):(cen+dx),1].max()-data[(cen-dx):(cen+dx),1].min(),cen,40/det_bin]
    coeff, var_matrix = curve_fit(gauss,data[(cen-dx):(cen+dx),0],data[(cen-dx):(cen+dx),1],p0=pp)
    return coeff, var_matrix

def calc_R(x,y, xc, yc):
    """ calculate the distance of each 2D points from the center (xc, yc) """
    return numpy.sqrt((x-xc)**2 + (y-yc)**2)

def f_2(c, x, y):
    """ calculate the algebraic distance between the data points and the mean circle centered at c=(xc, yc) """
    Ri = calc_R(x, y, *c)
    return Ri - Ri.mean()

def make_ang_mask(sh,sv,cx,cy,start_ang,ang_range,name='angular_mask.tif'):
    ''' make a mask for integration over
    angular wedge start_ang +- ang_range/2
    ''' 
    xm, ym = numpy.meshgrid(numpy.arange(sh),numpy.arange(sv))
    zm = numpy.degrees(numpy.arctan2(-ym+cy,xm-cx))
    zm = numpy.where(numpy.abs(zm-start_ang)<(0.5*ang_range),1,0)
    zm = numpy.flipud(zm.astype('uint16'))
    im = Image.frombytes('I;16',(sh,sv),zm.tobytes())
    im.save(name)

def find_image(folder,num):
    ''' find image file from 'num' series in 'folder' folder
    '''
    im_list=[]
    # find the image filename for the given series number
    rrr = re.compile('.*[SBT]'+str(num).zfill(3)+'_0_0+1\.tif')
    for file in os.listdir(folder):
        if rrr.match(file):
            im_list.append(file)
    if len(im_list) == 0:
        result = 0
    elif len(im_list) > 1:
        print("\n\033[0;37;41m\033[1m\033[4m Two or more files found! \033[0;0m")
        result = 1
    else:
        result = os.path.abspath(os.path.join(folder,im_list[0]))
    
    return result

def grow_mask(mm):
    cc = numpy.ones_like(mm)
    mx,my = mm.shape
    ii = numpy.where(mm==0)
    for xi,yi in zip(*ii):
        cc[max(0,xi-1):min(xi+2,mx),max(0,yi-1):min(yi+2,my)] = 0
    return(cc)

class MplCanvas(Canvas):
    def __init__(self):
        self.fig = Figure()       
        self.ax1 = self.fig.add_subplot(111)
        self.ax2 = self.ax1.twinx()
        Canvas.__init__(self, self.fig)
        Canvas.updateGeometry(self)

class mpl_widget(QWidget):
    def __init__(self, parent = None):
        QWidget.__init__(self, parent)
        self.canvas = MplCanvas()     
        self.mpl_toolbar = NavigationToolbar(self.canvas, self)
        self.vbl = QVBoxLayout()
        self.vbl.addWidget(self.mpl_toolbar)
        self.vbl.addWidget(self.canvas)     
        self.setLayout(self.vbl)


class MyWindow(QMainWindow):
    def __init__(self):
        super(MyWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        # set theme: "Motif" "Windows" "CDE" "Plastique"  "Cleanlooks" "GTK+"
        QApplication.setStyle(QStyleFactory.create("Cleanlooks"))
        self.show()
        
        # info about changing tabs
        #self.ui.tabWidget.blockSignals(True) #just for not showing the initial message
        self.ui.tabWidget.currentChanged.connect(self.onChange)
        #self.ui.tabWidget.blockSignals(False)
        
        ## button connections for qcalib_tab
        self.ui.superauto.clicked.connect(self.do_all)
        self.ui.find_agbe.clicked.connect(self.find_fol_agbe)
        self.ui.open_agbe.clicked.connect(self.open_agbe)
        self.ui.set_analysis.clicked.connect(self.set_analysis)
        self.ui.find_center_1.clicked.connect(self.find_center_1)
        self.ui.set_center_1.clicked.connect(self.set_center_1)
        self.ui.set_angles_auto.clicked.connect(self.set_angles_auto)
        self.ui.set_angles_man.clicked.connect(self.set_angles_man)
        self.ui.find_center_2.clicked.connect(self.refine_center)
        self.ui.q_calib.clicked.connect(self.q_calib)
        self.ui.save_integ.clicked.connect(self.save_integ)
        self.ui.zoom_select.clicked.connect(self.zoom_select)
        self.ui.max_slider.valueChanged.connect(self.max_changed)
        self.ui.min_slider.valueChanged.connect(self.min_changed)
        self.ui.zoom_slider.valueChanged.connect(self.zoom_changed)
        self.ui.radius1_px.valueChanged.connect(self.show_angles)
        self.ui.radius1_width.valueChanged.connect(self.show_angles)
        self.ui.ring_num.valueChanged.connect(self.show_angles)
        self.ui.energy_value.valueChanged.connect(self.setup_change)
        self.ui.app_distance_value.valueChanged.connect(self.setup_change)
        self.ui.px_size.valueChanged.connect(self.setup_change)
        self.ui.q_log.stateChanged.connect(self.show_agbe)
        self.ui.q_cm_reset.clicked.connect(self.q_reset_cm)
        
        ## button connections for mask_tab
        self.ui.open_im4mask.clicked.connect(self.open_im4mask)
        self.ui.mask_zoom_select.clicked.connect(self.mask_zoom_select)
        self.ui.max_mask.valueChanged.connect(self.mask_max_changed)
        self.ui.min_mask.valueChanged.connect(self.mask_min_changed)
        self.ui.zoom_mask.valueChanged.connect(self.mask_zoom_changed)
        self.ui.mask_opacity.valueChanged.connect(self.show_masked)
        self.ui.import_msk.clicked.connect(self.import_msk_file)
        self.ui.import_tif.clicked.connect(self.import_tif_file)
        self.ui.export_msk.clicked.connect(self.export_msk_file)
        self.ui.export_tif.clicked.connect(self.export_tif_file)
        self.ui.clear_mask.clicked.connect(self.clear_mask)
        self.ui.invert_mask.clicked.connect(self.invert_mask)
        self.ui.add_polygon.clicked.connect(self.add_polygon)
        self.ui.remove_polygon.clicked.connect(self.remove_polygon)
        self.ui.add_pixels.clicked.connect(self.add_pixels)
        self.ui.remove_pixels.clicked.connect(self.remove_pixels)
        self.ui.add_mask_frame.clicked.connect(self.add_mask_frame)
        self.ui.mask_above.clicked.connect(self.mask_above)
        self.ui.mask_below.clicked.connect(self.mask_below)
        self.ui.update_integmpp.clicked.connect(self.update_integ)
        self.ui.grow_mask.clicked.connect(self.grow_mask_px)
        self.ui.mask_log.stateChanged.connect(self.show_im4mask)
        self.ui.mask_cm_reset.clicked.connect(self.mask_reset_cm)
        
        ## button connections for rad_integ_tab
        self.ui.subtract_buffer.clicked.connect(self.update_radint_buttons)
        self.ui.multiple_samples.clicked.connect(self.update_radint_buttons)
        self.ui.use_saxspipe.clicked.connect(self.update_radint_buttons)
        self.ui.radint_set_data.clicked.connect(self.radint_set_data)
        self.ui.radint_set_analysis.clicked.connect(self.radint_set_analysis)
        self.ui.select_buffer.clicked.connect(self.select_buffer_file)
        self.ui.select_sample_first.clicked.connect(self.select_sample_first_file)
        self.ui.select_sample_last.clicked.connect(self.select_sample_last_file)
        self.ui.do_2dto1d.clicked.connect(self.do_2dto1d)
        self.ui.sample_first_text.textChanged.connect(self.update_radint_buttons)
        self.ui.sample_last_text.textChanged.connect(self.update_radint_buttons)
        self.ui.buffer_text.textChanged.connect(self.update_radint_buttons)
        
        ## button connections for hplc tab
        self.ui.hplc_data_file.clicked.connect(self.hplc_select_data_file)
        self.ui.hplc_analysis.clicked.connect(self.hplc_set_analysis)
        self.ui.hplc_analyse.clicked.connect(self.hplc_sastool)
        self.ui.hplc_load_1d.clicked.connect(self.open_hplc_data)
        self.ui.hplc_load_saxs_bip.clicked.connect(self.load_saxs_bip)
        self.ui.hplc_load_uv_bip.clicked.connect(self.load_uv_bip)
        self.ui.hplc_save_saxs.clicked.connect(self.save_saxs_txt)
        self.ui.hplc_save_uv.clicked.connect(self.save_uv_txt)
        self.ui.hplc_uv_xoffset.valueChanged.connect(self.hplc_uv_change)
        self.ui.hplc_uv1_yoffset.valueChanged.connect(self.hplc_uv_change)
        self.ui.hplc_uv2_yoffset.valueChanged.connect(self.hplc_uv_change)
        self.ui.hplc_plot_reset.clicked.connect(self.hplc_reset_plot)
        self.ui.hplc_replot.clicked.connect(self.replot_hplc)
        self.ui.hplc_rg_l.stateChanged.connect(self.hplc_rg_l_click)
        self.ui.hplc_rg_r.stateChanged.connect(self.hplc_rg_r_click)
        self.ui.hplc_rg_norm.stateChanged.connect(self.hplc_rg_norm_click)
        self.ui.hplc_i0_l.stateChanged.connect(self.hplc_i0_l_click)
        self.ui.hplc_i0_r.stateChanged.connect(self.hplc_i0_r_click)
        self.ui.hplc_i0_norm.stateChanged.connect(self.hplc_i0_norm_click)
        self.ui.hplc_iqmin_l.stateChanged.connect(self.hplc_iqmin_l_click)
        self.ui.hplc_iqmin_r.stateChanged.connect(self.hplc_iqmin_r_click)
        self.ui.hplc_iqmin_norm.stateChanged.connect(self.hplc_iqmin_norm_click)
        self.ui.hplc_uv1_l.stateChanged.connect(self.hplc_uv1_l_click)
        self.ui.hplc_uv1_r.stateChanged.connect(self.hplc_uv1_r_click)
        self.ui.hplc_uv1_norm.stateChanged.connect(self.hplc_uv1_norm_click)
        self.ui.hplc_uv2_l.stateChanged.connect(self.hplc_uv2_l_click)
        self.ui.hplc_uv2_r.stateChanged.connect(self.hplc_uv2_r_click)
        self.ui.hplc_uv2_norm.stateChanged.connect(self.hplc_uv2_norm_click)
        self.ui.hplc_uv12_l.stateChanged.connect(self.hplc_uv12_l_click)
        self.ui.hplc_uv12_r.stateChanged.connect(self.hplc_uv12_r_click)
        self.ui.hplc_uv12_norm.stateChanged.connect(self.hplc_uv12_norm_click)
        self.ui.hplc_i0uv_l.stateChanged.connect(self.hplc_i0uv_l_click)
        self.ui.hplc_i0uv_r.stateChanged.connect(self.hplc_i0uv_r_click)
        self.ui.hplc_i0uv_norm.stateChanged.connect(self.hplc_i0uv_norm_click)
        self.ui.hplc_average.clicked.connect(self.hplc_average)
        self.ui.hplc_yticks_num.valueChanged.connect(self.replot_hplc)
        #self.ui.mpl_plot.canvas.mpl_connect('button_release_event',self.hplc_resize)
        #self.ui.mpl_plot.canvas.mpl_connect('motion_notify_event',self.hplc_resize)
        self.ui.mpl_plot.canvas.mpl_connect('draw_event',self.hplc_resize)
        
        ## button connections for integmpp tab
        self.ui.integmpp_open.clicked.connect(self.integmpp_open_file)
        self.ui.integmpp_reload.clicked.connect(self.integmpp_toedit)
        self.ui.integmpp_save.clicked.connect(self.integmpp_save_file)
        self.ui.integmpp_help.clicked.connect(self.integmpp_show_help)
        
        
        ## qcalib_tab init
        
        # set image viewer
        self.create_scene()
        
        # set some variables
        self.data_folder = ''
        self.analysis_folder = ''
        self.agbe_file = ''
        self.det = ''
        self.calc_dist = 0
        self.pix_data = ''
        self.scale_min = 50
        self.scale_max = 950
        self.gray_data = numpy.array(0)
        self.zoom_factor = 1
        self.angles_list = []
        self.angles_width = []
        self.integmpp = ''
        self.d_agbe = 58.38
        self.zoom_select_points = []
        self.zoom_pol = ''
        
        self.cx1 = ''
        self.cy1 = ''
        self.cx2 = ''
        self.cy2 = ''
        self.i2 = 1
        self.det_bin=1
        #q=0 is always at 0 px
        self.ring_px = numpy.array([0])
        
        
        self.ui.graphicsView.setMouseTracking(True)
        self.ui.graphicsView.viewport().installEventFilter(self)
        
        ## mask_tab init
        # set image viewer
        self.create_mask_scene()
        
        self.im4mask_folder = ''
        self.im4mask = ''
        self.im4mask_path = ''
        self.mask_scale_min = 50
        self.mask_scale_max = 950
        self.track_mouse_line = ''
        self.maskpen = QPen(QColor(255,0,0))
        #self.maskpen.setWidthF(.5)
        self.zoompen = QPen(QColor(255,0,0))
        #self.zoompen.setWidth(2)
        self.savemask_tif_path = ''
        self.mask_zoom_select_points = []
        self.mask_zoom_pol = ''
        
        self.ui.graphicsView_mask.setMouseTracking(True)
        self.ui.graphicsView_mask.viewport().installEventFilter(self)
        
        ## rad_integ_tab init
        self.buffer_path = ''
        self.sample_first_path = ''
        self.sample_last_path = ''
        
        ## hplc tab
        self.hplc_data = ''
        self.hplc_analysis_folder = ''
        self.hplc_ave_folder = ''
        self.hplc_plots_folder = ''
        self.hplc_sastool_folder = ''
        self.im_num_ar = ''
        self.rg_ar = ''
        self.i0_ar = ''
        self.iqmin_ar = ''
        self.uv_num = ''
        self.uv1 = ''
        self.uv2 = ''
        self.leg_pos = ''
        self.ll = ''
        self.hplc_title = ''
        self.hplc_norm = [1,1]
        self.data_to_plot = list('' for i in range(7))
        self.x_to_plot = list('' for i in range(7))
        self.data_label = ['R$_g$ [$\AA$]','I$_0$','I$_qmin$','UV$_1$','UV$_2$','UV$_1$/UV$_2$','I$_0$/UV$_1$']
        self.cc = ['bo','go','ro','co','mo','ko','g-']
        self.hplc_plot_lines = list([] for i in range(7))
        self.hplc_xaxis = [0,0,0,1,1,1,1]
        self.hplc_yaxes=[self.ui.mpl_plot.canvas.ax1,self.ui.mpl_plot.canvas.ax2]
        
        ## integmpp tab
        self.integmpp_edit = ''
        
    
    
       
    
    ## all tabs
    
    def onChange(self,i):
        ''' what to do when surrent tab is changed
        '''
        # print("Current Tab Index: {}".format(i) ) #changed!
        # uncheck buttons, delete any point data that is left, delete mask/zoom lines 
        # self.status_message()
        # change cursor style if needed
        
        
    def eventFilter(self, source, event):
        ''' catch mouse clicks
        '''
        modifiers = QApplication.keyboardModifiers()
        # q-calib tab
        if self.ui.tabWidget.currentIndex() == 0:
            if (event.type() == QEvent.MouseButtonPress and QMouseEvent.button(event) == 1):
                click = self.ui.graphicsView.mapToScene(event.pos())
                if self.ui.set_center_1.isChecked():
                    self.set_center_point(click.x(),click.y())
                    
                if self.ui.set_angles_man.isChecked():
                    self.set_angles_point(click.x(),click.y())
                
                if self.ui.zoom_select.isChecked():
                    self.zoom_into_select(click.x(),click.y())
                    
            if event.type() == QEvent.Wheel:
            #     
            # zoom only with crtl+wheel:
            #
                
                if modifiers == Qt.ControlModifier:
                    if event.delta() > 0 and self.ui.zoom_slider.value()<5000:
                        zoomFactor = min(1.2,5000/(self.ui.zoom_slider.value()))
                    elif event.delta() < 0 and self.ui.zoom_slider.value()>75:
                        zoomFactor = max(1/1.2,75/(self.ui.zoom_slider.value()))
                    else:
                        zoomFactor = 1
                    
                    if zoomFactor != 1:
                        self.ui.graphicsView.scale(zoomFactor, zoomFactor)
                        self.ui.zoom_slider.setValue(self.ui.zoom_slider.value()*zoomFactor)
                    
            # draw rectangle for zooming-in:
            if event.type() == QEvent.MouseMove:
                try:
                    click = self.ui.graphicsView.mapToScene(event.pos())
                    self.ui.graphicsView.setToolTip("{} x {}".format(numpy.int(click.x()),self.agbe_size_v-numpy.int(click.y())))
                except:
                    pass
                
                if event.buttons() == Qt.NoButton and self.ui.zoom_select.isChecked() == True and self.zoom_select_points != []:
                    click = self.ui.graphicsView.mapToScene(event.pos())
                    self.zoom_mouse_x = click.x()
                    self.zoom_mouse_y = click.y()
                    if self.zoom_pol != '':
                        self.scene.removeItem(self.zoom_pol)
                    self.zoom_pol = self.scene.addRect(self.zoom_select_points[0],self.zoom_select_points[1],self.zoom_mouse_x-self.zoom_select_points[0],self.zoom_mouse_y-self.zoom_select_points[1],pen=self.zoompen)
        
        # mask tab
            
        if self.ui.tabWidget.currentIndex() == 1:
            if (event.type() == QEvent.MouseButtonPress and QMouseEvent.button(event) == 1):
                click = self.ui.graphicsView_mask.mapToScene(event.pos())
                if self.ui.add_polygon.isChecked() or self.ui.remove_polygon.isChecked():
                    #self.polygon_points.append([numpy.int(click.x()),numpy.int(click.y())])
                    self.polygon_points.append([click.x(),click.y()])
                    if len(self.polygon_points) > 1:
                        self.mask_scene.addLine(self.polygon_points[-1][0],self.polygon_points[-1][1],self.polygon_points[-2][0],self.polygon_points[-2][1],pen=self.maskpen)
                
                if self.ui.add_pixels.isChecked():
                    self.masked_data[numpy.int(click.y()),numpy.int(click.x())] = 0
                    self.show_masked()
                   
                if self.ui.remove_pixels.isChecked():
                    self.masked_data[numpy.int(click.y()),numpy.int(click.x())] = 1
                    self.show_masked()
                
                if self.ui.mask_zoom_select.isChecked():
                    self.mask_zoom_into_select(click.x(),click.y())
         
            if event.type() == QEvent.MouseMove:
                if (event.buttons() == Qt.NoButton and self.im4mask != ''):
                    click = self.ui.graphicsView_mask.mapToScene(event.pos())
                    self.mask_mouse_x = click.x()
                    self.mask_mouse_y = click.y()
                    if (self.mask_mouse_y >= 0  and self.mask_mouse_x >= 0) :
                        try:
                            self.ui.graphicsView_mask.setToolTip("I: {}".format(self.im4mask_data[numpy.int(self.mask_mouse_y),numpy.int(self.mask_mouse_x)]))
                            #self.ui.graphicsView_mask.setToolTip("{} x {}".format(numpy.int(click.x()),numpy.int(click.y())))
                        except:
                            self.ui.graphicsView_mask.setToolTip('e1')
                    else:
                        self.ui.graphicsView_mask.setToolTip('e2')
                    
                    if self.ui.add_polygon.isChecked() or self.ui.remove_polygon.isChecked():
                        if len(self.polygon_points) > 0:
                            if self.track_mouse_line != '':
                                    self.mask_scene.removeItem(self.track_mouse_line)
                            self.track_mouse_line = self.mask_scene.addLine(self.polygon_points[-1][0],self.polygon_points[-1][1],self.mask_mouse_x,self.mask_mouse_y,pen=self.maskpen)
            
            if event.type() == QEvent.Wheel:
            #     
            # zoom only with crtl+wheel:
            #
                
                if modifiers == Qt.ControlModifier:
                    if event.delta() > 0 and self.ui.zoom_mask.value()<5000:
                        zoomFactor = min(1.2,5000/(self.ui.zoom_mask.value()))
                    elif event.delta() < 0 and self.ui.zoom_mask.value()>75:
                        zoomFactor = max(1/1.2,75/(self.ui.zoom_mask.value()))
                    else:
                        zoomFactor = 1
                    
                    if zoomFactor != 1:
                        self.ui.graphicsView_mask.scale(zoomFactor, zoomFactor)
                        self.ui.zoom_mask.setValue(self.ui.zoom_mask.value()*zoomFactor)
                    
            # draw rectangle for zooming-in:
            if event.type() == QEvent.MouseMove:
                if event.buttons() == Qt.NoButton and self.ui.mask_zoom_select.isChecked() == True and self.mask_zoom_select_points != []:
                    click = self.ui.graphicsView_mask.mapToScene(event.pos())
                    self.zoom_mouse_x = click.x()
                    self.zoom_mouse_y = click.y()
                    if self.mask_zoom_pol != '':
                        self.mask_scene.removeItem(self.mask_zoom_pol)
                    self.mask_zoom_pol = self.mask_scene.addRect(self.mask_zoom_select_points[0],self.mask_zoom_select_points[1],self.zoom_mouse_x-self.mask_zoom_select_points[0],self.zoom_mouse_y-self.mask_zoom_select_points[1],pen=self.zoompen)
        
                        
        # block ctrl+wheel from forwarding
        if event.type() == QEvent.Wheel and modifiers == Qt.ControlModifier:
            return True
        else:
            return QWidget.eventFilter(self, source, event)
     
    ## qcalib_tab
    
    def find_fol_agbe(self):
        ''' find data and analysis folders
        find agbe file in data folder
        if agbe no found, set 10x10 gray pixels as image
        '''
        c_user = getpass.getuser()
        if  os.path.isdir('../data/'):
            self.data_folder = os.path.normpath(os.path.abspath('../data/'))
            self.analysis_folder = os.path.normpath(os.path.abspath('.'))
        elif c_user == 'staff':
            #self.last_folder =  max(glob.glob('/mnt/home/staff/UserSetup/*/'), key=os.path.getmtime)
            self.last_folder =  max(glob.glob('/mnt/home/staff/UserSetup/2*/'))
            if os.path.isdir(os.path.join(self.last_folder,'data')):
                self.data_folder = os.path.join(self.last_folder,'data')
            else:
                self.data_folder = self.last_folder
        else:
            try:
                self.last_folder =  max(glob.glob(os.path.expanduser('~/*/')), key=os.path.getmtime)
                self.no_data = 1
                while self.no_data == 1:
                    if os.path.isdir(os.path.join(self.last_folder,'data')):
                            self.data_folder = os.path.realpath(os.path.join(self.last_folder,'data'))
                            self.no_data = 0
                    else:
                        try:
                            self.last_folder_tmp = max(glob.glob(os.path.join(self.last_folder,'*/')), key=os.path.getmtime)
                            self.last_folder = self.last_folder_tmp
                        except:
                            # no more subfolders
                            self.data_folder = os.path.realpath(self.last_folder)
                            self.no_data = 0
            except:
                pass
        
        if self.data_folder != '' and self.analysis_folder == '':
            if os.path.isdir(os.path.normpath(os.path.join(self.data_folder,'../analysis/'))):
                self.analysis_folder = os.path.normpath(os.path.join(self.data_folder,'../analysis/'))
            else:
                self.analysis_folder = self.data_folder
        
        self.agbe_list=[]
        rrr = re.compile('.*[Aa][Gg][Bb][Ee].*0+1\.tif')
        for self.file in os.listdir(self.data_folder):
            if rrr.match(self.file):
                self.agbe_list.append(os.path.join(self.data_folder,self.file))
        
        self.agbe_list_s = sorted(self.agbe_list,key=lambda f: os.stat(f).st_mtime,reverse=True)
        if len(self.agbe_list_s) > 0:
            self.agbe_path = self.agbe_list_s[0]
            self.agbe_file= os.path.split(self.agbe_path)[1]
        else:
            self.agbe_file = ''
            self.empty_image()
        
        self.integmpp = os.path.join(self.analysis_folder,'integ.mpp')
        
        try:
            self.expname = str(os.path.split(self.analysis_folder)[1].split('_')[1:])
            if self_expname == []:
                self.expname = ''
        except:
            self.expname = ''
        
        self.cx1 = ''
        self.cx2 = ''    
        self.angles_list = []
        self.angles_width = []
        
        self.read_agbe()
        self.show_agbe()
        self.update_footer()
        self.update_buttons()
        
    def open_agbe(self):
        ''' manualy set agbe file 
        '''
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        if self.data_folder != '':
            self.start_folder = self.data_folder
        elif self.analysis_folder != '':
            if os.path.isdir(os.path.normpath(os.path.join(self.analysis_folder,'../data/'))):
                self.start_folder = os.path.normpath(os.path.join(self.analysis_folder,'../data/'))
            else:
                self.start_folder = self.analysis_folder
        else:
            self.start_folder = os.path.normpath(os.path.abspath('.'))
        #pyqt5
        #self.agbe_path = QFileDialog.getOpenFileName(self,"Select agbe file", self.start_folder,".tif files (*.tif *.tiff);;All Files (*)", options=options)[0]
        self.agbe_path = str(QFileDialog.getOpenFileName(self,"Select agbe file", self.start_folder,".tif files (*.tif *.tiff);;All Files (*)", options=options))
        self.data_folder,self.agbe_file = os.path.split(self.agbe_path)
        if self.analysis_folder == '':
            if os.path.isdir(os.path.normpath(os.path.join(self.data_folder,'../analysis/'))):
                self.analysis_folder = os.path.normpath(os.path.join(self.data_folder,'../analysis/'))
            else:
                self.analysis_folder = self.data_folder
        
        self.integmpp = os.path.join(self.analysis_folder,'integ.mpp')
        
        try:
            self.expname = str(os.path.split(self.analysis_folder)[1].split('_')[1:])
            if self_expname == []:
                self.expname = ''
        except:
            self.expname = ''
        
        

        self.cx1 = ''    
        self.cx2 = ''
        self.angles_list = []
        self.angles_width = []
        
        self.read_agbe()
        self.show_agbe()
        self.update_buttons()
        self.update_footer()

    
    def set_analysis(self):
        ''' manualy set analysis folder
        '''
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        if os.path.isdir(os.path.normpath(os.path.join(self.data_folder,'../analysis/'))):
            self.start_folder = os.path.normpath(os.path.join(self.data_folder,'../analysis/'))
        elif self.data_folder != '':
            self.start_folder = self.data_folder
        elif self.analysis_folder != '':
            self.start_folder = self.analysis_folder
        else:
            self.start_folder = os.path.normpath(os.path.abspath('.'))
        self.analysis_folder = str(QFileDialog.getExistingDirectory(self,'Select analysis folder',self.start_folder,QFileDialog.ShowDirsOnly))
        self.integmpp = os.path.join(self.analysis_folder,'integ.mpp')
        self.update_footer()

    def read_agbe(self):
        ''' open agbe file
        '''
        app.setOverrideCursor(QCursor(Qt.WaitCursor))

        if os.path.isfile(os.path.join(self.data_folder,self.agbe_file)):
            self.agbe_2d_im = Image.open(os.path.join(self.data_folder,self.agbe_file)).convert('I')
            self.agbe_2d = numpy.array(self.agbe_2d_im)
            self.gray_data = self.agbe_2d.astype('int32')
            self.gray_data_log = numpy.log(numpy.clip(self.gray_data,0.01,None))
            #self.gray_data_orig = self.agbe_2d.astype('int32')
            self.agbe_size_v = int(self.agbe_2d.shape[0])
            self.agbe_size_h = int(self.agbe_2d.shape[1])
            
            #self.agbe_data_max = numpy.amax(self.gray_data)
            self.agbe_data_max = 1.3*heapq.nlargest(5,self.gray_data.flatten())[-1]
            #self.agbe_data_min = numpy.amin(self.gray_data)
            self.agbe_data_min = -10
            #self.agbe_data_log_max = numpy.amax(self.gray_data_log)
            self.agbe_data_log_max = 1+heapq.nlargest(5,self.gray_data_log.flatten())[-1]
            #self.agbe_data_log_min = numpy.amin(self.gray_data_log)
            self.agbe_data_log_min = -1
            
            if os.path.isfile(os.path.join(self.data_folder,self.agbe_file[:-3]+'prp')):
                with open(os.path.join(self.data_folder,self.agbe_file[:-3]+'prp')) as agbe_prp:
                    for line in agbe_prp.readlines():
                        if 'Beam energy' in line:
                            e_kev=float(line.split('=')[1].split(' ')[0])/1000
                        if 'Pipe length' in line:
                            det_dist=float(line.split("=")[1].split(' ')[0])+150
                        if 'Detector mode' in line:
                            try:
                                # if detector mode is a number ('0' or '1' in .prp) -> Rayonix
                                int(line.split('=')[1][0])
                                self.det='mx'
                                self.det_bin = int(6144/self.agbe_size_h)
                                px_size = 225000/6144*self.det_bin
                                self.ui.px_size.setValue(px_size)
                            except:
                                # if detector mode is not a number ('Pilatus' in .prp) -> Pilatus
                                self.det='pil'
                                px_size = 172
                                self.ui.px_size.setValue(px_size)
                        if 'I_2' in line:
                            self.i2 = float(line.split('=')[1].split(' ')[0])
                self.ui.energy_value.setValue(e_kev)
                self.ui.app_distance_value.setValue(det_dist)
                #self.app_first_ring()
            
            self.show_angles()
        app.restoreOverrideCursor()

    def app_first_ring(self):
        ''' calulate approximate raidus of the first agbe ring
            px_size is in meters
        '''
        if (self.ui.app_distance_value.value() != 0 and self.ui.energy_value.value() !=0 and self.ui.px_size.value() != 0):
            
            agbe_ring_est = (self.ui.app_distance_value.value()/1000)*numpy.tan(2*numpy.arcsin((12.3984/self.ui.energy_value.value())*(2*numpy.pi/self.d_agbe)/(4*numpy.pi)))/(self.ui.px_size.value()*1e-6)
            # if if ring position < 100px, use a different ring
            ring_no = int(numpy.ceil(100/agbe_ring_est))
            
            if self.det == 'mx':
                px_peak_search_rg = int(min(160/self.det_bin,agbe_ring_est/2))
            else:
                px_peak_search_rg = int(min(40,agbe_ring_est/2))
            
            
            self.ui.radius1_px.setValue(agbe_ring_est)
            self.ui.radius1_width.setValue(px_peak_search_rg)
            self.ui.ring_num.setValue(ring_no)
        else:
            self.ui.radius1_px.setValue(0)
        
    def find_center_1(self):
        ''' find center (rough) using autocorrelation
        '''
        app.setOverrideCursor(QCursor(Qt.WaitCursor))
        data_corr = signal.fftconvolve(self.agbe_2d,self.agbe_2d)
        cx_crmax, cy_crmax = numpy.unravel_index(numpy.argmax(data_corr), data_corr.shape)
        self.cx1 = (1.0*cy_crmax+1)/2
        self.cy1 = (1.0*cx_crmax+1)/2
        self.add_target()
        #center_pix = self.ui.graphicsView.mapToScene(self.ui.graphicsView.viewport().rect()).boundingRect().center()
        self.cx2 = ''
        self.show_angles()
        #self.ui.graphicsView.centerOn(center_pix)
        self.update_footer()
        self.update_buttons()
        
        app.restoreOverrideCursor()
        
    def set_center_1(self):
        ''' set center rough (manual) button is clicked
        '''
        if self.ui.set_angles_man.isChecked():
            self.ui.set_angles_man.setChecked(False)
        elif self.ui.zoom_select.isChecked():
            self.ui.zoom_select.setChecked(False)
        elif self.ui.set_center_1.isChecked():
            app.setOverrideCursor(QCursor(Qt.ArrowCursor))
            self.ui.graphicsView.setDragMode(QGraphicsView.NoDrag)
        self.status_message()
    
    def set_center_point(self,xx,yy):
        ''' get the center point of zoomed-in graph
        '''
        self.cx1 = round(xx,1)
        self.cy1 = round(yy,1)
        #self.add_target()
        #center_pix = self.ui.graphicsView.mapToScene(self.ui.graphicsView.viewport().rect()).boundingRect().center()
        self.cx2 = ''
        self.show_angles()
        #self.ui.graphicsView.centerOn(center_pix)
        self.update_footer()
        self.update_buttons()
        
    def set_angles_auto(self):
        ''' set angles for rign search
        '''
        self.angles_list = 45 + numpy.arange(-180,180,90)
        self.angles_width = numpy.array([self.ui.angles_val.value()]*4)
        self.show_angles()
        
    def set_angles_man(self):
        ''' select angles for ring search
        '''
        if self.ui.set_center_1.isChecked():
            self.ui.set_center_1.setChecked(False)
        elif self.ui.zoom_select.isChecked():
            self.ui.zoom_select.setChecked(False)
        elif self.ui.set_angles_man.isChecked():
            app.setOverrideCursor(QCursor(Qt.ArrowCursor))
            self.ui.graphicsView.setDragMode(QGraphicsView.NoDrag)
        self.status_message()

    def set_angles_point(self,xx,yy):
        '''ads or removes an angle
        '''
        ang = numpy.degrees(numpy.arctan2(yy-self.cy1,xx-self.cx1))
        dd = []
        if len(self.angles_list)>0:
            for ind in range(len(self.angles_list)):
                if (numpy.abs(self.angles_list[ind] - ang)< ((self.ui.angles_val.value()+self.angles_width[ind])*0.5) ):
                    dd.append(ind)
            
        if dd != []:
            self.angles_list = numpy.delete(self.angles_list,dd)
            self.angles_width = numpy.delete(self.angles_width,dd)
        else:
            self.angles_list = numpy.append(self.angles_list,ang)
            self.angles_width = numpy.append(self.angles_width,self.ui.angles_val.value())
        
        self.show_angles()
    
    def refine_center(self):
        ''' perform a routine to refine center position
        '''
        app.setOverrideCursor(QCursor(Qt.WaitCursor))
        cen_fold = os.path.join(self.analysis_folder,'cen')
        integcenmpp = os.path.join(self.analysis_folder,'integ_cen.mpp')
        ang_mask_path = os.path.join(self.analysis_folder,'angular_mask.tif')
        agbe_datfile = os.path.join(self.analysis_folder,self.agbe_file[:-3]+'dat')
        logfile = time.strftime("%Y%m%d")+'_cen.log'
        
        if not os.path.isdir(cen_fold):
            os.makedirs(cen_fold)
        
        # create a new integcenmpp file
        
        ff = open(integcenmpp,'w')
        ff.write('-l '+logfile+'\n')
        ff.write('-c {0:.3f} {1:.3f}\n'.format(self.cx1,self.agbe_size_v-self.cy1))
        if self.i2 > 10:
            ff.write('-i yes i2 1000\n')
        #ff.write('-r yes 1.5\n')
        ff.write('-z yes 3.5\n')
        if self.det == 'pil':
            ff.write('-o 0\n')
        ff.write('-m yes {}\n'.format(ang_mask_path))
        ff.write('-f {}\n'.format(os.path.join(self.data_folder,self.agbe_file)))
        ff.close()
        
        # How many times to perform refinement
        repeats = 1
        
        ref_cen = numpy.zeros([repeats+1,2])
        ref_cen[0,:] = [self.cx1,self.agbe_size_v-self.cy1]
        
        num_of_ang = self.angles_list.size
        
        agbe_ring_dir = numpy.zeros([repeats,num_of_ang])
        #os.chdir(self.analysis_folder)
        
        for rr in range(1,repeats+1):
            for i in range(num_of_ang):
                for line in fileinput.input(integcenmpp, inplace = 1):
                    line = re.sub('^-c \d+(\.\d+)? \d+(\.\d+)?','-c '+str(ref_cen[rr-1,0])+' '+str(ref_cen[rr-1,1]),line)
                    sys.stdout.write(line)
                make_ang_mask(self.agbe_size_h,self.agbe_size_v,ref_cen[rr-1,0],ref_cen[rr-1,1],self.angles_list[i],self.angles_width[i],ang_mask_path)
                pp = subprocess.Popen(['sastool',integcenmpp],cwd = self.analysis_folder)
                pp.communicate()
                agbe_1d = numpy.genfromtxt(agbe_datfile)
                shutil.move(agbe_datfile,os.path.join(cen_fold,self.agbe_file[:-4]+'_r'+str(rr)+'_d'+str(i)+'.dat'))
                agbe_max_px = agbe_1d[int(self.ui.ring_num.value()*self.ui.radius1_px.value()-self.ui.radius1_width.value()):int(self.ui.ring_num.value()*self.ui.radius1_px.value()+self.ui.radius1_width.value()),1].argmax() + self.ui.ring_num.value()*self.ui.radius1_px.value()-self.ui.radius1_width.value()
                coeff, var_matrix = fit1dgauss(agbe_1d,agbe_max_px,self.ui.radius1_px.value()/2,self.det_bin)
                agbe_ring_dir[rr-1,i] = coeff[1]
                if rr == repeats:
                    agbe_1d[:,1] = numpy.maximum(agbe_1d[:,1],.1)
                    #plt.plot(agbe_1d[:,0],numpy.log(agbe_1d[:,1]))
            
            #x_del = 0
            #y_del = 0
            #for j in range(0,4):
            #    x_del = x_del+agbe_ring_dir[rr-1,j]*numpy.cos(numpy.radians(start_ang+90*j))/2
            #    y_del = y_del+agbe_ring_dir[rr-1,j]*numpy.sin(numpy.radians(start_ang+90*j))/2
            ring_points_x = numpy.zeros(num_of_ang)
            ring_points_y = numpy.zeros(num_of_ang)
            # set ring positions or circle fitting
            for j in range(num_of_ang):
                ring_points_x[j] = ref_cen[rr-1,0]+agbe_ring_dir[rr-1,j]*numpy.cos(numpy.radians(self.angles_list[j]))
                ring_points_y[j] = ref_cen[rr-1,1]-agbe_ring_dir[rr-1,j]*numpy.sin(numpy.radians(self.angles_list[j]))
            
            rx_m = numpy.mean(ring_points_x)
            ry_m = numpy.mean(ring_points_y)
            
            center_2, ier = leastsq(f_2,(rx_m,ry_m),args=(ring_points_x,ring_points_y))
            
            ref_cen[rr,0] = center_2[0]
            ref_cen[rr,1] = center_2[1]
            
        shutil.move(integcenmpp,os.path.join(cen_fold,'integ_cen.mpp'))
        shutil.move(ang_mask_path,os.path.join(cen_fold,os.path.split(ang_mask_path)[1]))
        shutil.move(os.path.join(self.analysis_folder,logfile),os.path.join(cen_fold,logfile))
        shutil.move(os.path.join(self.analysis_folder,self.agbe_file[:-3]+'log'),os.path.join(cen_fold,self.agbe_file[:-3]+'log'))
        shutil.move(os.path.join(self.analysis_folder,self.agbe_file[:-3]+'tot'),os.path.join(cen_fold,self.agbe_file[:-3]+'tot'))
        self.cx2 = ref_cen[-1,0]
        self.cy2 = self.agbe_size_v-ref_cen[-1,1]
        
        self.show_angles()
        self.update_footer()
        self.update_buttons()
    
        app.restoreOverrideCursor()
        
    def q_calib(self):
        ''' q calibration
        '''
        self.integmpp = os.path.join(self.analysis_folder,'integ.mpp')
        no_mask = os.path.join(self.analysis_folder,'no_mask.tif')
        agbe_datfile = os.path.join(self.analysis_folder,self.agbe_file[:-3]+'dat')
        # redefine self.ring_px, lcear the values if it was already calculated
        self.ring_px = numpy.array([0])
        
        # Check if the mask file exists
        mask_msk = ''
        mask_tif = ''
        try:
            mask_tif = max(glob.iglob(os.path.join(self.analysis_folder,'mask*.tif')), key=os.path.getctime)
        except:
            pass
        
        if mask_tif == '':
            try:
                mask_msk = max(glob.iglob(os.path.join(self.analysis_folder,'*msk')), key=os.path.getctime)
            except:
                pass
    
        if mask_msk != '':
            #os.chdir(self.analysis_folder)
            pp=subprocess.Popen(['msk2tif.py',mask_msk],cwd = self.analysis_folder)
            pp.communicate()
            mask_tif = mask_msk[:-3]+'tif'
        
        
        # Prepare integ.mpp
        if os.path.exists(self.integmpp):
            # change existing file
            for line in fileinput.input(self.integmpp, inplace = 1):
                line = re.sub('^-c \d+(\.\d+)? \d+(\.\d+)?','-c {0:.3f} {1:.3f}'.format(self.cx2,self.agbe_size_v-self.cy2),line)
                if mask_tif!='':
                    line = re.sub('^#*-m.*$','-m yes '+os.path.join(os.path.realpath('.'),mask_tif),line)
                else:
                    line = re.sub('^#*-m.*$','-m yes {}'.format(no_mask),line)
                if self.expname == '':
                    line = re.sub('^-l.*$','-l '+time.strftime("%Y%m%d")+'.log',line)
                else:
                    line = re.sub('^-l.*$','-l '+time.strftime("%Y%m%d")+'_'+self.expname+'.log',line)
                line = re.sub('^-q yes','#-q yes',line)
                line = re.sub('^-q wide','#-q wide',line)
                line = re.sub('^-s','#-s',line)
                line = re.sub('^-f','#-f',line)
                if 'i2' in line:
                    if self.i2 < 10:
                        line = re.sub('^-i','#-i',line)
                if (not line.startswith('-a') and not re.match(r'^\s*$', line)):        
                    sys.stdout.write(line)
        
            ff = open(self.integmpp,'a')
            ff.write('-f '+self.agbe_path+'\n')
            ff.close()
        

        else:
            # make a new file
            ff = open(self.integmpp,'w')
            if self.expname == '':
                ff.write('-l '+time.strftime("%Y%m%d")+'.log\n')
            else:
                ff.write('-l '+time.strftime("%Y%m%d")+'_'+self.expname+'.log\n')
            ff.write('-c {0:.3f} {1:.3f}\n'.format(self.cx2,self.agbe_size_v-self.cy2))
            ff.write('#-q yes 0 0\n')
            if self.i2 > 10:
                ff.write('-i yes i2 500000\n')
            ff.write('-r yes 1.5\n')
            ff.write('#-s yes\n')
            if mask_tif != '':
                ff.write('-m yes {} \n'.format(mask_tif))
            else:
                ff.write('-m yes {0}\n'.format(no_mask))
            ff.write('-z yes 3.5\n')
            if self.det == 'pil':
                ff.write('-o 0\n')
            ff.write('-f {} \n'.format(self.agbe_path))
            ff.close()
        
        if mask_tif=='':
            make_ang_mask(self.agbe_size_h,self.agbe_size_v,1,1,-45,90,no_mask)

        
        pp = subprocess.Popen(['sastool',self.integmpp],cwd = self.analysis_folder)
        pp.communicate()
        
        agbe_1d = numpy.genfromtxt(agbe_datfile)
        

        
        # find the position of the first agbe ring
        rad_max = int(agbe_1d[int(self.ui.radius1_px.value()-self.ui.radius1_width.value()):int(self.ui.radius1_px.value()+1.5*self.ui.radius1_width.value()),1].argmax()+self.ui.radius1_px.value()-self.ui.radius1_width.value())
        
        coeff, var_matrix = fit1dgauss(agbe_1d,rad_max,self.ui.radius1_px.value()/2,self.det_bin)
        self.ring_px = numpy.append(self.ring_px,coeff[1])
        
        
        for jj in range(2,6):
            if self.det == 'mx':
                px_peak_search_rg = int(min(160/self.det_bin,jj*self.ui.radius1_px.value()/2))
            else:
                px_peak_search_rg = int(min(40,jj*self.ui.radius1_px.value()/2))
            px_num_g = int(px_peak_search_rg/2)
            if agbe_1d.shape[0] > int(jj * self.ring_px[1] + px_num_g):
                try:                  
                    coeff, var_matrix = fit1dgauss(agbe_1d,jj*self.ring_px[jj-1]/(jj-1),px_num_g,self.det_bin)
                    self.ring_px = numpy.append(self.ring_px,coeff[1])
                except:
                    pass
        
        # calculate distance
        distance = []
        for yy in range(1,len(self.ring_px)):
            dd = self.ring_px[yy]*(self.ui.px_size.value()*1e-6)/numpy.tan(2*yy*numpy.arcsin((12.3984/self.ui.energy_value.value())/(2*self.d_agbe)))
            distance = numpy.append(distance,dd)
            print("ring {}: distance {} mm".format(yy,dd))
        if len(distance)>3:
            dist_start = 1
        else:
            dist_start = 0
        
        
        self.calc_dist = 1000*numpy.mean(distance[dist_start:])
            
        self.show_angles()
            
    def save_integ(self):
        ''' save integ.mpp
        '''
        no_mask = os.path.join(self.analysis_folder,'no_mask.tif')

        # Check if the mask file exists
        mask_msk = ''
        mask_tif = ''
        try:
            mask_tif = max(glob.iglob(os.path.join(self.analysis_folder,'mask*.tif')), key=os.path.getctime)
        except:
            pass
        
        if mask_tif == '':
            try:
                mask_msk = max(glob.iglob(os.path.join(self.analysis_folder,'*msk')), key=os.path.getctime)
            except:
                pass
    
        if mask_msk != '':
            #os.chdir(self.analysis_folder)
            pp=subprocess.Popen(['msk2tif.py',mask_msk],cwd = self.analysis_folder)
            pp.communicate()
            mask_tif = mask_msk[:-3]+'tif'
            
        #if mask_tif=='':
        #    make_ang_mask(self.agbe_size_h,self.agbe_size_v,1,1,-45,90,no_mask)
        
        
        # add q info into integmpp
        qinfo = ''
        for jj in range(len(self.ring_px)):
            qinfo = qinfo+' '+'{:.2f}'.format(self.ring_px[jj])+' '+str(jj*0.1076)
         
        if not os.path.exists(self.integmpp):
            ff = open(self.integmpp,'w')
            if self.expname == '':
                ff.write('-l '+time.strftime("%Y%m%d")+'.log\n')
            else:
                ff.write('-l '+time.strftime("%Y%m%d")+'_'+self.expname+'.log\n')
            ff.write('-c {0:.3f} {1:.3f}\n'.format(self.cx2,self.agbe_size_v-self.cy2))
            ff.write('-q yes{}\n'.format(qinfo))
            if self.i2 > 10:
                ff.write('-i yes i2 500000\n')
            ff.write('-r yes 1.5\n')
            ff.write('#-s yes\n')
            if mask_tif != '':
                ff.write('-m yes {} \n'.format(mask_tif))
            else:
                ff.write('-m yes {0}\n'.format(no_mask))
            ff.write('-z yes 3.5\n')
            if self.det == 'pil':
                ff.write('-o 0\n')
            ff.write('-f {} \n'.format(self.agbe_path))
            ff.close() 
        else: 
            for line in fileinput.input(self.integmpp, inplace = 1):
                line = re.sub('.*-q .*','-q yes'+qinfo,line)
                sys.stdout.write(line)
    
        
        if self.calc_dist < 1000:
            for line in fileinput.input(self.integmpp, inplace = 1):
                if '-q' in line:
                    line = '-q wide {0:.0f} {1}\n'.format(self.ui.energy_value.value()*1000,self.calc_dist)
                sys.stdout.write(line)
        
        pp = subprocess.Popen(['sastool',self.integmpp],cwd = self.analysis_folder)
        pp.communicate()
        
        # display message
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setText("integ.mpp saved")
        msg.setWindowTitle("All done")
        msg.setStandardButtons(QMessageBox.Ok)
        msg.exec_()

    
    def show_agbe(self):
        ''' show agbe image
        '''
        if self.gray_data.any() != 0:
            if self.ui.q_log.isChecked():
                self.pix_data = QPixmap.fromImage(ImageQt(Image.fromarray(gray_to_color(self.gray_data_log,self.scale_min*(2*self.agbe_data_log_max-self.agbe_data_log_min)/1000 + self.agbe_data_log_min,self.scale_max*(2*self.agbe_data_log_max-self.agbe_data_log_min)/1000 + self.agbe_data_log_min))))
            else:
                self.pix_data = QPixmap.fromImage(ImageQt(Image.fromarray(gray_to_color(self.gray_data,self.scale_min*(2*self.agbe_data_max-self.agbe_data_min)/1000 + self.agbe_data_min,self.scale_max*(2*self.agbe_data_max-self.agbe_data_min)/1000 + self.agbe_data_min))))
            self.show_image.setPixmap(self.pix_data)
            self.scene.setSceneRect(-100,-100,200+self.agbe_size_h,200+self.agbe_size_v)
            self.update_buttons()
            self.ui.graphicsView.fitInView(self.show_image,Qt.KeepAspectRatio)
            # get the initial zoom after fitinView, correct for the scroolbars (1.02031930334 or 1.02982731554 )
            if self.ui.zoom_slider.value()>100:
                self.init_zoom = self.ui.graphicsView.transform().m11()*1.02031930334
                #self.init_zoom = self.ui.graphicsView.transform().m11()*1.02982731554
            else:
                self.init_zoom = self.ui.graphicsView.transform().m11()
            
            # apply the current zoom from the slider value if different than 1
            if self.ui.zoom_slider.value()!=100:
                self.ui.graphicsView.scale(self.ui.zoom_slider.value()*1.02031930334/100,self.ui.zoom_slider.value()*1.02031930334/100)
                #self.ui.graphicsView.scale(self.ui.zoom_slider.value()*1.02982731554,self.ui.zoom_slider.value()*1.02982731554)
            
            
            
    def show_angles(self):
        ''' show center/angles/rings when needed
        '''
        # remove old center/angles/ring if exist
        ooo = self.scene.items()
        for pp in ooo:
            if 'Pixmap' not in str(pp):
                self.scene.removeItem(pp)
        
        
        if self.cx2 != '':
            self.add_target2()
            
            if len(self.ring_px)>1:
                pen = QPen(QColor(100, 100, 100, 200))
                pen.setWidth(5)
                brush = QBrush(QColor(0,0,0,0))
                for rn in numpy.arange(1,len(self.ring_px)):
                    self.scene.addEllipse(numpy.double(self.cx2)-self.ring_px[rn], numpy.double(self.cy2)-self.ring_px[rn], 2*self.ring_px[rn], 2*self.ring_px[rn], pen, brush)
            
        else:
            # add center target
            if self.cx1 != '':
                self.add_target()
            
                # add angles
                if len(self.angles_list) >0:
                    angles_area = QPainterPath()
                    rmax = self.ui.ring_num.value()*self.ui.radius1_px.value()+self.ui.radius1_width.value()+40
                    for aa,jj in zip(self.angles_list,self.angles_width):
                        angles_area.addPolygon(QPolygonF([QPointF(numpy.double(self.cx1),numpy.double(self.cy1)), QPointF(numpy.double(self.cx1+rmax*numpy.cos(numpy.deg2rad(aa+jj))),numpy.double(self.cy1+rmax*numpy.sin(numpy.deg2rad(aa+jj)))), QPointF(numpy.double(self.cx1+rmax*numpy.cos(numpy.deg2rad(aa-jj))),numpy.double(self.cy1+rmax*numpy.sin(numpy.deg2rad(aa-jj))))]))
                        
                        #angles_area.addPolygon(QPolygonF([QPointF(numpy.double(self.cx1),numpy.double(self.cy1)), QPointF(*numpy.double(find_edge(self.agbe_size_h,self.agbe_size_v,self.cx1,self.cy1,aa+jj))),QPointF(*numpy.double(find_edge(self.agbe_size_h,self.agbe_size_v,self.cx1,self.cy1,aa-jj)))]))
                    
                    
                    self.scene.addPath(angles_area, QPen(QColor(0,0,255,150)), QBrush(QColor(0,0,255, 150)))
                    
                    
                # add agbe ring search area
                if self.ui.radius1_px.value() > 0 and self.cx1 != '':
                    ring_search = QPainterPath()
                    rmax = self.ui.ring_num.value()*self.ui.radius1_px.value()+self.ui.radius1_width.value()
                    rmin = self.ui.ring_num.value()*self.ui.radius1_px.value()-self.ui.radius1_width.value()
                    ring_search.addEllipse(numpy.double(self.cx1)-rmax, numpy.double(self.cy1)-rmax, 2*rmax, 2*rmax)
                    ring_search.addEllipse(numpy.double(self.cx1)-rmin, numpy.double(self.cy1)-rmin, 2*rmin, 2*rmin)
                    
                    self.scene.addPath(ring_search, QPen(QColor(255,0,0, 100)), QBrush(QColor(255,0,0, 100)))

        # update footer/buttons
        self.update_footer()
        self.update_buttons()
            
        
    def do_all(self):
        ''' find center automatically
        '''
        self.find_fol_agbe()
        if self.ui.energy_value.value() != 0 and self.ui.app_distance_value.value != 0:
            self.app_first_ring()
            self.find_center_1()
            self.set_angles_auto()
            self.refine_center()
            self.q_calib()
            self.save_integ()
            
            
        self.update_footer()
        self.update_buttons()
        
    def empty_image(self):
        ''' empty image to show ig=f there is no agbe
        '''
        self.gray_data = numpy.array(0)
        self.empty = QPixmap(10,10)
        self.empty.fill(QColor(200,200,200))
        self.scene.clear()
        self.create_scene()
        self.show_image.setPixmap(self.empty)
        self.ui.graphicsView.fitInView(self.show_image,Qt.KeepAspectRatio)
    
    def add_target(self):
        ''' add target at the rough center
        '''
        step = numpy.ceil(self.agbe_size_v/500)
        rrr = numpy.arange(step*6,0,-step)
        for ttt in numpy.arange(len(rrr)):
            cc = 255*((-1)**ttt + 1)/2
            pen = QPen(QColor(cc, cc, cc, 50))
            brush = QBrush(pen.color())
            self.scene.addEllipse(numpy.double(self.cx1)-rrr[ttt], numpy.double(self.cy1)-rrr[ttt], 2*rrr[ttt], 2*rrr[ttt], pen, brush)
            
    def add_target2(self):
        ''' add target at the refined center
        '''
        step = numpy.ceil(self.agbe_size_v/500)
        rrr = numpy.arange(step*6,0,-step)
        for ttt in numpy.arange(len(rrr)):
            cc = 255*((-1)**ttt + 1)/2
            pen = QPen(QColor(cc, cc, 255, 150))
            brush = QBrush(pen.color())
            self.scene.addEllipse(numpy.double(self.cx2)-rrr[ttt], numpy.double(self.cy2)-rrr[ttt], 2*rrr[ttt], 2*rrr[ttt], pen, brush)
        
        
    
    def setup_change(self):
        ''' what to do if setup parameters change
        '''
        if self.ui.energy_value.value() != 0 and self.ui.app_distance_value.value != 0:
            self.app_first_ring()
            
    def zoom_select(self):
        if self.ui.set_center_1.isChecked():
            self.ui.set_center_1.setChecked(False)
        elif self.ui.set_angles_man.isChecked():
            self.ui.set_angles_man.setChecked(False)
        elif self.ui.zoom_select.isChecked():
            app.setOverrideCursor(QCursor(Qt.ArrowCursor))
            self.ui.graphicsView.setDragMode(QGraphicsView.NoDrag)
        
        if not self.ui.mask_zoom_select.isChecked():
            self.zoom_select_points = []
            if self.zoom_pol != '':
                self.scene.removeItem(self.zoom_pol)
                self.zoom_pol = ''
        
        
        self.status_message()
        

    def zoom_into_select(self,xx,yy):
        if self.zoom_select_points == []:
            self.zoom_select_points.append(xx)
            self.zoom_select_points.append(yy)
        else:
            self.ui.graphicsView.fitInView(min(xx,self.zoom_select_points[0]),min(yy,self.zoom_select_points[1]),abs(xx-self.zoom_select_points[0]),abs(yy-self.zoom_select_points[1]),Qt.KeepAspectRatio)
            self.zoom_select_points = []
            self.ui.zoom_select.setChecked(False)
            self.scene.removeItem(self.zoom_pol)
            self.zoom_pol = ''
            zoom = int(100*self.ui.graphicsView.transform().m11()/self.init_zoom)
            if zoom <= 5000:
                self.ui.zoom_slider.setValue(zoom)
            else:
                self.ui.zoom_slider.setValue(5000)
                self.ui.graphicsView.scale(5000/zoom,5000/zoom)
            self.status_message()
        
    
    def zoom_changed(self):
        ''' zoom change
        '''
        # current zoom in respect to fitinView
        self.zoom = self.ui.graphicsView.transform().m11()/self.init_zoom
        # how much to zoom in this step
        self.zoom_factor = self.ui.zoom_slider.value()/(100*self.zoom)
        # make zoom
        self.ui.graphicsView.scale(self.zoom_factor, self.zoom_factor)
        
    
    def max_changed(self):
        ''' lut maximum change
        '''
        center_pix = self.ui.graphicsView.mapToScene(self.ui.graphicsView.viewport().rect()).boundingRect().center()
        self.scale_max = self.ui.max_slider.value()
        self.show_agbe()
        self.ui.graphicsView.centerOn(center_pix)
        
    def min_changed(self):
        ''' lut minimum change
        '''
        center_pix = self.ui.graphicsView.mapToScene(self.ui.graphicsView.viewport().rect()).boundingRect().center()
        self.scale_min = self.ui.min_slider.value()
        self.show_agbe()
        self.ui.graphicsView.centerOn(center_pix)        
    
    def q_reset_cm(self):
        #self.scale_min = 50
        #self.scale_max = 950
        self.ui.min_slider.setValue(50)
        self.ui.max_slider.setValue(950)
    
    def create_scene(self):
        ''' create scene in qgraphicsview in q_calib tab
        '''
        self.scene = QGraphicsScene(self.ui.graphicsView)
        self.ui.graphicsView.setScene(self.scene)
        self.show_image = QGraphicsPixmapItem()
        self.scene.addItem(self.show_image)
                       
        
    def update_buttons(self):
        ''' enable/disable buttons in q_calib tab, as needed
        '''
        if self.agbe_file != '':
            self.ui.find_center_1.setEnabled(True)
            self.ui.set_center_1.setEnabled(True)
            self.ui.energy_label.setEnabled(True)
            self.ui.app_distance_label.setEnabled(True)
            self.ui.radius1_px_label.setEnabled(True)
            self.ui.radius1_width_label.setEnabled(True)
            self.ui.energy_value.setEnabled(True)
            self.ui.app_distance_value.setEnabled(True)
            self.ui.radius1_px.setEnabled(True)
            self.ui.radius1_width.setEnabled(True)
            self.ui.ring_num_label.setEnabled(True)
            self.ui.ring_num.setEnabled(True)
            self.ui.px_size_label.setEnabled(True)
            self.ui.px_size.setEnabled(True)
            
        else:
            self.ui.find_center_1.setEnabled(False)
            self.ui.set_center_1.setEnabled(False)
            self.ui.set_center_1.setChecked(False)
            self.ui.energy_label.setEnabled(False)
            self.ui.app_distance_label.setEnabled(False)
            self.ui.radius1_px_label.setEnabled(False)
            self.ui.radius1_width_label.setEnabled(False)
            self.ui.energy_value.setEnabled(False)
            self.ui.app_distance_value.setEnabled(False)
            self.ui.radius1_px.setEnabled(False)
            self.ui.radius1_width.setEnabled(False)
            self.ui.ring_num_label.setEnabled(False)
            self.ui.ring_num.setEnabled(False)
            self.ui.px_size_label.setEnabled(False)
            self.ui.px_size.setEnabled(False)
            self.empty_image()
            self.ui.zoom_slider.setProperty("value", 100)
            self.ui.max_slider.setProperty("value", 255)
            self.ui.min_slider.setProperty("value", -2)
            self.cx1 = ''
            self.cy1 = ''
        
        if self.gray_data.any() != 0:
            self.ui.max_slider.setEnabled(True)
            self.ui.min_slider.setEnabled(True)
            self.ui.lut_max.setEnabled(True)
            self.ui.lut_min.setEnabled(True)
            self.ui.zoom_slider.setEnabled(True)
            self.ui.zoom_label.setEnabled(True)
            self.ui.zoom_select.setEnabled(True)
        
        else:  
            self.ui.max_slider.setEnabled(False)
            self.ui.min_slider.setEnabled(False)
            self.ui.lut_max.setEnabled(False)
            self.ui.lut_min.setEnabled(False)
            self.ui.zoom_slider.setEnabled(False)
            self.ui.zoom_label.setEnabled(False)
            self.ui.zoom_select.setEnabled(False)
        
        if self.cx1 != '' and self.ui.radius1_px.value() > 1:
            self.ui.angles_val.setEnabled(True)
            self.ui.angle_width_text.setEnabled(True)
            self.ui.set_angles_auto.setEnabled(True)
            self.ui.set_angles_man.setEnabled(True)
        else:
            self.ui.angles_val.setEnabled(False)
            self.ui.angle_width_text.setEnabled(False)
            self.ui.set_angles_auto.setEnabled(False)
            self.ui.set_angles_man.setEnabled(False)
            self.ui.set_angles_man.setChecked(False)
            
        if self.cx1 != 0 and len(self.angles_list)>2 and self.ui.radius1_px.value()>1:
            self.ui.find_center_2.setEnabled(True)
        else:
            self.ui.find_center_2.setEnabled(False)
         
        if self.cx2 != '':
            self.ui.q_calib.setEnabled(True)
            self.ui.save_integ.setEnabled(True)
        else:
            self.ui.q_calib.setEnabled(False)
            self.ui.save_integ.setEnabled(False)
        
    ## end of qcalib_tab
    
    ## mask_tab
    
    def open_im4mask(self):
        ''' open image(s) for mask drawing 
        '''
        options = QFileDialog.Options()
        # opening files works faster when using native dialog
        #options |= QFileDialog.DontUseNativeDialog
        if self.data_folder != '':
            self.start_folder = self.data_folder
        else:
            self.start_folder=os.path.normpath(os.path.abspath('.'))
            #self.start_folder = os.path.expanduser('~')
        
        #self.im4mask_path = str(QFileDialog.getOpenFileName(self,"Select a file", self.start_folder,".tif files (*.tif *.tiff);;All Files (*)", options=options))
        # open one or multiple images
        self.im4mask_list = QFileDialog.getOpenFileNames(self,"Select one or multiple files", self.start_folder,".tif files (*.tif *.tiff);;All Files (*)", options=options)
        
        # QFileDialog object method for selecting files
        #dlg = QFileDialog()
        #dlg.setFileMode(QFileDialog.ExistingFiles)
        #dlg.setFilter("tif files (*.tif)")
        #if dlg.exec_():
        #    self.im4mask_list = dlg.selectedFiles()
        
        
        
        #get info from the first image:
        self.im4mask_folder, self.im4mask = os.path.split(str(self.im4mask_list[0]))
        
        if self.data_folder == "":
            self.data_folder = self.im4mask_folder
        
        self.im4mask_data = numpy.array(Image.open(str(self.im4mask_list[0])).convert('I')).astype('int32')
        
        # open image(s)
        if len(self.im4mask_list)> 1:
            for ii in range(1,len(self.im4mask_list)):
            
                self.im4mask_data = self.im4mask_data + numpy.array(Image.open(str(self.im4mask_list[ii])).convert('I')).astype('int32')
                #self.im4mask_im = Image.open(ll).convert('I')
                #self.im4mask_data = self.im4mask_data + numpy.array(self.im4mask_im).astype('int32')
            self.im4mask_data = self.im4mask_data/len(self.im4mask_list)
        
        self.im4mask_size_v = int(self.im4mask_data.shape[0])
        self.im4mask_size_h = int(self.im4mask_data.shape[1])
        self.im4mask_data_log = numpy.log(numpy.clip(self.im4mask_data,0.01,None))
        #self.im4mask_data_max = numpy.amax(self.im4mask_data)
        self.im4mask_data_max = 1.3*heapq.nlargest(5,self.im4mask_data.flatten())[-1]
        #self.im4mask_data_min = numpy.amin(self.im4mask_data)
        self.im4mask_data_min = -10
        #self.im4mask_data_log_max = numpy.amax(self.im4mask_data_log)
        self.im4mask_data_log_max = 1+heapq.nlargest(5,self.im4mask_data_log.flatten())[-1]
        #self.im4mask_data_log_min = numpy.amin(self.im4mask_data_log)
        self.im4mask_data_log_min = -1
        
        
        # prepare mask and alpha channel(?) of the same size, if doesn't exist
        try:
            if self.im4mask_data.shape != self.masked_data.shape:
                self.masked_data = numpy.ones_like(self.im4mask_data).astype('uint16')

        except:
            self.masked_data = numpy.ones_like(self.im4mask_data).astype('uint16')
            
        #self.alpha = QPixmap(self.im4mask_size_h,self.im4mask_size_v)
        #self.alpha.fill(QColor(120,120,120))
        
        self.show_im4mask()
        self.show_masked()
        self.update_mask_buttons()
        
        
        
    def show_im4mask(self):
        if self.im4mask_data.any() != 0:
            if self.ui.mask_log.isChecked():
                self.pix_mask_data = QPixmap.fromImage(ImageQt(Image.fromarray(gray_to_color(self.im4mask_data_log,self.mask_scale_min*(2*self.im4mask_data_log_max-self.im4mask_data_log_min)/1000 + self.im4mask_data_log_min,self.mask_scale_max*(2*self.im4mask_data_log_max-self.im4mask_data_log_min)/1000 + self.im4mask_data_log_min))))
            else:
                self.pix_mask_data = QPixmap.fromImage(ImageQt(Image.fromarray(gray_to_color(self.im4mask_data,self.mask_scale_min*(2*self.im4mask_data_max-self.im4mask_data_min)/1000 + self.im4mask_data_min,self.mask_scale_max*(2*self.im4mask_data_max-self.im4mask_data_min)/1000 + self.im4mask_data_min))))
            # set transparency; deprecated method, but works
            #aa = QPixmap(self.im4mask_size_h,self.im4mask_size_v)
            #aa.fill(QColor(120,120,120))
            #self.pix_mask_data.setAlphaChannel(aa)
            self.show_mask_image.setPixmap(self.pix_mask_data)
            self.mask_scene.setSceneRect(-100,-100,200+self.im4mask_size_h,200+self.im4mask_size_v)
            #self.update_buttons()
            self.ui.graphicsView_mask.fitInView(self.show_mask_image,Qt.KeepAspectRatio)
            # get the initial zoom after fitinView, correct for the scroolbars (1.02031930334 or 1.02982731554 )
            if self.ui.zoom_mask.value()>100:
                self.init_mask_zoom = self.ui.graphicsView_mask.transform().m11()*1.02031930334
                #self.init_mask_zoom = self.ui.graphicsView_mask.transform().m11()*1.02982731554
            else:
                self.init_mask_zoom = self.ui.graphicsView_mask.transform().m11()
            
            # apply the current zoom from the slider value if different than 1
            if self.ui.zoom_mask.value()!=100:
                self.ui.graphicsView_mask.scale(self.ui.zoom_mask.value()*1.02031930334/100,self.ui.zoom_mask.value()*1.02031930334/100)
                #self.ui.graphicsView_mask.scale(self.ui.zoom_mask.value()*1.02982731554,self.ui.zoom_mask.value()*1.02982731554)
    
    
    def import_msk_file(self):
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        if self.analysis_folder != '':
            self.start_folder = self.analysis_folder
        else:
            self.start_folder=os.path.normpath(os.path.abspath('.'))
            #self.start_folder = os.path.expanduser('~')
        
        self.msk_path = str(QFileDialog.getOpenFileName(self,"Select a file", self.start_folder,".msk files (*.msk);;All Files (*)", options=options))
        
        # adapted from FabIO
        
        f = open(self.msk_path, 'rb')
        
        header = f.read(1024)
        
        ttt = numpy.fromstring(header,numpy.uint32)
        d1 = ttt[4]
        d2 = ttt[5]
        
        #d1 = header[16]*16**3+header[17]*16**2+header[18]*16+header[19]
        #d2 = header[20]*16**3+header[21]*16**2+header[22]*16+header[23]
        
        num_ints = (d1 + 31) // 32
        
        total = d2 * num_ints * 4 
        msk_data = f.read()
        f.close()
        
        
        
        msk_data = numpy.fromstring(msk_data, numpy.uint8)
        msk_data = numpy.reshape(msk_data, (d2, num_ints * 4))
        result = numpy.zeros((d2, num_ints * 4 * 8), numpy.uint8)
        
        #bits = numpy.ones((1), numpy.uint8)
        for i in range(8):
            #temp = numpy.bitwise_and(bits, data)
            temp = numpy.bitwise_and(1<<i, msk_data)
            result[:, i::8] = temp.astype(numpy.uint8)
            #bits = bits * 2
        
        spares = num_ints * 4 * 8 - d1
        if spares == 0:
            msk_pix = numpy.where(result == 0, 1, 0)
        else:
            msk_pix = numpy.where(result[:, :-spares] == 0, 1, 0)
            
        msk_pix = numpy.reshape(msk_pix.astype(numpy.uint16),(d2, d1))
        
        self.masked_data = numpy.flipud(msk_pix)
        
        self.show_masked()
        
        
    def import_tif_file(self):
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        if self.analysis_folder != '':
            self.start_folder = self.analysis_folder
        else:
            self.start_folder=os.path.normpath(os.path.abspath('.'))
            #self.start_folder = os.path.expanduser('~')
        
        tif_path = str(QFileDialog.getOpenFileName(self,"Select a file", self.start_folder,".tif files (*.tif *.tiff);;All Files (*)", options=options))
        
        self.masked_data = numpy.array(Image.open(tif_path).convert('L'))
        
        self.show_masked()
    
    def show_masked(self):
        ''' show mask as overlay
        '''
        # create inverse mask (1<->0) and zero-mask 
        inv_data = 1 - numpy.transpose(self.masked_data)
        zero_data = numpy.zeros_like(inv_data)
        # prepare data for rgba image; fully transparent where 0 in inverse mask, somewhat transparent red where 1
        rr = numpy.array([inv_data*255,zero_data,zero_data,inv_data*self.ui.mask_opacity.value()])
        # reshape to correct shape 
        rr = rr.ravel(order='F').reshape(self.im4mask_size_v,self.im4mask_size_h,4).astype('uint8')
        # create QPixmap
        self.pix_masked = QPixmap.fromImage(ImageQt(Image.fromarray(rr)))
        
        self.show_mask_masked.setPixmap(self.pix_masked)
    
    def export_msk_file(self):
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        if self.analysis_folder != '':
            self.start_folder = self.analysis_folder
        else:
            self.start_folder=os.path.normpath(os.path.abspath('.'))
        
        #savemsk_path = str(QFileDialog.getSaveFileName(self, 'Save .msk File',self.start_folder,".msk files (*.msk);;All Files (*)", options=options))
        #if not(savemsk_path.endswith('.msk')):
        #    savemsk_path += '.msk'
                
        dialog = QFileDialog()
        dialog.setFilter(dialog.filter() | QDir.Hidden)
        dialog.setDefaultSuffix('msk')
        dialog.setAcceptMode(QFileDialog.AcceptSave)
        dialog.setNameFilters(['.msk files (*.msk)','All files (*)'])
        dialog.setDirectory(self.start_folder)
        
        if dialog.exec_() == QDialog.Accepted:
            savemsk_path = str(dialog.selectedFiles()[0])
        
            if self.analysis_folder == '':
                self.analysis_folder = os.path.split(savemsk_path)[0]
            

            # adapted from FabIO
    
            #if sys.version < '3':
            #    bytes = str
            
            header = bytearray(b"\x00" * 1024)
            header[0] = 77  # M
            header[4] = 65  # A
            header[8] = 83  # S
            header[12] = 75  # K
            header[24] = 1  # 1
            header[16:20] = struct.pack("<I", self.im4mask_size_h)
            header[20:24] = struct.pack("<I", self.im4mask_size_v)
            
            data_to_write = numpy.zeros((self.im4mask_size_v, ((self.im4mask_size_h + 31) // 32) * 4), dtype=numpy.uint8)
            expanded_data = numpy.zeros((self.im4mask_size_v, ((self.im4mask_size_h + 31) // 32) * 32), dtype=numpy.uint8)
            expanded_data[:self.im4mask_size_v, :self.im4mask_size_h] = (numpy.flipud(self.masked_data) != 1)
            for i in range(8):
                order = (1 << i)
                data_to_write += expanded_data[:, i::8] * order
            with open(savemsk_path, mode="wb") as outfile:
                outfile.write(bytes(header))
                outfile.write(data_to_write.tostring())
        
        self.update_mask_buttons()
        
    def export_tif_file(self):
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        
        if self.analysis_folder != '':
            self.start_folder = self.analysis_folder
        else:
            self.start_folder=os.path.normpath(os.path.abspath('.'))
        
       
        #self.savemask_tif_path = str(QFileDialog.getSaveFileName(self, 'Save .tif File', self.start_folder,".tif files (*.tif *.tiff);;All Files (*)", options=options))
        #if not(self.savemask_tif_path.endswith('.tif')) and not(self.savemask_tif_path.endswith('.tif')):
        #    self.savemask_tif_path += '.tif'
        
        dialog = QFileDialog()
        dialog.setFilter(dialog.filter() | QDir.Hidden)
        dialog.setDefaultSuffix('tif')
        dialog.setAcceptMode(QFileDialog.AcceptSave)
        dialog.setNameFilters(['.tif files (*.tif *tiff)','All files (*)'])
        dialog.setDirectory(self.start_folder)
        
        if dialog.exec_() == QDialog.Accepted:
            self.savemask_tif_path = str(dialog.selectedFiles()[0])
        
            if self.analysis_folder == '':
                self.analysis_folder = os.path.split(self.savemask_tif_path)[0]
            
            qq = self.masked_data.astype(numpy.uint16)
            mask_im = Image.frombytes('I;16',(self.im4mask_size_h,self.im4mask_size_v),qq.tobytes())
            mask_im.save(self.savemask_tif_path)
            
        self.update_mask_buttons()
        


    def add_polygon(self):
        if self.ui.remove_polygon.isChecked():
            self.ui.remove_polygon.setChecked(False)
        elif self.ui.mask_zoom_select.isChecked():
            self.ui.mask_zoom_select.setChecked(False)
        elif self.ui.add_polygon.isChecked():
            #app.setOverrideCursor(QCursor(Qt.ArrowCursor))
            self.ui.graphicsView_mask.setDragMode(QGraphicsView.NoDrag)
        
        if self.ui.add_polygon.isChecked():
            self.polygon_points = []
            QApplication.setOverrideCursor(Qt.CrossCursor)
        else:
            ooo = self.mask_scene.items()
            QApplication.restoreOverrideCursor()
            for pp in ooo:
                if 'Pixmap' not in str(pp):
                    self.mask_scene.removeItem(pp)
            self.track_mouse_line = ''
            if len(self.polygon_points)>2:
                x, y = numpy.meshgrid(numpy.arange(self.im4mask_size_h), numpy.arange(self.im4mask_size_v))
                x, y = x.flatten(), y.flatten()
                points = numpy.vstack((x,y)).T
                #oo = numpy.array([x,y]).ravel(order='F').reshape(self.im4mask_size_h*self.im4mask_size_v,2).astype('float')
                pp = path.Path(self.polygon_points)
                # set radius depending if the path is cw or ccw
                r=.3
                r = r*is_ccw(pp) - r*(1-is_ccw(pp))
                grid = pp.contains_points(points,radius=r)
                grid = grid.reshape((self.im4mask_size_v,self.im4mask_size_h)) * 1
                self.masked_data = numpy.clip(self.masked_data - grid,0,1)
                self.show_masked()
                    
        self.status_message()
        
    def remove_polygon(self):
        if self.ui.add_polygon.isChecked():
            self.ui.add_polygon.setChecked(False)
        elif self.ui.mask_zoom_select.isChecked():
            self.ui.mask_zoom_select.setChecked(False)
        elif self.ui.remove_polygon.isChecked():
            #app.setOverrideCursor(QCursor(Qt.ArrowCursor))
            self.ui.graphicsView_mask.setDragMode(QGraphicsView.NoDrag)
        
        if self.ui.remove_polygon.isChecked():
            self.polygon_points = []
            QApplication.setOverrideCursor(Qt.CrossCursor)
        else:
            ooo = self.mask_scene.items()
            QApplication.restoreOverrideCursor()
            for pp in ooo:
                if 'Pixmap' not in str(pp):
                    self.mask_scene.removeItem(pp)
            self.track_mouse_line = ''
            if len(self.polygon_points)>2:
                x, y = numpy.meshgrid(numpy.arange(self.im4mask_size_h), numpy.arange(self.im4mask_size_v))
                x, y = x.flatten(), y.flatten()
                points = numpy.vstack((x,y)).T
                #oo = numpy.array([x,y]).ravel(order='F').reshape(self.im4mask_size_h*self.im4mask_size_v,2).astype('float')
                pp = path.Path(self.polygon_points)
                # set radius depending if the path is cc or ccw
                r=.3
                r = r*is_ccw(pp) - r*(1-is_ccw(pp))
                grid = pp.contains_points(points,radius=r)
                grid = grid.reshape((self.im4mask_size_v,self.im4mask_size_h)) * 1
                self.masked_data = numpy.clip(self.masked_data + grid,0,1)
                self.show_masked()
        
        self.status_message()
        
    def add_pixels(self):
        if self.ui.remove_pixels.isChecked():
            self.ui.remove_pixels.setChecked(False)
        elif self.ui.mask_zoom_select.isChecked():
            self.ui.mask_zoom_select.setChecked(False)
        elif self.ui.add_pixels.isChecked():
            app.setOverrideCursor(QCursor(Qt.CrossCursor))
            self.ui.graphicsView_mask.setDragMode(QGraphicsView.NoDrag)
        
        if not self.ui.add_pixels.isChecked():
            QApplication.restoreOverrideCursor()
        
        self.status_message()    
            
    def remove_pixels(self):
        if self.ui.add_pixels.isChecked():
            self.ui.add_pixels.setChecked(False)
        elif self.ui.mask_zoom_select.isChecked():
            self.ui.mask_zoom_select.setChecked(False)
        elif self.ui.remove_pixels.isChecked():
            app.setOverrideCursor(QCursor(Qt.CrossCursor))
            self.ui.graphicsView_mask.setDragMode(QGraphicsView.NoDrag)
        
        if not self.ui.remove_pixels.isChecked():
            QApplication.restoreOverrideCursor()
        
        self.status_message()
    
            
    def clear_mask(self):
        self.masked_data = numpy.ones_like(self.masked_data)
        self.show_masked()
        
    def invert_mask(self):
        self.masked_data = 1 - self.masked_data
        self.show_masked()
    
    def mask_below(self):
        ''' mask pixels below given intensity
        '''
        self.masked_data[numpy.where(self.im4mask_data<self.ui.mask_below_num.value())]=0
        self.show_masked()
        
    def mask_above(self):
        ''' mask pixels above given intensity
        '''
        self.masked_data[numpy.where(self.im4mask_data>self.ui.mask_above_num.value())]=0
        self.show_masked()
        
        
    def add_mask_frame(self):
        self.masked_data[0:self.ui.add_frame_px.value(),0:self.im4mask_size_h] = 0
        self.masked_data[self.im4mask_size_v-self.ui.add_frame_px.value():,0:self.im4mask_size_h] = 0
        self.masked_data[0:self.im4mask_size_v,0:self.ui.add_frame_px.value()] = 0
        self.masked_data[0:self.im4mask_size_v,self.im4mask_size_h-self.ui.add_frame_px.value():] = 0
        
        self.show_masked()
        
    
    def grow_mask_px(self):
        app.setOverrideCursor(QCursor(Qt.WaitCursor))
        self.masked_data = grow_mask(self.masked_data)
        self.show_masked()
        app.restoreOverrideCursor()
    
    def mask_zoom_select(self):
        if self.ui.add_polygon.isChecked():
            self.ui.add_polygon.setChecked(False)
        elif self.ui.remove_polygon.isChecked():
            self.ui.remove_polygon.setChecked(False)
        elif self.ui.mask_zoom_select.isChecked():
            app.setOverrideCursor(QCursor(Qt.ArrowCursor))
            self.ui.graphicsView_mask.setDragMode(QGraphicsView.NoDrag)
        
        if not self.ui.mask_zoom_select.isChecked():
            self.mask_zoom_select_points = []
            if self.mask_zoom_pol != '':
                self.mask_scene.removeItem(self.mask_zoom_pol)
                self.mask_zoom_pol = ''
            
        self.status_message()
    
    def mask_zoom_into_select(self,xx,yy):
        if self.mask_zoom_select_points == []:
            self.mask_zoom_select_points.append(xx)
            self.mask_zoom_select_points.append(yy)
        else:
            self.ui.graphicsView_mask.fitInView(min(xx,self.mask_zoom_select_points[0]),min(yy,self.mask_zoom_select_points[1]),abs(xx-self.mask_zoom_select_points[0]),abs(yy-self.mask_zoom_select_points[1]),Qt.KeepAspectRatio)
            self.mask_zoom_select_points = []
            self.ui.mask_zoom_select.setChecked(False)
            self.mask_scene.removeItem(self.mask_zoom_pol)
            self.mask_zoom_pol = ''
            zoom = int(100*self.ui.graphicsView_mask.transform().m11()/self.init_mask_zoom)
            if zoom <= 5000:
                self.ui.zoom_mask.setValue(zoom)
            else:
                self.ui.zoom_mask.setValue(5000)
                self.ui.graphicsView_mask.scale(5000/zoom,5000/zoom)
            self.status_message()
    
    def mask_zoom_changed(self):
        ''' zoom change
        '''
        # current zoom in respect to fitinView
        self.mask_zoom = self.ui.graphicsView_mask.transform().m11()/self.init_mask_zoom
        # how much to zoom in this step
        self.mask_zoom_factor = (self.ui.zoom_mask.value()/100)/self.mask_zoom
        # make zoom
        self.ui.graphicsView_mask.scale(self.mask_zoom_factor, self.mask_zoom_factor)
    
    
    def mask_max_changed(self):
        ''' lut maximum change
        '''
        center_pix = self.ui.graphicsView_mask.mapToScene(self.ui.graphicsView_mask.viewport().rect()).boundingRect().center()
        self.mask_scale_max = self.ui.max_mask.value()
        self.show_im4mask()
        self.ui.graphicsView_mask.centerOn(center_pix)

    def mask_min_changed(self):
        ''' lut minimum change
        '''
        center_pix = self.ui.graphicsView_mask.mapToScene(self.ui.graphicsView_mask.viewport().rect()).boundingRect().center()
        self.mask_scale_min = self.ui.min_mask.value()
        self.show_im4mask()
        self.ui.graphicsView_mask.centerOn(center_pix)        
    
    def mask_reset_cm(self):
        #self.mask_scale_min = 50
        #self.mask_scale_max = 950
        self.ui.min_mask.setValue(50)
        self.ui.max_mask.setValue(950)
    
    def create_mask_scene(self):
        ''' create scene in qgraphicsview in masktab
        '''
        self.mask_scene = QGraphicsScene(self.ui.graphicsView_mask)
        self.ui.graphicsView_mask.setScene(self.mask_scene)
        self.show_mask_image = QGraphicsPixmapItem()
        self.mask_scene.addItem(self.show_mask_image)
        self.show_mask_masked = QGraphicsPixmapItem()
        self.mask_scene.addItem(self.show_mask_masked)
        
    
    def update_integ(self):
        if self.savemask_tif_path != '':
            options = QFileDialog.Options()
            #options |= QFileDialog.DontUseNativeDialog
        
            integmpp_path = str(QFileDialog.getOpenFileName(self,"Select a file", self.start_folder,".mpp files (*.mpp);;All Files (*)", options=options))
        
            for line in fileinput.input(integmpp_path, inplace = 1):
                line = re.sub('^#*-m.*$','-m yes {}'.format(self.savemask_tif_path),line)
                sys.stdout.write(line)
            
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Information)
            msg.setText("{} updated".format(os.path.split(integmpp_path)[1]))
            msg.setWindowTitle("mpp update")
            msg.setStandardButtons(QMessageBox.Ok)
            msg.exec_()
        
        else:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Information)
            msg.setText("Please export mask as .tif first!")
            msg.setWindowTitle("mpp update")
            msg.setStandardButtons(QMessageBox.Ok)
            msg.exec_()
    
    def update_mask_buttons(self):
        if self.im4mask_data.any() != 0:
            self.ui.import_msk.setEnabled(True)
            self.ui.import_tif.setEnabled(True)
            self.ui.export_msk.setEnabled(True)
            self.ui.export_tif.setEnabled(True)
            self.ui.zoom_mask.setEnabled(True)
            self.ui.zoom_mask_label.setEnabled(True)
            self.ui.max_mask.setEnabled(True)
            self.ui.max_mask_label.setEnabled(True)
            self.ui.min_mask.setEnabled(True)
            self.ui.min_mask_label.setEnabled(True)
            self.ui.mask_opacity.setEnabled(True)
            self.ui.mask_opacity_label.setEnabled(True)
            self.ui.add_polygon.setEnabled(True)
            self.ui.add_pixels.setEnabled(True)
            self.ui.remove_polygon.setEnabled(True)
            self.ui.remove_pixels.setEnabled(True)
            self.ui.clear_mask.setEnabled(True)
            self.ui.invert_mask.setEnabled(True)
            self.ui.add_mask_frame.setEnabled(True)
            self.ui.add_frame_px.setEnabled(True)
            self.ui.mask_zoom_select.setEnabled(True)
            self.ui.mask_below.setEnabled(True)
            self.ui.mask_below_num.setEnabled(True)
            self.ui.mask_above.setEnabled(True)
            self.ui.mask_above_num.setEnabled(True)
            self.ui.grow_mask.setEnabled(True)
            
        if self.savemask_tif_path != '':
            self.ui.update_integmpp.setEnabled(True)
            
    ## end of mask_tab
        
    ## rad_integ_tab
    
    def radint_set_data(self):
        ''' manualy set data folder
        '''
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        if self.data_folder != '':
            self.start_folder = self.data_folder
        else:
            self.start_folder = os.path.realpath(os.path.normpath('.'))
        self.data_folder = str(QFileDialog.getExistingDirectory(self,'Select analysis folder',self.start_folder,QFileDialog.ShowDirsOnly))
        if self.analysis_folder == '' and os.path.isdir(os.path.normpath(os.path.join(self.data_folder,'../analysis/'))):
                self.analysis_folder = os.path.normpath(os.path.join(self.data_folder,'../analysis/'))
        
        self.update_footer()
        self.update_radint_buttons()
        
    def radint_set_analysis(self):
        ''' manualy set analysis folder
        '''
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        if os.path.isdir(os.path.normpath(os.path.join(self.data_folder,'../analysis/'))):
            self.start_folder = os.path.normpath(os.path.join(self.data_folder,'../analysis/'))
        elif self.data_folder != '':
            self.start_folder = self.data_folder
        elif self.analysis_folder != '':
            self.start_folder = self.analysis_folder
        else:
            self.start_folder = os.path.realpath(os.path.normpath('.'))
        self.analysis_folder = str(QFileDialog.getExistingDirectory(self,'Select analysis folder',self.start_folder,QFileDialog.ShowDirsOnly))
        self.update_footer()
        self.update_radint_buttons()
        
    def select_buffer_file(self):
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        if self.data_folder != '':
            self.start_folder = self.data_folder
        else:
            self.start_folder=os.path.normpath(os.path.abspath('.'))
        
        self.buffer_path = str(QFileDialog.getOpenFileName(self,"Select a buffer file", self.start_folder,"01.tif files (*01.tif *01.tiff);;.tif files (*.tif *.tiff);;All Files (*)", options=options))
        
        self.data_folder = os.path.dirname(self.buffer_path)
        if self.analysis_folder == '' and os.path.isdir(os.path.normpath(os.path.join(self.data_folder,'../analysis/'))):
                self.analysis_folder = os.path.normpath(os.path.join(self.data_folder,'../analysis/'))
        self.update_footer()
        self.ui.buffer_text.setText('{}'.format(self.buffer_path))
        
    def select_sample_first_file(self):
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        if self.data_folder != '':
            self.start_folder = self.data_folder
        else:
            self.start_folder=os.path.normpath(os.path.abspath('.'))
    
        self.sample_first_path = str(QFileDialog.getOpenFileName(self,"Select a sample file", self.start_folder,"01.tif files (*01.tif *01.tiff);;.tif files (*.tif *.tiff);;All Files (*)", options=options))
    
        self.data_folder = os.path.dirname(self.sample_first_path)
        if self.analysis_folder == '' and os.path.isdir(os.path.normpath(os.path.join(self.data_folder,'../analysis/'))):
                self.analysis_folder = os.path.normpath(os.path.join(self.data_folder,'../analysis/'))
        self.update_footer()
        self.ui.sample_first_text.setText('{}'.format(self.sample_first_path))
        
    def select_sample_last_file(self):
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        if self.data_folder != '':
            self.start_folder = self.data_folder
        else:
            self.start_folder=os.path.normpath(os.path.abspath('.'))
    
        self.sample_last_path = str(QFileDialog.getOpenFileName(self,"Select a sample file", self.start_folder,"01.tif files (*01.tif *01.tiff);;.tif files (*.tif *.tiff);;All Files (*)", options=options))
    
        self.data_folder = os.path.dirname(self.sample_last_path)
        if self.analysis_folder == '' and os.path.isdir(os.path.normpath(os.path.join(self.data_folder,'../analysis/'))):
                self.analysis_folder = os.path.normpath(os.path.join(self.data_folder,'../analysis/'))
        self.update_footer()
        self.ui.sample_last_text.setText('{}'.format(self.sample_last_path))
        
    def do_2dto1d(self):
        app.setOverrideCursor(QCursor(Qt.WaitCursor))
        fs_text = self.ui.sample_first_text.text()
        try:
            fs_num = int(fs_text)
            fs_path = ''
        except:
            fs_num = int(fs_text.split('_')[-3][1:])
            fs_path = str(fs_text)
        
        ls_path = ''
        if self.ui.multiple_samples.isChecked():
            ls_text = self.ui.sample_last_text.text()
            try:
                ls_num = int(ls_text)
            except:
                ls_num = int(ls_text.split('_')[-3][1:])
                ls_path = str(ls_text)
        
        else:
            ls_num = fs_num
        
        # for multiple samples make sure that they are in the same folder
        if self.ui.multiple_samples.isChecked() and fs_path != '' and ls_path != '' and os.path.dirname(fs_path) != os.path.dirname(ls_path):
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Information)
            msg.setText("samples have to be in the same folder!")
            msg.setWindowTitle("Samples path problem")
            msg.setStandardButtons(QMessageBox.Ok)
            msg.exec_()
       
        else:
            bf_path = ''
            if self.ui.subtract_buffer.isChecked():
                bf_text = str(self.ui.buffer_text.text())
                try:
                    bf_num = int(bf_text)
                    # for saxspipe auto sample finding
                    if self.ui.use_saxspipe.isChecked() and bf_num == -1:
                        bf_path = -1
                    else:
                        bf_path = find_image(self.data_folder,bf_num)
                except:
                    #bf_num = int(bf_text.split('_')[-3][1:])
                    bf_path = bf_text
            
            if self.ui.subtract_buffer.isChecked() and (bf_path == 1 or bf_path == 0):
                msg = QMessageBox()
                msg.setIcon(QMessageBox.Information)
                msg.setText("found {} buffer files".format(['0','multiple'][bf_path]))
                msg.setWindowTitle("Buffer file problem")
                msg.setStandardButtons(QMessageBox.Ok)
                msg.exec_()
            else:
                
                # if no saxspipe, check if the integ.mpp is in the correct place and comment out -f lines
                
                if not self.ui.use_saxspipe.isChecked():
                    if not os.path.isfile(os.path.join(self.analysis_folder,'integ.mpp')):
                        msg = QMessageBox()
                        msg.setIcon(QMessageBox.Information)
                        msg.setText('no integ.mpp file in {}'.format(self.analysis_folder))
                        msg.setWindowTitle("integ.mpp file problem")
                        msg.setStandardButtons(QMessageBox.Ok)
                        msg.exec_()
                    else:
                        integmpp = os.path.join(self.analysis_folder,'integ.mpp')
                        for line in fileinput.input(integmpp, inplace = 1):
                            line = re.sub('^-f','#-f',line)
                            sys.stdout.write(line)
                
                # loop through all samples
                for sn in numpy.arange(fs_num,ls_num+1):
                    if sn == fs_num and fs_path != '':
                        # use filename if given as first sample
                        sample_path = fs_path
                    elif self.ui.multiple_samples.isChecked() and sn == ls_num and ls_path != '':
                        # use filename if given as last sample
                        sample_path = ls_path
                    else:
                        sample_path = find_image(self.data_folder,sn)
                    
                    if sample_path == 1 or sample_path == 0:
                        # if there are 0 or multiple files found continue to the next sample, write message on the console
                        print('series {} has {} sample files found, skipping'.format(sn,['0','multiple'][sample_path]))
                    else:
                        # use saxspipe
                        if self.ui.use_saxspipe.isChecked():
                            # check for the integ.mpp in data/../analysis folder, where saxspipe expects it
                            if not os.path.isfile(os.path.join(os.path.normpath(os.path.join(self.data_folder,'../analysis/')),'integ.mpp')):
                                msg = QMessageBox()
                                msg.setIcon(QMessageBox.Information)
                                msg.setText('no integ.mpp file in {}'.format(self.analysis_folder))
                                msg.setWindowTitle("integ.mpp file problem")
                                msg.setStandardButtons(QMessageBox.Ok)
                                msg.exec_()
                            else:
                                if self.ui.subtract_buffer.isChecked():
                                    # subtract buffer
                                    if bf_path == -1:
                                        # find buffer automatically
                                        pp = subprocess.Popen(['saxspipe2.py',sample_path])
                                        pp.communicate()
                                    else:
                                        # use buffer file from the input field
                                        pp = subprocess.Popen(['saxspipe2.py',sample_path,bf_path])
                                        pp.communicate()
                                else:
                                    # no buffer subtraction
                                    pp = subprocess.Popen(['saxspipe2.py',sample_path,'0'])
                                    pp.communicate()
                        else:
                            # use sastool
                            # prepare integ.mpp
                            for line in fileinput.input(integmpp, inplace = 1): 
                                line = re.sub('-s no','-s yes',line)
                                if not self.ui.subtract_buffer.isChecked():
                                    line = re.sub('^-s','#-s',line)
                                else:
                                    line = re.sub('^#-s','-s',line)	
                                sys.stdout.write(line)
                            
                            # write new sample line in integ.mpp
                            new_line = '-f '+sample_path                          
                            if self.ui.subtract_buffer.isChecked():
                                new_line = new_line+" "+bf_path
                                
                            with open(integmpp,'a') as ff:
                                ff.write(new_line+"\n")
                                
                                
            
                # run sastool once all the -f lines are in integ.mpp
                if not self.ui.use_saxspipe.isChecked():
                    pp = subprocess.Popen(['sastool',integmpp],cwd=self.analysis_folder)
                    pp.communicate()
                            
        app.restoreOverrideCursor()
    
    def update_radint_buttons(self):
        if self.ui.subtract_buffer.isChecked():
            self.ui.buffer_label.setEnabled(True)
            self.ui.select_buffer.setEnabled(True)
            self.ui.buffer_text.setEnabled(True)
        else:
            self.ui.buffer_label.setEnabled(False)
            self.ui.select_buffer.setEnabled(False)
            self.ui.buffer_text.setEnabled(False)
            
        if self.ui.multiple_samples.isChecked():
            self.ui.sample_last_label.setEnabled(True)
            self.ui.select_sample_last.setEnabled(True)
            self.ui.sample_last_text.setEnabled(True)
        else:
            self.ui.sample_last_label.setEnabled(False)
            self.ui.select_sample_last.setEnabled(False)
            self.ui.sample_last_text.setEnabled(False)
        
        if self.ui.use_saxspipe.isChecked():
            self.ui.radint_set_analysis.setEnabled(False)
            self.ui.radint_analysis_text.setText(os.path.normpath(os.path.join(self.data_folder,'../analysis/')))
            #self.ui.radint_analysis_text.setEnabled(False)
        else:
            self.ui.radint_set_analysis.setEnabled(True)
            self.ui.radint_analysis_text.setText(self.analysis_folder)
            #self.ui.radint_analysis_text.setEnabled(True)
        
        if self.ui.subtract_buffer.isChecked() and self.ui.use_saxspipe.isChecked():
            self.ui.saxspipe_buffer_label.setText('buffer \'-1\' for saxspipe to automatically find autosampler buffer')
            self.ui.saxspipe_buffer_label.setStyleSheet('color: red')
        else:
            self.ui.saxspipe_buffer_label.setText('')
        
            
        if self.data_folder == '' or self.analysis_folder == '' or (self.ui.subtract_buffer.isChecked() and str(self.ui.buffer_text.text()) == '') or (self.ui.multiple_samples.isChecked() and str(self.ui.sample_last_text.text()) == '') or str(self.ui.sample_first_text.text()) == '':
            self.ui.do_2dto1d.setEnabled(False)
        else:
            self.ui.do_2dto1d.setEnabled(True)
        
            
    
    ## end of rad_integ_tab
      
      
    ## hplc_tab 
      
    def hplc_select_data_file(self):
        ''' select first data file of hplc series
        '''
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        if self.data_folder != '':
            self.start_folder = self.data_folder
        else:
            self.start_folder=os.path.normpath(os.path.abspath('.'))
            
        self.hplc_data = str(QFileDialog.getOpenFileName(self,"Select a file", self.start_folder,"01.tif files (*01.tif *01.tiff);;.tif files (*.tif *.tiff);;All Files (*)", options=options))
        
        # set analysis folder and integ.mpp, only if they are not previously set and they exist 
        if self.analysis_folder == '':
            if os.path.exists(os.path.normpath(os.path.join(os.path.split(self.hplc_data)[0],'../analysis'))):
                self.analysis_folder = os.path.normpath(os.path.join(os.path.split(self.hplc_data)[0],'../analysis'))
            if os.path.isfile(os.path.join(self.analysis_folder,'integ.mpp')):
                self.integmpp = os.path.join(self.analysis_folder,'integ.mpp')
        self.ui.hplc_info_label.setText('{}'.format(self.hplc_data))
        self.hplc_title= '_'.join(os.path.split(self.hplc_data)[1].split('_')[:-3])+' ' + os.path.split(self.hplc_data)[1].split('_')[-3]
        try:
            uv_bip = '_'.join(self.hplc_data.split('_')[:-2])+'uv.bip'
            uv_data = numpy.genfromtxt(uv_bip,skip_header=27)
            self.uv_num = uv_data[:,0]
            self.uv1 = uv_data[:,1]
            self.uv2 = uv_data[:,2]
            self.ui.hplc_uv_info_label.setText('{}'.format(uv_bip))
            self.plot_hplc()
        except:
            pass
        self.update_footer()
    
    
    def hplc_set_analysis(self):
        ''' manualy set analysis folder
        '''
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        if self.analysis_folder != '':
            self.start_folder = self.analysis_folder
        elif os.path.split(self.hplc_data)[0] != '':
            self.start_folder = os.path.join(os.path.split(self.hplc_data)[0],'..')
        else:
            self.start_folder = os.path.abspath('.')
        self.analysis_folder = str(QFileDialog.getExistingDirectory(self,'Select analysis folder',self.start_folder,QFileDialog.ShowDirsOnly))
        self.integmpp = os.path.join(self.analysis_folder,'integ.mpp')
        
    
    def hplc_sastool(self):
        ''' run sastool on hplc data
        '''
        app.setOverrideCursor(QCursor(Qt.WaitCursor))
        # create folders
        
        # get file name and series number
        hplc_name = os.path.split(self.hplc_data)[1]
        self.hplc_analysis_folder = os.path.join(self.analysis_folder,'_'.join(hplc_name.split('_')[:-2]))
        if not os.path.isdir(self.hplc_analysis_folder):
            os.makedirs(self.hplc_analysis_folder)
        
        self.hplc_ave_folder = os.path.join(self.hplc_analysis_folder,'average')
        self.hplc_plots_folder = os.path.join(self.hplc_analysis_folder,'plots')
        self.hplc_sastool_folder = os.path.join(self.hplc_analysis_folder,'sastool')
        
        for nn in [self.hplc_ave_folder,self.hplc_plots_folder,self.hplc_sastool_folder]:
            if not os.path.isdir(nn):
                os.makedirs(nn)
        
        # create integ.mpp in sastool folder and run sastool
        
        hplc_integmpp = os.path.join(self.hplc_sastool_folder,'integ.mpp')
        
        # get the data from the original integ.mpp
        newmpp=[]
        
        with open(self.integmpp, "r") as impp:
            for line in impp:
                if not line.startswith('#') and not line.startswith('-f') and not re.match(r'^\s*$', line):
                    if line.startswith('-m'):
                        if not os.path.isabs(line.split()[-1]):
                            line='-m yes {}'.format(os.path.normpath(os.path.expanduser(os.path.join(self.analysis_folder,line.split()[-1]))))
                        else:
                            line='-m yes {}'.format(os.path.abspath(line.split()[-1]))
                    if not line.endswith('\n'):
                        line=line+'\n'
                    newmpp.append(line)
        
        # create a new integ.mpp in the sample sastool folder
        with open(hplc_integmpp, 'w') as mpp_file:
            for item in newmpp:
                mpp_file.write("{}".format(item))
            
            num_length = len(hplc_name.split('_')[-1].split('.')[0])
            first_sample = '_'.join(self.hplc_data.split('_')[:-1]+[str(int(hplc_name.split('_')[-1].split('.')[0])+self.ui.hplc_buf_num.value()).zfill(num_length)+'.tif'])
            
            mpp_file.write('-f {} {} {}'.format(first_sample, self.hplc_data, self.ui.hplc_buf_num.value()))
        
        # run sastool
        pp = subprocess.Popen(['sastool','integ.mpp'],cwd = self.hplc_sastool_folder)
        pp.communicate()
        
        # read .dat files. calculate rg and i0, get iqmin
        
        self.open_sub_data = os.path.join(self.hplc_sastool_folder,os.path.splitext(os.path.split(first_sample)[1])[0]+'.dat')
        
        self.hplc_analysis(self.open_sub_data)
        
        self.plot_hplc()
        
        app.restoreOverrideCursor()
        
    
    def open_hplc_data(self):
        ''' select already integrated data
        '''
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        if self.analysis_folder != '':
            self.start_folder = self.analysis_folder
        else:
            self.start_folder=os.path.normpath(os.path.abspath('.'))
            
        self.open_sub_data = str(QFileDialog.getOpenFileName(self,"Select a file", self.start_folder,".dat files (*.dat);;All Files (*)", options=options))
        
        # set folders, create if not exist
        self.hplc_analysis_folder = os.path.normpath(os.path.join(os.path.split(self.open_sub_data)[0],'..'))
        self.hplc_sastool_folder = os.path.split(self.open_sub_data)[0]
        self.hplc_plots_folder = os.path.join(self.hplc_analysis_folder,'plots')
        self.hplc_ave_folder = os.path.join(self.hplc_analysis_folder,'average')
        
        for nn in [self.hplc_ave_folder,self.hplc_plots_folder]:
            if not os.path.isdir(nn):
                os.makedirs(nn)
        
        
        app.setOverrideCursor(QCursor(Qt.WaitCursor))
        # read .sub files. calculate rg and i0, get iqmin
        self.ui.hplc_info_label.setText('{}'.format(self.open_sub_data))
        self.hplc_analysis(self.open_sub_data)
        self.hplc_title= '_'.join(os.path.split(self.open_sub_data)[1].split('_')[:-3])+' ' + os.path.split(self.open_sub_data)[1].split('_')[-3]
        
        try:
            uv_bip = os.path.join(self.hplc_plots_folder,'_'.join(os.path.split(self.open_sub_data)[1].split('_')[:-3])+'_UV_data.txt')
            uv_data = numpy.genfromtxt(uv_bip,skip_header=1)
            self.uv_num = uv_data[:,0]
            self.uv1 = uv_data[:,1]
            self.uv2 = uv_data[:,2]
            self.ui.hplc_uv_info_label.setText('{}'.format(self.hplc_plots_folder,'_'.join(os.path.split(self.open_sub_data)[1].split('_')[:-3])+'_UV_data.txt'))
        except:
            pass
        
        self.plot_hplc()
        
        
        app.restoreOverrideCursor()
        
        
        
    def load_saxs_bip(self):
        ''' load pre-analysed saxs data, saved in a .bip file
        '''
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        if self.analysis_folder != '':
            self.start_folder = self.analysis_folder
        else:
            self.start_folder=os.path.normpath(os.path.abspath('.'))
            
        saxs_bip = str(QFileDialog.getOpenFileName(self,"Select a file", self.start_folder,".bip *.txt files (*.bip *.txt);;All Files (*)", options=options))
        
        if os.path.splitext(saxs_bip)[1] == '.bip':
            saxs_data = numpy.genfromtxt(saxs_bip,skip_header=30)
            self.im_num_ar = saxs_data[:,0]
            self.rg_ar = saxs_data[:,1]
            self.i0_ar = saxs_data[:,2]
            self.iqmin_ar = saxs_data[:,3]
            self.hplc_title= '_'.join(os.path.split(saxs_bip)[1].split('_')[:-1])+' S'+saxs_bip.split('_')[-1].split('.')[0]
        elif os.path.splitext(saxs_bip)[1] == '.txt':
            saxs_data = numpy.genfromtxt(saxs_bip,skip_header=1)
            
            self.im_num_ar = saxs_data[:,0]
            self.rg_ar = saxs_data[:,1]
            self.i0_ar = saxs_data[:,2]
            self.iqmin_ar = saxs_data[:,3]
            self.hplc_title= '_'.join(os.path.split(saxs_bip)[1].split('_')[:-2])
        
        self.ui.hplc_info_label.setText('{}'.format(saxs_bip))   
        self.plot_hplc()
        
        
    def load_uv_bip(self):
        ''' load uv data saved in a .bip file
        '''
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        if self.analysis_folder != '':
            self.start_folder = self.analysis_folder
        else:
            self.start_folder=os.path.normpath(os.path.abspath('.'))
            
        uv_bip = str(QFileDialog.getOpenFileName(self,"Select a file", self.start_folder,".bip *.txt files (*.bip *.txt);;All Files (*)", options=options))
        
        if os.path.splitext(uv_bip)[1] == '.bip':
            uv_data = numpy.genfromtxt(uv_bip,skip_header=27)
            self.uv_num = uv_data[:,0]
            self.uv1 = uv_data[:,1]
            self.uv2 = uv_data[:,2]
            self.hplc_title= '_'.join(os.path.split(uv_bip)[1].split('_')[:-1])+' S'+uv_bip.split('_')[-1].split('u')[0]
        elif os.path.splitext(uv_bip)[1] == '.txt':
            uv_data = numpy.genfromtxt(uv_bip,skip_header=1)
            self.uv_num = uv_data[:,0]
            self.uv1 = uv_data[:,1]
            self.uv2 = uv_data[:,2]
            self.hplc_title= '_'.join(os.path.split(uv_bip)[1].split('_')[:-2])
         
        self.ui.hplc_uv_info_label.setText('{}'.format(uv_bip))
        self.plot_hplc()
    
    
    def save_saxs_txt(self):
        if len(self.im_num_ar) > 0:
            options = QFileDialog.Options()
            #options |= QFileDialog.DontUseNativeDialog
            
            if self.hplc_plots_folder != '':
                self.start_folder = self.hplc_plots_folder
            else:
                self.start_folder=os.path.normpath(os.path.abspath('.'))
            
            # try to set filename : os.path.join(plots_sfol,'{}_plot_data.txt'.format(sample_name)
            
            save_saxs_file = str(QFileDialog.getSaveFileName(self, 'Save .txt File', self.start_folder,".txt files (*.txt);;All Files (*)", options=options))
            
            numpy.savetxt(save_saxs_file,numpy.transpose([self.im_num_ar,self.rg_ar,self.i0_ar,self.iqmin_ar]),fmt='%.8G',delimiter='\t',header='im.num. \tRg \tI0 \tIqmin')
        else:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Information)
            msg.setText("No data to save!")
            msg.setWindowTitle("Data missing")
            msg.setStandardButtons(QMessageBox.Ok)
            msg.exec_()

            
            
    def save_uv_txt(self):
        if len(self.uv_num) > 0:
            options = QFileDialog.Options()
            #options |= QFileDialog.DontUseNativeDialog
            
            if self.hplc_plots_folder != '':
                self.start_folder = self.hplc_plots_folder
            else:
                self.start_folder=os.path.normpath(os.path.abspath('.'))
            
            # try to set filename : os.path.join(plots_sfol,'{}_plot_data.txt'.format(sample_name)
            
            save_uv_file = str(QFileDialog.getSaveFileName(self, 'Save .txt File', self.start_folder,".txt files (*.txt);;All Files (*)", options=options))
            
            numpy.savetxt(save_uv_file,numpy.transpose([self.uv_num+self.ui.hplc_uv_xoffset.value(),self.uv1+self.ui.hplc_uv1_yoffset.value(),self.uv2+self.ui.hplc_uv2_yoffset.value()]),fmt='%.8G',delimiter='\t',header='im.num. \tUV1 \tUV2')
        
        else:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Information)
            msg.setText("No data to save!")
            msg.setWindowTitle("Data missing")
            msg.setStandardButtons(QMessageBox.Ok)
            msg.exec_()
        
    
    def hplc_average(self):
        
        if self.open_sub_data != '' and self.hplc_sastool_folder != '' and self.hplc_ave_folder != '':
            ave_data = os.path.split(self.open_sub_data)[1]
            sample_name_ser = '_'.join(ave_data.split('_')[:-2])
            num_length = len(ave_data.split('_')[-1].split('.')[0])
            
            for aa in numpy.arange(self.ui.hplc_avg_from.value(),self.ui.hplc_avg_to.value(), self.ui.hplc_avg_step.value()):
                try:
                    sub_file = os.path.join(self.hplc_sastool_folder,sample_name_ser+"_0_"+str(int(aa)).zfill(num_length)+".dat")
                    avg_data = numpy.genfromtxt(sub_file)
                    for zz in numpy.arange(1,self.ui.hplc_avg_step.value()):
                        sub_file = os.path.join(self.hplc_sastool_folder,sample_name_ser+"_0_"+str(int(aa+zz)).zfill(num_length)+".dat")
                        sub_data = numpy.genfromtxt(sub_file)
                        avg_data[:,1]=avg_data[:,1]+sub_data[:,1]
                        avg_data[:,2]=avg_data[:,2]+sub_data[:,2]
                    
                    avg_data[:,1] = avg_data[:,1]/self.ui.hplc_avg_step.value()
                    avg_data[:,2] = avg_data[:,2]/self.ui.hplc_avg_step.value()
                        
                    numpy.savetxt(os.path.join(self.hplc_ave_folder,"Ave_"+sample_name_ser+"_0_"+str(int(aa)).zfill(num_length)+"-"+str(int(aa+self.ui.hplc_avg_step.value())).zfill(num_length)+'.dat'),avg_data,fmt='%.8g')
                except:
                    print("Problem averaging data!")
        else:
            print('problem reading data/folder')
            
            
        
    
    def hplc_reset_plot(self):
        self.ui.hplc_left_min.setValue(0)
        self.ui.hplc_left_max.setValue(0)
        
        self.ui.hplc_right_min.setValue(0)
        self.ui.hplc_right_max.setValue(0)
        
        self.ui.hplc_x_min.setValue(0)
        self.ui.hplc_x_max.setValue(0)
        
        self.ui.hplc_yticks_num.setValue(5)
        
        self.plot_hplc()
        

    
    def replot_hplc(self):
        if self.ui.hplc_left_min.value() !=self.ui.hplc_left_max.value():
            self.ui.mpl_plot.canvas.ax1.set_ylim(self.ui.hplc_left_min.value(),self.ui.hplc_left_max.value())
        if self.ui.hplc_right_min.value() !=self.ui.hplc_right_max.value():
            self.ui.mpl_plot.canvas.ax2.set_ylim(self.ui.hplc_right_min.value(),self.ui.hplc_right_max.value())
        if self.ui.hplc_x_min.value() != self.ui.hplc_x_max.value():
            self.ui.mpl_plot.canvas.ax1.set_xlim(self.ui.hplc_x_min.value(),self.ui.hplc_x_max.value())
        
       
        # align y-axes
        #self.ui.mpl_plot.canvas.ax1.yaxis.set_major_locator(AutoLocator())
        self.ui.mpl_plot.canvas.ax1.yaxis.set_major_locator(LinearLocator(1+self.ui.hplc_yticks_num.value()))
        #self.ui.mpl_plot.canvas.ax2.yaxis.set_major_locator(AutoLocator())
        self.ui.mpl_plot.canvas.ax2.yaxis.set_major_locator(LinearLocator(1+self.ui.hplc_yticks_num.value()))
        
        self.ui.mpl_plot.canvas.draw()
        
    def hplc_rg_l_click(self):
        if self.ui.hplc_rg_l.isChecked():
            self.ui.hplc_rg_r.setChecked(False)
        if len(self.im_num_ar)>0 and not self.ui.hplc_rg_r.isChecked():
            self.hplc_show_line(0)
        
    def hplc_rg_r_click(self):
        if self.ui.hplc_rg_r.isChecked():
            self.ui.hplc_rg_l.setChecked(False)
        if len(self.im_num_ar)>0 and not self.ui.hplc_rg_l.isChecked():
            self.hplc_show_line(0)
     
    def hplc_rg_norm_click(self):
        if len(self.im_num_ar)>0:
            self.hplc_show_line(0)
        
    def hplc_i0_l_click(self):
        if self.ui.hplc_i0_l.isChecked():
            self.ui.hplc_i0_r.setChecked(False)
        if len(self.im_num_ar)>0 and not self.ui.hplc_i0_r.isChecked():
            self.hplc_show_line(1)
        
    def hplc_i0_r_click(self):
        if self.ui.hplc_i0_r.isChecked():
            self.ui.hplc_i0_l.setChecked(False)
        if len(self.im_num_ar)>0 and not self.ui.hplc_i0_l.isChecked():
            self.hplc_show_line(1)
     
    def hplc_i0_norm_click(self):
        if len(self.im_num_ar)>0:
            self.hplc_show_line(1)
        
    def hplc_iqmin_l_click(self):
        if self.ui.hplc_iqmin_l.isChecked():
            self.ui.hplc_iqmin_r.setChecked(False)
        if len(self.im_num_ar)>0 and not self.ui.hplc_iqmin_r.isChecked():
            self.hplc_show_line(2)
        
    def hplc_iqmin_r_click(self):
        if self.ui.hplc_iqmin_r.isChecked():
            self.ui.hplc_iqmin_l.setChecked(False)
        if len(self.im_num_ar)>0 and not self.ui.hplc_iqmin_l.isChecked():
            self.hplc_show_line(2)
    
    def hplc_iqmin_norm_click(self):
        if len(self.im_num_ar)>0:
            self.hplc_show_line(2)
    
    def hplc_uv1_l_click(self):
        if self.ui.hplc_uv1_l.isChecked():
            self.ui.hplc_uv1_r.setChecked(False)
        if len(self.uv_num)>0 and not self.ui.hplc_uv1_r.isChecked():
            self.hplc_show_line(3)
        
    def hplc_uv1_r_click(self):
        if self.ui.hplc_uv1_r.isChecked():
            self.ui.hplc_uv1_l.setChecked(False)
        if len(self.uv_num)>0 and not self.ui.hplc_uv1_l.isChecked():
            self.hplc_show_line(3)
    
    def hplc_uv1_norm_click(self):
        if len(self.uv_num)>0:
            self.hplc_show_line(3)
    
    def hplc_uv2_l_click(self):
        if self.ui.hplc_uv2_l.isChecked():
            self.ui.hplc_uv2_r.setChecked(False)
        if len(self.uv_num)>0 and not self.ui.hplc_uv2_r.isChecked():
            self.hplc_show_line(4)
        
    def hplc_uv2_r_click(self):
        if self.ui.hplc_uv2_r.isChecked():
            self.ui.hplc_uv2_l.setChecked(False)
        if len(self.uv_num)>0 and not self.ui.hplc_uv2_l.isChecked():
            self.hplc_show_line(4)
    
    def hplc_uv2_norm_click(self):
        if len(self.uv_num)>0:
            self.hplc_show_line(4)
    
    def hplc_uv12_l_click(self):
        if self.ui.hplc_uv12_l.isChecked():
            self.ui.hplc_uv12_r.setChecked(False)
        if len(self.uv_num)>0 and not self.ui.hplc_uv12_r.isChecked():
            self.hplc_show_line(5)
        
    def hplc_uv12_r_click(self):
        if self.ui.hplc_uv12_r.isChecked():
            self.ui.hplc_uv12_l.setChecked(False)
        if len(self.uv_num)>0 and not self.ui.hplc_uv12_l.isChecked():
            self.hplc_show_line(5)
    
    def hplc_uv12_norm_click(self):
        if len(self.uv_num)>0:
            self.hplc_show_line(5)
        
    def hplc_i0uv_l_click(self):
        if self.ui.hplc_i0uv_l.isChecked():
            self.ui.hplc_i0uv_r.setChecked(False)
        if len(self.uv_num)>0 and len(self.im_num_ar)>0 and not self.ui.hplc_i0uv_r.isChecked():
            self.hplc_show_line(6)
        
    def hplc_i0uv_r_click(self):
        if self.ui.hplc_i0uv_r.isChecked():
            self.ui.hplc_i0uv_l.setChecked(False)
        if len(self.uv_num)>0 and len(self.im_num_ar)>0 and not self.ui.hplc_i0uv_l.isChecked():
            self.hplc_show_line(6)
    
    def hplc_i0uv_norm_click(self):
        if len(self.uv_num)>0 and len(self.im_num_ar)>0:
            self.hplc_show_line(6)
    
    def hplc_uv_change(self):
        if len(self.uv1) > 0:
            plotuv1 = self.uv1+self.ui.hplc_uv1_yoffset.value()
            plotuv2 = self.uv2+self.ui.hplc_uv2_yoffset.value()
            uv12 = numpy.where(plotuv2==0,0,plotuv1/plotuv2)
            self.x_to_plot[1] = self.uv_num + self.ui.hplc_uv_xoffset.value()
        else:
            plotuv1 = ''
            plotuv2 = ''
            uv12 = ''
        
        # calculate i0/uv1
        if len(self.i0_ar)> 0 and len(plotuv1) > 0:
            dd = len(plotuv1)-len(self.i0_ar)
            i0uv = numpy.where(plotuv1==0,0,numpy.pad(self.i0_ar,(dd-int(self.ui.hplc_uv_xoffset.value()),int(self.ui.hplc_uv_xoffset.value())),'constant')/plotuv1)
        else:
            i0uv = ''
        
        # organize data to plot
        self.data_to_plot[3:] = [plotuv1,plotuv2,uv12,i0uv]
        
        
        
        for kk in numpy.arange(3,7):
            self.hplc_show_line(kk)

    
    def hplc_analysis(self,sample_name):
        ''' read dat files and calculate rg and i0
        '''
        
        # sample name + series number
        hplc_name = os.path.split(sample_name)[1]
        hplc_name_ser = hplc_name.split('_')[:-2]
        # get first and last .sub file
        files_list_all = sorted(glob.glob(os.path.join(self.hplc_sastool_folder,'{}*.dat'.format(hplc_name_ser))))
        files_list = files_list_all[int(self.ui.hplc_buf_num.value()):]
        first_num = int(files_list[0].split('_')[-1].split('.')[0])
        
        num_of_sam = len(files_list)
        
        # empty arrays, used for plotting later
        self.im_num_ar = numpy.zeros(num_of_sam)
        self.rg_ar = numpy.zeros(num_of_sam)
        self.i0_ar = numpy.zeros(num_of_sam)
        self.iqmin_ar = numpy.zeros(num_of_sam)
        
        for sub_file in files_list:
            # image number
            ii = int(sub_file.split('_')[-1].split('.')[0])
            
            #delete zeros from sub file:
            for line in fileinput.input(sub_file, inplace = 1):
                if (not re.match('.* 0 0',line) or fileinput.filelineno() > 30):
                    sys.stdout.write(line)
            
            # run autorg
            try:
                #rg_out = subprocess.check_output([autorg,'--sminrg=1','--smaxrg=1.3',sub_file])
                rg_out = subprocess.check_output(['autorg',sub_file])
                rg=float(rg_out.split()[2])
                
                #rg_stdev=rg_out.split()[4])
                i0=float(rg_out.split()[8])
                #i0_stdev=rg_out.split()[10]
            except:
                rg = 0
                i0 = 0
            
            if rg<0:
                rg = 0
            if i0<0:
                i0=0
            
            # read i_qmin value
            dat_d = numpy.genfromtxt(sub_file)
            iqmin = max(dat_d[self.ui.hplc_iqmin.value(),1],0)
    
            # put the values in arrayes for printing later
            self.im_num_ar[ii-first_num] = ii
            self.rg_ar[ii-first_num] = rg
            self.i0_ar[ii-first_num] = i0
            self.iqmin_ar[ii-first_num] = iqmin
    
    
    def hplc_checkboxes(self):
        ''' get status of checkboxes
        '''
        self.left_axes = numpy.array([self.ui.hplc_rg_l.isChecked(),self.ui.hplc_i0_l.isChecked(),self.ui.hplc_iqmin_l.isChecked(),self.ui.hplc_uv1_l.isChecked(),self.ui.hplc_uv2_l.isChecked(),self.ui.hplc_uv12_l.isChecked(),self.ui.hplc_i0uv_l.isChecked()])
        self.right_axes = numpy.array([self.ui.hplc_rg_r.isChecked(),self.ui.hplc_i0_r.isChecked(),self.ui.hplc_iqmin_r.isChecked(),self.ui.hplc_uv1_r.isChecked(),self.ui.hplc_uv2_r.isChecked(),self.ui.hplc_uv12_r.isChecked(),self.ui.hplc_i0uv_r.isChecked()])
        self.norm = numpy.array([self.ui.hplc_rg_norm.isChecked(),self.ui.hplc_i0_norm.isChecked(),self.ui.hplc_iqmin_norm.isChecked(),self.ui.hplc_uv1_norm.isChecked(),self.ui.hplc_uv2_norm.isChecked(),self.ui.hplc_uv12_norm.isChecked(),self.ui.hplc_i0uv_norm.isChecked()])
    
    def plot_hplc(self):
        # try to set folder for saving plots
        if os.path.exists(self.hplc_plots_folder):
            rcParams["savefig.directory"] = self.hplc_plots_folder
        else:
            rcParams["savefig.directory"] = os.path.abspath('.')
        
        # clear the graph
        self.ui.mpl_plot.canvas.ax1.cla()
        self.ui.mpl_plot.canvas.ax2.cla()
        
        self.hplc_checkboxes()
        
     
        # calculate uv1/uv2
        if len(self.uv1) > 0:
            plotuv1 = self.uv1+self.ui.hplc_uv1_yoffset.value()
            plotuv2 = self.uv2+self.ui.hplc_uv2_yoffset.value()
            uv12 = numpy.where(plotuv2==0,0,plotuv1/plotuv2)
        else:
            plotuv1 = ''
            plotuv2 = ''
            uv12 = ''
        
        # calculate i0/uv1
        if len(self.i0_ar)> 0 and len(plotuv1) > 0:
            dd = len(plotuv1)-len(self.i0_ar)
            i0uv = numpy.where(plotuv1==0,0,numpy.pad(self.i0_ar,(dd-int(self.ui.hplc_uv_xoffset.value()),int(self.ui.hplc_uv_xoffset.value())),'constant')/plotuv1)
        else:
            i0uv = ''
        
        # organize data to plot
        self.data_to_plot = [self.rg_ar,self.i0_ar,self.iqmin_ar,plotuv1,plotuv2,uv12,i0uv]
        self.x_to_plot = [self.im_num_ar,self.uv_num]
        if len(self.uv_num) > 0 :
                self.x_to_plot[1] = self.uv_num + self.ui.hplc_uv_xoffset.value()
        
        #right_yax = ''
        #left_yax = ''
        #sc = []
        # first plot non-normalized data
        for ii in numpy.arange(len(self.left_axes)):
            if len(self.data_to_plot[ii]) > 0:
                if (self.left_axes[ii] or self.right_axes[ii]) and not self.norm[ii]:
                    axpos = self.right_axes[ii]*1 # 0 for left axis, 1 for right
                    self.hplc_plot_lines[ii] = self.hplc_yaxes[axpos].plot(self.x_to_plot[self.hplc_xaxis[ii]],self.data_to_plot[ii],self.cc[ii],label = self.data_label[ii])
                    
                  
        self.hplc_norm = [self.ui.mpl_plot.canvas.ax1.get_ylim()[1]*0.95,self.ui.mpl_plot.canvas.ax2.get_ylim()[1]*0.95]

        
       
       # plot normalized data
        for ii in numpy.arange(len(self.left_axes)):
            if len(self.data_to_plot[ii]) > 0:
                if (self.left_axes[ii] or self.right_axes[ii]) and self.norm[ii]:
                    axpos = self.right_axes[ii]*1 # 0 for left axis, 1 for right
                    if ii<5:
                        self.hplc_plot_lines[ii] = self.hplc_yaxes[axpos].plot(self.x_to_plot[self.hplc_xaxis[ii]],self.hplc_norm[axpos]*self.data_to_plot[ii]/max(self.data_to_plot[ii][numpy.where(numpy.isfinite(self.data_to_plot[ii]))]),self.cc[ii],label = self.data_label[ii]+'$_{[norm]}$')
                    # special normalization for uv1/uv2 and I0/uv1
                    else:
                        self.hplc_plot_lines[ii] = self.hplc_yaxes[axpos].plot(self.x_to_plot[self.hplc_xaxis[ii]],self.hplc_norm[axpos]*(self.data_to_plot[ii]-min(self.data_to_plot[ii]))/max(self.data_to_plot[ii]-min(self.data_to_plot[ii])),self.cc[ii],label = self.data_label[ii]+'$_{[norm]}$')
           
                    
                    
              
        # et y-axes names and legend
        self.hplc_legend()
        
        # set x-axis name and plot title
        
        self.ui.mpl_plot.canvas.ax1.set_xlabel('image number',fontsize=18)
        self.ui.mpl_plot.canvas.ax2.set_title(self.hplc_title,fontsize=22)
        
        
        # read plot limits and set into spinboxes
        self.ui.hplc_left_min.setValue(self.ui.mpl_plot.canvas.ax1.get_ylim()[0])
        self.ui.hplc_left_max.setValue(self.ui.mpl_plot.canvas.ax1.get_ylim()[1])
        self.ui.hplc_right_min.setValue(self.ui.mpl_plot.canvas.ax2.get_ylim()[0])
        self.ui.hplc_right_max.setValue(self.ui.mpl_plot.canvas.ax2.get_ylim()[1])
        self.ui.hplc_x_min.setValue(self.ui.mpl_plot.canvas.ax1.get_xlim()[0])
        self.ui.hplc_x_max.setValue(self.ui.mpl_plot.canvas.ax1.get_xlim()[1])
        
        
        # draw grid and align both y-axes
        self.ui.mpl_plot.canvas.ax1.grid()
        self.ui.mpl_plot.canvas.ax1.yaxis.set_major_locator(LinearLocator(1+self.ui.hplc_yticks_num.value()))
        #self.ui.mpl_plot.canvas.ax1.yaxis.set_major_locator(AutoLocator())
        self.ui.mpl_plot.canvas.ax2.yaxis.set_major_locator(LinearLocator(1+self.ui.hplc_yticks_num.value()))
        #self.ui.mpl_plot.canvas.ax2.yaxis.set_major_locator(AutoLocator())
        
        
        self.ui.mpl_plot.canvas.draw_idle()
        

    def hplc_show_line(self,jj):
        ''' add/remove lines to the existing plot
        '''
        if len(self.hplc_plot_lines[jj]) > 0:
            self.hplc_plot_lines[jj].pop().remove()
        
        self.hplc_checkboxes()
        
        if self.left_axes[jj] == True or self.right_axes[jj] == True:
            axpos = self.right_axes[jj]*1 # 0 for left axis, 1 for right
            if not self.norm[jj]:
                self.hplc_plot_lines[jj] = self.hplc_yaxes[axpos].plot(self.x_to_plot[self.hplc_xaxis[jj]],self.data_to_plot[jj],self.cc[jj],label = self.data_label[jj])
            elif jj < 5:
                self.hplc_plot_lines[jj] = self.hplc_yaxes[axpos].plot(self.x_to_plot[self.hplc_xaxis[jj]],self.hplc_norm[axpos]*self.data_to_plot[jj]/max(self.data_to_plot[jj][numpy.where(numpy.isfinite(self.data_to_plot[jj]))]),self.cc[jj],label = self.data_label[jj]+'$_{[norm]}$')
            else:
                self.hplc_plot_lines[jj] = self.hplc_yaxes[axpos].plot(self.x_to_plot[self.hplc_xaxis[jj]],self.hplc_norm[axpos]*(self.data_to_plot[jj]-min(self.data_to_plot[jj]))/max(self.data_to_plot[jj]-min(self.data_to_plot[jj])),self.cc[jj],label = self.data_label[jj]+'$_{[norm]}$')
        
            
        #self.ui.mpl_plot.canvas.draw_idle()
        self.hplc_legend()
        self.replot_hplc()
        
        
    def hplc_legend(self):
        ''' show legend and y-axes titles
        '''
        # add legend
        lines1, labels1 = self.ui.mpl_plot.canvas.ax1.get_legend_handles_labels()
        lines2, labels2 = self.ui.mpl_plot.canvas.ax2.get_legend_handles_labels()
        
        lines = lines1+lines2
        labels = labels1+labels2
        
        
        # sort the labels in the same order as the data select buttons
        rr= numpy.zeros(len(lines))
        
        for ii in range(len(lines)):
            rr[ii] = self.data_label.index(labels[ii].split('$_{[norm]}$')[0])
        
        rr_s, lines_s, labels_s = zip(*sorted(zip(rr,lines,labels)))
        
        
        try:
            rr1 = numpy.zeros(len(lines1))
            for ii in range(len(lines1)):
                rr1[ii] = self.data_label.index(labels1[ii].split('$_{[norm]}$')[0])
        
            rr1_s, lines1_s, labels1_s = zip(*sorted(zip(rr1,lines1,labels1)))
        
        except:
            labels1_s = labels1
        try:
            rr2 = numpy.zeros(len(lines2))
            for ii in range(len(lines2)):
                rr2[ii] = self.data_label.index(labels2[ii].split('$_{[norm]}$')[0])
            
            rr2_s, lines2_s, labels2_s = zip(*sorted(zip(rr2,lines2,labels2)))
        except:
            labels2_s = labels2
        
        # set lagend
        if self.ll != '':
            #vv = self.ll.get_bbox_to_anchor().inverse_transformed(self.ui.mpl_plot.canvas.ax2.transAxes)
            vp = self.ll._loc_real
            self.ll.remove()
            # reposition the legend to the old location
            self.ll = self.ui.mpl_plot.canvas.ax2.legend(lines_s, labels_s, numpoints=1, loc = vp , fontsize=14, frameon=False, framealpha=0.5, labelspacing = 0.1)
        else:
            self.ll = self.ui.mpl_plot.canvas.ax2.legend(lines_s, labels_s, numpoints=1,loc=0, fontsize=14, frameon=False, framealpha=0.5,labelspacing = 0.1)
        
        self.ll.draggable(True)
        # add axes names
        self.ui.mpl_plot.canvas.ax1.set_ylabel(','.join(labels1_s),fontsize=18)
        self.ui.mpl_plot.canvas.ax2.set_ylabel(','.join(labels2_s),fontsize=18)
        
            
    
    def hplc_resize(self,event):
        if self.ll != '':
            if (self.left_axes[:3].any() and len(self.im_num_ar) >0) or (self.left_axes[3:].any() and len(self.uv_num) >0):
                self.ui.hplc_left_min.setValue(self.ui.mpl_plot.canvas.ax1.get_ylim()[0])
                self.ui.hplc_left_max.setValue(self.ui.mpl_plot.canvas.ax1.get_ylim()[1])
            
            if (self.right_axes[:3].any() and len(self.im_num_ar) >0) or (self.right_axes[3:].any() and len(self.uv_num) >0):
                self.ui.hplc_right_min.setValue(self.ui.mpl_plot.canvas.ax2.get_ylim()[0])
                self.ui.hplc_right_max.setValue(self.ui.mpl_plot.canvas.ax2.get_ylim()[1])
            
            self.ui.hplc_x_min.setValue(self.ui.mpl_plot.canvas.ax1.get_xlim()[0])
            self.ui.hplc_x_max.setValue(self.ui.mpl_plot.canvas.ax1.get_xlim()[1])
        
        # try: 
        #     self.leg_pos = self.ll.get_window_extent()
        # except:
        #     self.leg_pos = ''
        
    
    # end hplc tab
    
    # start integmpp tab
    
    def integmpp_open_file(self):
        ''' select integ.mpp top open
        '''
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        if self.analysis_folder != '':
            self.start_folder = self.analysis_folder
        else:
            self.start_folder=os.path.normpath(os.path.abspath('.'))
            
        self.integmpp_edit = str(QFileDialog.getOpenFileName(self,"Select integ.mpp file", self.start_folder,".mpp files (*.mpp);;All Files (*)", options=options))
        
        self.integmpp_toedit()
    
    
    def integmpp_toedit(self):
        with open(self.integmpp_edit,'r') as f_in:
            integdata = f_in.read()
    
        self.ui.integmpp_edit.setPlainText(integdata)
        self.ui.integmpp_label.setText(self.integmpp_edit)
    
    def integmpp_save_file(self):
        ''' save integ.mpp and remove all new lines
        '''
        with open(self.integmpp_edit,'w') as f_out:
            # remove new line at the end of the file
            #f_out.write(str(self.ui.integmpp_edit.toPlainText()).rstrip())
            f_out.write(str(self.ui.integmpp_edit.toPlainText()))
        
        for line in fileinput.input(self.integmpp_edit, inplace = 1):
            #if not re.match(r'^\s*$', line):
            if line.rstrip():
                sys.stdout.write(line)
        
    def integmpp_show_help(self):
        self.inhelp = QDialog(self)
        self.inhelp.resize(600,600)
        self.inhelp.setStyleSheet("QDialog{background:#f9f9f4;color:black;}")
        self.inhelp.setWindowTitle('integ.mpp help')
        self.inhelp_label = QLabel(self.inhelp)
        self.inhelp_label.setText('\n'
        #'switch: \t explanation:\n\n'
        '-l \t log-file name \n'
        '\t example: -l logfile.log \n\n'
        '-c \t beam center position in pixels\n'
        '\t example: -c 400.34 500.45 \n\n'
        '-q \t conersion form pixels to q (not required) \n'
        '\t example: -q yes 0 0.0 102 0.1076 \n\n'
        '-i \t use i1 or i2 for data normalization (not required) \n'
        '\t example: -i2 yes 1000 \n\n'
        '-r \t variance factor for rejection of individual files from \n\t the summation of the whole series (not required) \n'  
        '\t example: -r yes 3.5\n\n'
        '-s \t subtract buffer intensity from individual sample frames (not required) \n'
        '\t example: -s yes \n\n'
        '-t \t re-use existing buffer .tot file (not required) \n'
        '\t example: -t yes\n\n'
        '-m \t .tif mask file \n'
        '\t example: -m yes /path/to/mask.tif \n\n'
        '-z \t use dezinger (not required) \n'
        '\t example: -z yes 3.5 \n\n'
        '-o \t detector offset (not required, default 10)\n'
        '\t example: -o 0 \n\n'
        '-f \t file(s) to integrate \n'
        '\t example: -f ./path/sample.tif\n'
        '\t example: -f ./path/sample.tif ./path/buffer.tif \n')
        self.inhelp.show()
    
    
    # end integmpp tab
    
    ## main window
    
    def status_message(self):
        ''' change status message
        '''
        if self.ui.set_angles_man.isChecked() or self.ui.set_center_1.isChecked() or self.ui.add_polygon.isChecked() or self.ui.remove_polygon.isChecked() or self.ui.add_pixels.isChecked() or self.ui.remove_pixels.isChecked() or self.ui.zoom_select.isChecked() or self.ui.mask_zoom_select.isChecked():
            self.ui.statusBar.setStyleSheet("QStatusBar{background:yellow;color:black;font-weight:bold;}")
            self.ui.statusBar.showMessage("           Select point(s) with mouse left-click")
        else:
            self.ui.statusBar.setStyleSheet("QStatusBar{background:None}")
            self.ui.statusBar.clearMessage()
            app.restoreOverrideCursor()
            self.ui.graphicsView_mask.setDragMode(QGraphicsView.ScrollHandDrag)
            self.ui.graphicsView.setDragMode(QGraphicsView.ScrollHandDrag)
            
    
    def update_footer(self):
        ''' update footer info
        '''
        self.ui.data_folder.setText("data folder: {}".format(self.data_folder))
        self.ui.analysis_folder.setText("analysis folder: {}".format(self.analysis_folder))
        self.ui.radint_data_text.setText('{}'.format(self.data_folder))
        if self.ui.use_saxspipe.isChecked():
            self.ui.radint_analysis_text.setText(os.path.normpath(os.path.join(self.data_folder,'../analysis/')))
        else:
            self.ui.radint_analysis_text.setText('{}'.format(self.analysis_folder))
        
        
        if self.agbe_file != '':
            self.ui.agbe_file.setText("agbe file: {}  {}x{}".format(self.agbe_file, self.agbe_size_h, self.agbe_size_v))
        else:
            self.ui.agbe_file.setText("agbe file: {}".format(self.agbe_file))
        if self.calc_dist == 0:
            self.calc_dist_str = ''
        else:
            self.calc_dist_str = '{:.1f}'.format(self.calc_dist) + ' mm'
        self.ui.calculated_distance.setText("calculated distance: {}".format(self.calc_dist_str))
        
        if self.cx1 != '':
            if self.cx2 != '':
                self.ui.center_info.setText('center: {}, {} (rough);\t{:.2f}, {:.2f} (refined)'.format(self.cx1,self.agbe_size_v-self.cy1,self.cx2,self.agbe_size_v-self.cy2))
            else:
                self.ui.center_info.setText('center: {}, {} (rough)'.format(self.cx1,self.agbe_size_v-self.cy1))
        else:
            self.ui.center_info.setText('center:')
    
       
if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MyWindow()
    sys.exit(app.exec_())
    
    