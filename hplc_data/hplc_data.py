#!/usr/bin/env python

'''
@author: rajkovic
'''

import numpy
#from __future__ import division

from bokeh.layouts import gridplot, row
from bokeh.plotting import figure, show, output_file, save
from bokeh.resources import INLINE
from bokeh.models import HoverTool, ColumnDataSource, LinearAxis, Range1d


import sys
import os
#import re
#import shutil
#import distutils.spawn
#from distutils.dir_util import mkpath
import subprocess
import glob

import matplotlib.pyplot as plt


if __name__ == "__main__":
    
    if not (len(sys.argv) == 6 or len(sys.argv)==4):
        sys.exit("""\n\n\tHow-to:
        
        {0} <path-to-dat-file> <q_min_point_number> <num_buf>
        
        """.format(os.path.basename(sys.argv[0])))
        
    # set some variables from arguments 
    #data_fol = os.path.dirname(os.path.abspath(sys.argv[1]))
    sample_name_path = '_'.join(sys.argv[1].split('_')[0:-3])
    q_min = int(sys.argv[2])
    num_buf = int(sys.argv[3])
    
    
    list_dat = sorted(glob.iglob(sample_name_path+'*.dat'))
    img_num = []
    rg = []
    i0 = []
    iqmin = []
    for dat_file in list_dat:
        
        dat_num = int(dat_file.split('_')[-1].split('.')[0])
        
        if num_buf < dat_num :
            img_num.append(dat_num)
            try:
                #rg_out = subprocess.check_output(['autorg','--sminrg=1','--smaxrg=1.3',dat_file])
                rg_out = subprocess.check_output(['autorg',dat_file])
                rg.append(float(rg_out.split()[2]))
                #rg_stdev=rg_out.split()[4])
                i0.append(float(rg_out.split()[8]))
                #i0_stdev=rg_out.split()[10]
            except:
                rg.append(0)
                i0.append(0)
            
            dat_data = numpy.loadtxt(dat_file)
            iqmin.append(dat_data[q_min-1,1])
        
        
    
    
    source = ColumnDataSource(data=dict(x=img_num, y0=i0, y1=iqmin,y2=rg))
    
    #hover = HoverTool(tooltips=[("(x,y)", "(@x, $y)")])
    
    p1 = figure(toolbar_location="above",toolbar_sticky=False,tools="pan,box_zoom,zoom_in,zoom_out,wheel_zoom,box_select,undo,reset")
    #p1.add_tools(hover)
    p1.circle('x','y0',size=8,color="navy",source=source,legend='I0')
    p1.triangle('x','y1',size=8,color="red",source=source,legend='I_qmin')
    p1.add_tools(HoverTool(show_arrow=False,  tooltips=[('num', '@x'),('Rg', '@y2{0.0}'),('I0', '@y0{0.0}'),('I_qmin', '@y1{0.0}')]))
    p1.legend.click_policy="hide"
    
    
    p2 = figure(toolbar_location="above",toolbar_sticky=False,tools="pan,box_zoom,zoom_in,zoom_out,wheel_zoom,box_select,undo,reset",x_range=p1.x_range)
    #p2.add_tools(hover)
    p2.inverted_triangle('x','y2',size=8,color='green',source=source,legend='Rg')
    p2.add_tools(HoverTool(show_arrow=False, tooltips=[('num', '@x'),('Rg', '@y2{0.0}'),('I0', '@y0{0.0}'),('I_qmin', '@y1{0.0}')]))
    
    p3 = figure(toolbar_location="above",toolbar_sticky=False,tools="pan,box_zoom,zoom_in,zoom_out,wheel_zoom,box_select,undo,reset",x_range=p1.x_range)
    p3.circle('x','y0',size=8,color="navy",source=source,legend='I0')
    p3.triangle('x','y1',size=8,color="red",source=source,legend='I_qmin')
    p3.extra_y_ranges = {"Rg": Range1d(start=-10, end=1.3*max(rg))}
    p3.add_layout(LinearAxis(y_range_name="Rg"), 'right')
    p3.inverted_triangle('x','y2',size=8,source=source,y_range_name='Rg',color="green",legend='Rg')
    p3.add_tools(HoverTool(show_arrow=False, tooltips=[('num', '@x'),('Rg', '@y2{0.0}'),('I0', '@y0{0.0}'),('I_qmin', '@y1{0.0}')]))
    p3.legend.click_policy="hide"
    
    html_file = os.path.basename(sample_name_path)+'_secsaxs.html'
    output_file(html_file, title="{} hplc data".format(os.path.basename(sample_name_path)), mode='inline')
    save(row(p1,p2,p3))
