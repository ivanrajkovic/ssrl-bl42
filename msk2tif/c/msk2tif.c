#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdint.h>
#include <tiffio.h>
#include <string.h>	

unsigned int header[256];
int i, d1, d2, num_ints, yy, zz, xx;
TIFF *img;
int main(int argc, char *argv[])
{
	if (argc!=2 && argc!=3)
		{
		printf("Usage: %s <infile.msk> [outfile.tif]\n",argv[0]);
		exit(0);
		}
	
	FILE *msk_file;
//	msk_file=fopen("fit2d.msk","rb");   
	msk_file=fopen(argv[1],"rb");
	if (!msk_file)
	{
		return 1;
	}
    
    fread(header,sizeof(header),1,msk_file);

	d1 = header[4];
	d2 = header[5];

	//printf("\n d1 = %u ; d2 = %u \n",d1,d2);


	num_ints = floor((d1 + 31)/32);
	//printf("num_ints = %i \n",num_ints);
	unsigned char data[num_ints*d2*4];
	// unsigned short results[d2][num_ints*4*8];
	unsigned short results[d2][d1];
	      
   	
	fread(&data,num_ints*d2*4,1,msk_file);
        
	for (yy = 0; yy<d2; yy++)
		{ 
		for (xx = 0; xx<num_ints*4; xx++)
			{ //int ooo = 1;
			for (zz=0 ; zz<8 ; zz++)
				{
				// add condition xx*8+zz <= d1
				if(xx*8+zz<d1)
					{
					if((data[yy*num_ints*4+xx]&(1<<zz))==0)
					//if((data[yy*num_ints*4+xx]&&ooo)==0)
						{ results[yy][xx*8+zz]=1;
						}
					else
						{ results[yy][xx*8+zz]=0;
						}
					}					
					//ooo=ooo*2;
				}
			}
		}
        
    if (argc == 2)
		{
		
        // version 1

        //char *dot, *outfile;
		//int len;
		//dot = strrchr(argv[1], '.');
		//if(dot) len = dot-argv[1];
        //else len = strlen(argv[1]);
		//outfile=malloc(len+5);
		//strncpy(outfile, argv[1], len);
		//strcpy(outfile + len, ".tif");
        

        // version 2        

        //char *token;
        //char delim[] = ".";
        
        //char *outfile = calloc(strlen(argv[1])+5,sizeof(char));
        //*outfile = '\0';

        //token = strtok(argv[1], delim);
        //while(token != NULL) {
        //    if (strcmp(token,"msk") != 0) {       
        //        strcat(outfile, token);        
        //        strcat(outfile, ".");       
        //    }
        //    token = strtok(NULL, delim);
        //}

        // version 3

        char *dot = strrchr(argv[1], '.');
        char *outfile = malloc(strlen(argv[1])+5);
     
        strcpy(outfile,argv[1]);
        if (dot && strcmp(dot,".msk") == 0) outfile[strlen(outfile)-4] = '\0' ;
        strcat(outfile,".tif");


        printf("\nMask filename is: %s\n\n",outfile);
		img = TIFFOpen (outfile, "w");
		}
	else 
		{
		img = TIFFOpen (argv[2], "w");
		}


	
	TIFFSetField (img, TIFFTAG_IMAGELENGTH, d2);
 	//TIFFSetField (img, TIFFTAG_IMAGEWIDTH, num_ints*4*8);
    TIFFSetField (img, TIFFTAG_IMAGEWIDTH, d1);
    //TIFFSetField (img, TIFFTAG_YRESOLUTION,1);
	//TIFFSetField (img, TIFFTAG_XRESOLUTION,1);
	//TIFFSetField (img, TIFFTAG_RESOLUTIONUNIT, 1);
	//TIFFSetField (img, TIFFTAG_SAMPLESPERPIXEL, 1);
	//TIFFSetField (img, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);
	TIFFSetField (img, TIFFTAG_BITSPERSAMPLE, 16);
	TIFFSetField (img, TIFFTAG_COMPRESSION, COMPRESSION_NONE);
	//TIFFSetField (img, TIFFTAG_PLANARCONFIG, 1);
 	//TIFFSetField (img, TIFFTAG_SAMPLEFORMAT, 1);
	TIFFSetField (img, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);
	TIFFSetField (img, TIFFTAG_ROWSPERSTRIP, d2);

	for (i = 0 ; i<d2 ; i++)
		{        
      		TIFFWriteScanline (img, results[d2-i-1], i, 0);
    		}


	TIFFClose (img);


}
