#!/usr/bin/env python

'''
@author: rajkovic
'''
from __future__ import division

# import time
# starttime = time.time()

import sys
import os
import re
import distutils.spawn
from distutils.dir_util import mkpath
import shutil
import fileinput
import subprocess
import subprocess32
import glob

# importing modules later, while sastool is running 

# import matplotlib
# matplotlib.use('Agg')
# import matplotlib.pyplot as plt
# import numpy
# from heapq import nlargest, nsmallest

# from bokeh.layouts import gridplot, row
# from bokeh.plotting import figure, output_file, save
# from bokeh.resources import INLINE
# from bokeh.models import HoverTool, Range1d, ColumnDataSource
# import bokeh.settings

#import platform
#from colorama import init
#from colorama import Fore, Back, Style


def set_path():
    global autorg
    global datgnom
    global sastool
    
    atsas_path='/mnt/home/staff/SW/ATSAS'
    exe_path='/mnt/home/sw/bin:{}'.format(os.path.join(atsas_path,'bin'))
    ld_path=os.path.join(atsas_path,'lib64/atsas')
    
    try:
        os.environ['LD_LIBRARY_PATH']=os.environ['LD_LIBRARY_PATH']+':'+ld_path
    except:
        os.environ['LD_LIBRARY_PATH']=ld_path
    
    try:
        os.environ['PATH']=os.environ['PATH']+':'+exe_path
    except:
        os.environ['PATH']=exe_path
            
    
    sastool = distutils.spawn.find_executable('sastool')

    autorg = distutils.spawn.find_executable('autorg')

    datgnom = distutils.spawn.find_executable('datgnom')
    
    
    if autorg == None:
        #sys.exit('\'autorg\' not found, please add it to the path.')
        print('\'autorg\' not found, please add it to the path.')
    if datgnom == None:
        #sys.exit('\'datgnom\' not found, please add it to the path.')
        print('\'datgnom\' not found, please add it to the path.')
    if sastool == None:
        sys.exit('\'sastool\' not found, please add it to the path.')
        #print('\'sastool\' not found, please add it to the path.')
    
        

def find_image(folder,num):
    im_list=[]
    # find the image filename for the given series number
    rrr = re.compile('.*[SBT]'+str(num).zfill(3)+'_0_0+1\.tif')
    for file in os.listdir(folder):
        if rrr.match(file):
            im_list.append(file)
    if len(im_list) == 0:
        sys.exit("\nNo files found!\n")
    elif len(im_list) > 1:
        print("\n\033[0;37;41m\033[1m\033[4m Two or more files found! \033[0;0m")
        sys.exit(im_list)
    else:
        return im_list[0]
        
def find_last_buf(folder,num):
    bb = 0
    b_num = num
    buf_name=[]
    while bb == 0:
        b_num = b_num-1
        if b_num == 0:
            sys.exit('\nNo buffer found!')
        rrb=re.compile('.*[B]'+str(b_num).zfill(3)+'.*_0_0+1\.tif')
        for file in os.listdir(folder):
            if rrb.match(file):
                buf_name = file
                bb = 1
    return (buf_name)



# find all buffer files
# rrs = re.compile('.*_[B]\d\d\d_\d_0+1\.tif')
# rrs = re.compile('.*_[B]\d{3}_\d_0+1\.tif')


#######################################################
############### main program ##########################
#######################################################

if __name__ == "__main__":
    
    # get some helpfull messages (1 -> yes, anything else -> no)
    debug = 0
    
    # if no or more than two arguments -> show help
    if (len(sys.argv) == 1 or len(sys.argv)>3):
        sys.exit("""\n\n\tHow-to:
        
        {0} <sample> [buffer]
        
        If only a sample file is given, program will search for the most recent buffer file and, if found, use it for analysis. To analyze the sample without the buffer subtraction input '0' as the buffer.
         If only a sample is given and the file has buffer designation (Bxxx in the filename), it will be buffer subtracted only if the found buffer file has the same sample name as the sample file. This is for the case of buffers taken during one concentration series. If the previous buffer has different name, there will be no buffer subtraction. In case that both sample and buffer are given on the command line this test is skipped.
        
        Sample and buffer can be specified in three different ways:
        
        1) full path to the file: {0} /path/to/my/data/file.tif
        2) relative path to the file: {0} ../path/data/file.tif
        3) series number of the file: {0} 6 
            for case 3), data are expected to be in ../data/ folder 
        
        In case 1), results will be saved in /path/to/my/analysis/automatic_analysis/
        In case 2) and 3), results will be saved in ./automatic_analysis/
        
        
        
        """.format(os.path.basename(sys.argv[0])))
    
    # analysis folder: current if sample file specified as relative path or series number, next to 'data' if sample file is apsolute path 
    
    if os.path.isabs(os.path.expanduser(sys.argv[1])):
        data_fol = os.path.realpath(os.path.dirname(sys.argv[1]))
        analysis_fol = os.path.normpath(os.path.join(data_fol,'../analysis/'))
    elif os.path.isfile(sys.argv[1]):
        data_fol = os.path.realpath(os.path.abspath(os.path.dirname(sys.argv[1])))
        analysis_fol = os.path.abspath('.')
    else:
        data_fol = os.path.abspath('../data/')
        analysis_fol = os.path.abspath('.')
        
   
    # set paths and folders
    set_path()
    
    # these will be created if they don't exist
    results_fol = os.path.join(analysis_fol,'automatic_analysis')
    sastool_sfol = os.path.join(results_fol,'sastool')
    other_sfol = os.path.join(results_fol,'other_files')
    subfiles_sfol = os.path.join(results_fol,'sub_files')
    res_html = os.path.join(results_fol,'results.html')
    t_mpp = os.path.join(results_fol,'template.mpp')
    save_file = os.path.join(results_fol,'save.dat')
    
    sample_list = os.path.join(other_sfol,'sample_list.txt')
    
    if debug == 1:
        print('\n\n\tData folder is: '+'\n'+data_fol+'\n')
        print('\tAnalysis folder is: '+'\n'+analysis_fol+'\n')
    
    #    
    ## find sample/buffer names
    #
    
    # find the sample name from first argument; first check if it a series number or a file name
    try:
        sn = int(sys.argv[1])
        sample_file = []
        if debug == 1:
            print('\tSpecified sample series number:\n'+str(sn)+'\n')
    except:
        sn = int(sys.argv[1].split('_')[-3][1:])
        sample_file = os.path.abspath(sys.argv[1])
        if debug == 1:
            print('\tSpecified sample name:\n'+sample_file+'\n')
            print('\tSample series number from file name:\n'+str(sn)+'\n')
    
    # if the first argument is a number, search for the file name
    if sample_file == []:
        try:
            sample_file=os.path.join(data_fol,find_image(data_fol,sn))
            if debug == 1:
                print('\tSample name from series number:\n'+sample_file+'\n')
        except:
            print('\n \033[0;37;41m\033[1m\033[4m Sample file not found! \033[0;0m \n')
            sys.exit()
    
    # if only one argument, search for the last buffer
    if len(sys.argv) == 2:          
        try:
            buffer_file = os.path.join(data_fol,find_last_buf(data_fol,sn))
            bn =  int(buffer_file.split('_')[-3][1:])
        except:
            print('\n \033[0;37;41m\033[1m\033[4m Buffer file not found! \033[0;0m ')
            print('\n \033[0;37;41m\033[1m\033[4m Working without a buffer \033[0;0m \n')
            bn = 0
            buffer_file = ''
        
        if debug == 1:
            print('\tLast buffer series number before specified sample:\n'+str(bn)+'\n')
            print('\tLast buffer name before specified sample:\n'+buffer_file+'\n')
        
        # If provided only sample and it is designated as a buffer (Bnnn), check if the previous buffer is from the same row
        # if not, set bn = 0, buffer_file ='' (no buffer subtraction)
        
        if bn != 0 and os.path.basename(sample_file).split('_')[-3][0] == 'B':
            if not '_'.join(os.path.basename(sample_file).split('_')[0:-4]) == '_'.join(os.path.basename(buffer_file).split('_')[0:-4]):
                bn = 0
                buffer_file = ''
                print('First buffer of the series, working without buffer subtraction.')    
            
    # if there is a second argument, find the buffer name; first check if it a series number or a file name  
    if len(sys.argv) == 3:
        try:
            bn = int(sys.argv[2])
            buffer_file = []
            if debug == 1:
                print('\tSpecified buffer series number:\n'+str(bn)+'\n')
        except:
            bn = int(sys.argv[2].split('_')[-3][1:])
            buffer_file = os.path.realpath(os.path.abspath(sys.argv[2]))
            if debug == 1:
                print('\tSpecified buffer name:\n'+buffer_file+'\n')
                print('\tBuffer series number from file name:\n'+str(bn)+'\n')
        
        if bn == 0:
            buffer_file = ''
            if debug == 1:
                print('No buffer file provided.\n')
        
        # if the second argument is a number, search for the file name
        if buffer_file == []:
            try:
                buffer_file=os.path.join(data_fol,find_image(data_fol,bn))
                if debug == 1:
                    print('\tBuffer name from series number:\n'+buffer_file+'\n')
            except:
                print('\n \033[0;37;41m\033[1m\033[4m Buffer file not found! \033[0;0m \n')
                sys.exit()
    
    
    
    
    
    # get acquisition file of the first sample file
    #print(os.path.splitext(sample_file)[0]+'.prp')
    try:
        for line in  fileinput.input(os.path.splitext(sample_file)[0]+'.prp'):
            if line.startswith('Time this file was written'):
                acq_time = line.split('written: ')[-1][:-1]
    except:
        try:
            with open('_'.join(sample_file.split('_')[:-1])+'_.log') as logfile:
                ll = logfile.readlines()[1]
                acq_time = ' '.join(ll.split()[2].split('T'))
                
        except:
            print('\n \033[0;37;41m\033[1m\033[4m Cannot get sample file info (.prp or .log file) \033[0;0m \n')
            acq_time = '0000'
    
    
    #
    ## prepare folders, integ.mpp, and run sastool
    #
    mkpath(sastool_sfol)
    mkpath(other_sfol)
    mkpath(subfiles_sfol)


    # copy integ.mpp to the automatic_analysis folder as template.mpp, remove all lines starting with '-f' or '#', and all empty lines
    # if the buffer is empty comment out '-s'; if not, uncomment '-s' if it was commented out
    # add full path to the mask file, if necessary
    
    shutil.copy(os.path.join(analysis_fol,'integ.mpp'),t_mpp)
    
    for line in fileinput.input(t_mpp, inplace = 1):
        if line.startswith('-s') or line.startswith('#-s') or line.startswith('# -s'):
            line = re.sub('# *-s','-s',line)
            if buffer_file == '':
                line = '#'+line
        if line.startswith('-m'):
            mask_file = line.split()[-1]
            if os.path.isabs(mask_file) == False:
                line = '-m yes {} \n'.format(os.path.abspath(os.path.join(analysis_fol,mask_file)))
        if (not line.startswith('-f') and not line.startswith('#') and not re.match(r'^\s*$', line)):        
            sys.stdout.write(line)


    # add files to be processed
    ff = open(t_mpp,'a')
    ff.write('-f '+sample_file+' '+buffer_file)
    ff.close()
    

    # run sastool in sastool subfolder
    os.chdir(sastool_sfol)
    psas = subprocess.Popen([sastool,t_mpp],cwd=sastool_sfol)
    
    #import modules while sastool is working
    
    import matplotlib
    matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    import numpy
    from heapq import nlargest, nsmallest
    
    from bokeh.layouts import gridplot, row
    from bokeh.plotting import figure, output_file, save
    from bokeh.resources import INLINE
    from bokeh.models import HoverTool, Range1d, ColumnDataSource
    import bokeh.settings
    
    import urllib
    
    psas.communicate()
    
    #subprocess.call([sastool,t_mpp])
    

    # for (sample - buffer), copy/rename .sub files to sub_files subfolder
    
    if bn != 0:
        sub_orig = os.path.join(sastool_sfol,os.path.splitext(os.path.basename(sample_file))[0]+'.sub')
        sub_file = os.path.join(subfiles_sfol,'_'.join(os.path.basename(sample_file).split('_')[:-2])+'_'+buffer_file.split('_')[-3]+"_sub.dat")
        
        shutil.copy(sub_orig,sub_file)
    
        top_zeros = 0
        for line in fileinput.input(sub_file, inplace = 1):
            if (not re.match('.* 0 0',line) or top_zeros == 1):
                sys.stdout.write(line)
                top_zeros = 1
    
    # sastool log file for this run
    sample_log = os.path.join(sastool_sfol,os.path.splitext(os.path.basename(sample_file))[0]+'.log')
    if bn != 0:
        buffer_log = os.path.join(sastool_sfol,os.path.splitext(os.path.basename(buffer_file))[0]+'.log')
    
    #
    ## Get info from processed files
    #
    
    # get the number of processed and rejected sample/buffer files; get variance
    sam_var=[]
    if bn != 0:
        tot_bf = -1
        rej_bf = -1
    for line in fileinput.input(sample_log):
        if bn != 0:
            if line.startswith('Buffer'):
                tot_bf =  int(line.split(' ')[1].split(':')[1])
                rej_bf =  int(line.split(' ')[2].split(':')[1])
        if line.startswith('Sample'):
            tot_sf =  int(line.split(' ')[1].split(':')[1])
            rej_sf =  int(line.split(' ')[2].split(':')[1])
        
        if line.startswith('frame'):
            sam_var.append(float(line.split()[4]))
        if line.startswith('Variance used'):
            variance_num = float(line.split(' ')[-1])
        if line.startswith('Factor'):
            var_factor = float(line.split(' ')[-1])
    if bn != 0:
        buf_var=[]
        for line in fileinput.input(buffer_log):
            if line.startswith('frame'):
                buf_var.append(float(line.split()[4]))
            
    if debug == 1:
        if bn != 0:
            print('\nBuffer files: total = {0}, rejected = {1}'.format(tot_bf,rej_bf))
            print('\nBuffer variance: {}'.format(buf_var))
        print('\nSample files: total = {0}, rejected = {1}\n'.format(tot_sf,rej_sf))
        print('\nSample variance: {}\n'.format(sam_var))
    
    # get .tot files data
    sam_tot=os.path.join(sastool_sfol,os.path.splitext(os.path.basename(sample_file))[0]+'.tot')
    sam_tot_data = numpy.loadtxt(sam_tot)
    if bn != 0:
        buf_tot=os.path.join(sastool_sfol,os.path.splitext(os.path.basename(buffer_file))[0]+'.tot')
        buf_tot_data = numpy.loadtxt(buf_tot)
    

    #
    ## run autorg and datgnom (if found)
    #
    
    # create list of .dat files
    dat_list = sorted(glob.glob(os.path.join(sastool_sfol,"_".join(os.path.basename(sample_file).split("_")[:-1])+'*.dat')))
    
    # only run if buffer-subtracted sample
    if bn != 0 and os.path.basename(sample_file).split('_')[-3][0] != 'B':
        
        # check if we have autorg
        if autorg != None:
            rg_single = []
            rg_single_del = []
            i0_single = []
            i0_single_del = []
            
            # autorg on .dat files
            for ii in dat_list:
                try:
                    rg_out = subprocess.check_output([autorg,ii])
                    rg_file = open(os.path.join(other_sfol,os.path.splitext(os.path.basename(ii))[0]+'_'+buffer_file.split('_')[-3]+"_rg.log"),'w')
                    rg_file.write(rg_out)
                    rg_file.close()
                    rg_single.append(float(rg_out.split()[2]))
                    rg_single_del.append(float(rg_out.split()[4]))
                    i0_single.append(float(rg_out.split()[8]))
                    i0_single_del.append(float(rg_out.split()[10]))
                except:
                    #print('No Rg found for {}'.format(os.path.basename(ii)))
                    rg_single.append(0)
                    rg_single_del.append(0)
                    i0_single.append(0)
                    i0_single_del.append(0)
            # autorg on the .sub file
            rg_file_name = os.path.join(other_sfol,'_'.join(os.path.basename(sample_file).split('_')[:-2])+'_'+buffer_file.split('_')[-3]+"_sub_rg.log")
            try:
                rg_out = subprocess.check_output([autorg,sub_file])
                rg_file=open(rg_file_name,'w')
                rg_file.write(rg_out)
                rg_file.close()
                rg_sub = [float(rg_out.split()[2])]
                rg_sub_del = [float(rg_out.split()[4])]
                i0_sub = [float(rg_out.split()[8])]
                i0_sub_del = [float(rg_out.split()[10])]
                rg_range = " ".join(rg_out.split()[12:17])
                rg_start = int(rg_range.split()[0])
                rg_end = int(rg_range.split()[2])
            except:
                rg_sub = [0]
                rg_sub_del = [0]
                i0_sub = [0]
                i0_sub_del = [0]
                rg_range=''
        
                
        # check if we have datgnom    
        if datgnom != None:
            
            #   Don't run datgnom on individual .dat files, only on .sub  
            
            # dmax_single=[]
            # guin_rg_single=[]
            # gnom_rg_single=[]
            # for ii in dat_list:
            #     try:
            #         datgnom_out = subprocess.check_output([datgnom,ii])
            #         dmax_single.append(float(datgnom_out.split()[6]))
            #         guin_rg_single.append(float(datgnom_out.split()[12]))
            #         gnom_rg_single.append(float(datgnom_out.split()[15]))
            #     except:
            #         print('No datgnom results for {}'.format(os.path.basename(ii)))
            #         dmax_single.append(0)
            #         guin_rg_single.append(0)
            #         gnom_rg_single.append(0)
            
            # datgnom on .sub file
            dg_file_name = os.path.join(other_sfol,'_'.join(os.path.basename(sample_file).split('_')[:-2])+'_'+buffer_file.split('_')[-3]+"_sub_datgnom.log")
            try: 
                datgnom_out = subprocess32.check_output([datgnom,sub_file],timeout=15)
                dg_file=open(dg_file_name,'w')
                dg_file.write(datgnom_out)
                dg_file.close()
                dmax_sub = float(datgnom_out.split()[6])
                guin_rg_sub = float(datgnom_out.split()[12])
                gnom_rg_sub = float(datgnom_out.split()[15])
            except:
                print('No datgnom for {}'.format(os.path.splitext(os.path.basename(sample_file))[0]+"_sub.dat"))
                gnom_rg_sub=0
                guin_rg_sub=0
                dmax_sub=0
                
    
        # check if rg values are similar (within 5%)
    
        if rg_sub[0]*gnom_rg_sub != 0:
            if abs((rg_sub[0]-gnom_rg_sub)/rg_sub[0])> 0.05:
                data_quality = "Check data - real and reciporocal Rgs differ for more than 5%"
            else:
                data_quality = "--"
        else:
            data_quality = "No usable Rg"
    

    #           
    # add sample and buffer name to the list of processed samples; check if the same sample series as previous and has same buffer subtracted
    # Don't do this for buffers, even if buffer subtracted
    #
    if bn != 0 and os.path.basename(sample_file).split('_')[-3][0] != 'B':
        sample_backup = sample_list+'.bak'
        connected_samples_list = []
        if os.path.isfile(sample_list):
            # try to get the same name samples only if one after another
            samples_connected = 1
            # write modifications into a backup file, then move if no errors
            sb_write = open(sample_backup,'w')
            
            for line in fileinput.input(sample_list):
                # check if there are samples from the same row only if sample_connected is still 1; if not, just copy the line
                if samples_connected == 1:
                    
                    
                    
                    
                    
                    
                    # add the new sample to the top of the list
                    if fileinput.isfirstline():
                        line = line+os.path.basename(sample_file)+'\t'+os.path.basename(buffer_file)+'\t\t'+str(rg_sub[0])+'\t\t'+str(rg_sub_del[0])+'\t\t'+str(i0_sub[0])+'\t\t'+str(i0_sub_del[0])+'\n'
                    # check if it is the same basename and the same series (same row in the autosampler) and the same buffer
                    elif '_'.join(line.split()[0].split('_')[0:-4]) == '_'.join(os.path.basename(sample_file).split('_')[0:-4]) and \
                    line.split()[0].split('_')[-4][:2] == os.path.basename(sample_file).split('_')[-4][:2] and \
                    '_'.join(line.split()[1].split('_')[0:-2]) == '_'.join(os.path.basename(buffer_file).split('_')[0:-2]):
                        connected_samples_list.append([line.split()[0],line.split()[1]])
                        rg_sub.append(float(line.split()[2]))
                        rg_sub_del.append(float(line.split()[3]))
                        i0_sub.append(float(line.split()[4]))
                        i0_sub_del.append(float(line.split()[5]))
                        
                    else:
                        # if not the same sample and not the first line (headers), samples are not in the same row
                        if not fileinput.isfirstline():
                            samples_connected = 0
                
                sb_write.write(line)
                
            sb_write.close()
            
            
        else:
            samples_connected = 1
            ff = open(sample_backup,'w')
            ff.write('Sample name\t\t\t\tBuffer name\t\t\tRg\t\t+/- Rg\t\tI0\t\t+/-I0\n')
            ff.write(os.path.basename(sample_file)+'\t'+os.path.basename(buffer_file)+'\t\t'+str(rg_sub[0])+'\t\t'+str(rg_sub_del[0])+'\t\t'+str(i0_sub[0])+'\t\t'+str(i0_sub_del[0])+'\n')
            ff.close()
        
         # move backup to sample_list file at the end, after html and plot files are created
        
        if debug == 1:
            print('Rg values for the {0} row: {1}'.format('_'.join(os.path.basename(sample_file).split('_')[0:-4]),rg_sub))
            print('I0 values for the {0} row: {1}'.format('_'.join(os.path.basename(sample_file).split('_')[0:-4]),i0_sub))

    #
    ## make graphs
    #
    font = {'family' : 'serif',
            'weight' : 'normal',
            'size'   : 16}
    
    matplotlib.rc('font', **font)
    
    
    
    # make image graphs
    plt.figure(num=None, figsize=(16,4))
    plt.subplot(141)
    #plt.hold(True)
    plt.title("Variance")
    if bn != 0:
        plt.plot(range(2, len(buf_var)+1), buf_var[1:],'o-')
    plt.plot(range(2, len(sam_var)+1), sam_var[1:],'o-')
    plt.plot(range(1, len(sam_var)+2),variance_num*var_factor*numpy.ones(len(sam_var)+1),'--')
    plt.locator_params(nbins=4)
    
    
    plt.subplot(142)
    #plt.hold(True)
    plt.title("Tot file(s)")
    if bn != 0:
        plt.plot(buf_tot_data[:,0], buf_tot_data[:,1])
    plt.plot(sam_tot_data[:,0],sam_tot_data[:,1])
    plt.locator_params(nbins=4)
    
    plt.subplot(143)
    plt.title(r"Dat files q<0.2*q$_{max}$")
    #plt.hold(True)
    for ii in dat_list:
        jj = numpy.loadtxt(ii)
        plt.plot(jj[:,0],jj[:,1])
    plt.xlim(0,max(jj[:,0]*.2))
    plt.locator_params(nbins=4)
     
    plt.subplot(144)
    plt.title("Image Rg")
    if bn != 0 and os.path.basename(sample_file).split('_')[-3][0] != 'B':
        plt.errorbar(range(1, len(rg_single)+1), rg_single , yerr= rg_single_del)
        plt.locator_params(nbins=4)
    
    plt.tight_layout(pad=0.2)
    if bn != 0:
        img_gr = os.path.join(other_sfol,'_'.join(os.path.basename(sample_file).split('_')[:-2])+'_'+buffer_file.split('_')[-3]+"_image_graphs.png")
    else:
        img_gr = os.path.join(other_sfol,os.path.splitext(os.path.basename(sample_file))[0]+"_image_graphs.png")
    
    plt.savefig(img_gr)
    
    # make data graphs, only if buffer subtracted
    if bn != 0:
        # load i vs q data
        dd = numpy.loadtxt(sub_file,dtype=numpy.float32)
        
        plt.figure(num=None, figsize=(12,4))
        
        if max(dd[:,1])>0.00001:
            plt.subplot(131)
            plt.title("log(i) vs. q")
            plt.locator_params(numticks=3)
            plt.semilogy(dd[:,0],dd[:,1])
            #plt.grid(True)
            
            
            plt.subplot(132)
            plt.title("log(i) vs. log(q)")
            plt.loglog(dd[:,0],dd[:,1])
            plt.xlim(dd[numpy.nonzero(dd[:,1])[0][0],0]*0.7,max(dd[:,0])*1.3)
            #plt.grid(True)
            plt.locator_params(numticks=5)
            
        else:
            plt.subplot(131)
            plt.title("i vs. q")
            plt.plot(dd[:,0],dd[:,1])
            plt.locator_params(nbins=4)
             
            plt.subplot(132)
            plt.title("i vs. log(q)")
            plt.semilogx(dd[:,0],dd[:,1])
            plt.locator_params(numticks=4)
        
        
        
        plt.subplot(133)
        plt.title("Kratky")
        plt.plot(dd[:,0],dd[:,1]*(dd[:,0]**2))
        plt.ylim(min(dd[:int(0.9*len(dd)),1]*dd[:int(0.9*len(dd)),0]**2),1.1*max(dd[:int(0.9*len(dd)),1]*dd[:int(0.9*len(dd)),0]**2))
        #plt.grid(True)
        plt.locator_params(nbins=4)
         
        plt.tight_layout(pad=0.2)
        if bn != 0:
            dat_gr = os.path.join(other_sfol,'_'.join(os.path.basename(sample_file).split('_')[:-2])+'_'+buffer_file.split('_')[-3]+"_data_graphs.png")
        else:
            dat_gr = os.path.join(other_sfol,os.path.splitext(os.path.basename(sample_file))[0]+"_data_graphs.png")
        
        plt.savefig(dat_gr)
        
        
        # make bokeh html to link from data graphs
        
        # copy css and js files to be used localy
       
        for aa in bokeh.settings.settings.js_files():
            if os.path.basename(aa) == 'bokeh.min.js':
                b_js = aa
        
        for aa in bokeh.settings.settings.css_files():
            if os.path.basename(aa) == 'bokeh.min.css':
                b_css = aa
                
        # check if any of the files are missing; if both are present -> don't copy
        if not (os.path.isfile(os.path.join(other_sfol,'bokeh.min.js')) and os.path.isfile(os.path.join(other_sfol,'bokeh.min.css'))):
            shutil.copy(b_js,other_sfol)
            shutil.copy(b_css,other_sfol) 
            
        source = ColumnDataSource(data=dict(q=dd[:,0], i = dd[:,1], kr=dd[:,1]*(dd[:,0]**2)))
        
        if max(dd[:,1])>0.00001:
            p1 = figure(title="log(i) vs q",toolbar_location="above",y_axis_type="log",toolbar_sticky=False,tools="pan,box_zoom,zoom_in,zoom_out,wheel_zoom,box_select,undo,reset")
            p2 = figure(title="log(i) vs log(q)",toolbar_location="above",y_axis_type="log",x_axis_type="log",toolbar_sticky=False,tools="pan,box_zoom,zoom_in,zoom_out,wheel_zoom,box_select,undo,reset")
        
        else:
            p1 = figure(title="i vs q",toolbar_location="above",toolbar_sticky=False,tools="pan,box_zoom,zoom_in,zoom_out,wheel_zoom,box_select,undo,reset")
            p2 = figure(title="i vs log(q)",toolbar_location="above",toolbar_sticky=False,tools="pan,box_zoom,zoom_in,zoom_out,wheel_zoom,box_select,undo,reset")
        
        p1.circle('q','i',source=source,legend='scatter',size=5,color="navy")
        p1.line('q', 'i',source=source,legend='line', line_width=2, color="navy" )
        p1.add_tools(HoverTool(show_arrow=False,  tooltips=[('q', '@q'),('i', '@i')]))    
        p1.legend.click_policy="hide"
        
        p2.circle('q','i',source=source,legend='scatter',size=5,color="red")
        p2.line('q','i',source=source,legend='line',line_width=2, color="red")
        p2.add_tools(HoverTool(show_arrow=False,  tooltips=[('q', '@q'),('i', '@i')]))  
        p2.x_range = Range1d(dd[numpy.nonzero(dd[:,1])[0][0],0]*0.7,max(dd[:,0])*1.3)
        p2.legend.click_policy="hide"
        
        p3 = figure(title="Kratky",toolbar_location="above",toolbar_sticky=False,tools="pan,box_zoom,zoom_in,zoom_out,wheel_zoom,box_select,undo,reset")
        p3.circle('q','kr',source=source,legend='scatter',size=5,color="green")
        p3.line('q','kr',source=source,legend='line',line_width=2,color="green")
        p3.add_tools(HoverTool(show_arrow=False,  tooltips=[('q', '@q'),('iq^2', '@kr')]))
        p3.y_range = Range1d(min(dd[:int(0.9*len(dd)),1]*dd[:int(0.9*len(dd)),0]**2),1.1*max(dd[:int(0.9*len(dd)),1]*dd[:int(0.9*len(dd)),0]**2))
        p3.legend.click_policy="hide"
        
        dat_gr_html = dat_gr[:-3]+'html'
        output_file(dat_gr_html, title="Data graphs", mode='absolute')
        save(row(p1,p2,p3))
        # edit the html file to pont to local css and js files
        for line in fileinput.input(dat_gr_html, inplace=1):
            line = re.sub(b_js,os.path.join('.',os.path.basename(b_js)), line.rstrip())
            line = re.sub(b_css,os.path.join('.',os.path.basename(b_css)), line.rstrip())
            print(line)
        
        # make data analysis graphs, only if buffer subtracted samples (previous IF)
        # and if the sample is not designated as buffer
        if os.path.basename(sample_file).split('_')[-3][0] != 'B' and autorg != None and datgnom != None:
            plt.figure(num=None, figsize=(8,4))
            
            
            plt.subplot(121)
            if rg_sub[0] == 0:
                plt.title('No good Rg')
                plt.plot([0],[0])
                plt.locator_params(nbins=4)
            else:
                plt.title('Guinier plot')
                plt.plot(dd[rg_start:rg_end+1,0]**2,numpy.log10(dd[rg_start:rg_end+1,1]),'bo',markersize=10)
                plt.figtext(0.25,0.7,'Rg: {} \nI$_0$: {}'.format(rg_sub[0],i0_sub[0]), fontsize=20)
                #plt.plot(dd[rg_start:rg_end+1,0],dd[rg_start:rg_end+1,1])
                try:
                    coefs = numpy.lib.polyfit(dd[rg_start:rg_end+1,0]**2, numpy.log10(dd[rg_start:rg_end+1,1]), 1) 
                    fit_y = numpy.lib.polyval(coefs, dd[rg_start:rg_end+1,0]**2)
                    plt.plot(dd[rg_start:rg_end+1,0]**2, fit_y, 'r--',linewidth=3.0)
                except:
                    print('problem plotting coef')
                
                plt.ylim(0.99*min(numpy.log10(dd[rg_start:rg_end+1,1])),1.01*max(numpy.log10(dd[rg_start:rg_end+1,1])))
                plt.xlim(0.9*dd[rg_start,0]**2,1.1*dd[rg_end,0]**2)
                plt.locator_params(nbins=4)
                
                
            plt.subplot(122)
            if gnom_rg_sub == 0:
                plt.title('No good P(r)')
                plt.plot([0],[0])
                plt.locator_params(nbins=4)
                
            else:
                # load Pr from datgnom .out file
                
                # prx=[]
                # pry=[]
                # pr_data = False
                # for line in  fileinput.input(os.path.join(sastool_sfol,os.path.splitext(os.path.basename(sample_file))[0]+'_sub.out')):
                #     if 'Distance distribution' in line:
                #         pr_data = True
                #     if pr_data == True:
                #         try:
                #             prx.append(float(line.split()[0]))
                #             pry.append(float(line.split()[1]))
                #         except:
                #             pass
                
                regexp = r"(?<!\d)  (\d\.\d+E[+|-]\d+)  (\d\.\d+E[+|-]\d+)\s\s(\d\.\d+E[+|-]\d+)\n"
                pr_xy = numpy.fromregex(os.path.join(sastool_sfol,'_'.join(os.path.basename(sample_file).split('_')[:-2])+'_'+buffer_file.split('_')[-3]+'_sub.out'),regexp,[('prx', numpy.float32),('pry', numpy.float32),('errpry', numpy.float32)])
                        
                
                
                plt.title('P(r) distribution')
                plt.plot(pr_xy['prx'],pr_xy['pry'])
                plt.locator_params(nbins=4)
                
                # export log(i) vs q^2 and fitting data to txt file
                try:
                    exp_rg_txt = [dd[:3*rg_end,0]**2,numpy.log(dd[:3*rg_end,1]),numpy.lib.polyval(coefs, dd[:3*rg_end,0]**2)]
                    numpy.savetxt(os.path.join(other_sfol,os.path.splitext(os.path.basename(sample_file))[0]+'_'+buffer_file.split('_')[-3]+"_guiner.txt"),numpy.transpose(exp_rg_txt),header='q^2\t\tln(i)\t\tlin_fit')
                except:
                    try:
                        exp_rg_txt = [dd[:,0]**2,numpy.log(dd[:,1])]
                        numpy.savetxt(os.path.join(other_sfol,os.path.splitext(os.path.basename(sample_file))[0]+'_'+buffer_file.split('_')[-3]+"_guiner.txt"),numpy.transpose(exp_rg_txt),header='q^2\t\tln(i)')
                    except:
                        exp_rg_txt = [dd[:,0]**2,numpy.log(dd[:,1])]
                        numpy.savetxt(os.path.join(other_sfol,os.path.splitext(os.path.basename(sample_file))[0]+"_guiner.txt"),numpy.transpose(exp_rg_txt),header='q^2\t\tln(i)')
                
                #print(exp_rg_txt)
            
            if bn != 0:
                dat_an_gr = os.path.join(other_sfol,'_'.join(os.path.basename(sample_file).split('_')[:-2])+'_'+buffer_file.split('_')[-3]+"_data_analysis_graphs.png")
            else:
                dat_an_gr = os.path.join(other_sfol,os.path.splitext(os.path.basename(sample_file))[0]+"_data_analysis_graphs.png")
            plt.tight_layout(pad=0.2)
            plt.savefig(dat_an_gr)
                
            
            # make row analysis graphs
            
            # rg and i0 data are sorted from newest to oldest, reverse them
            rg_sub.reverse()
            rg_sub_del.reverse()
            i0_sub.reverse()
            i0_sub_del.reverse()
            connected_samples_list.reverse()
            
            plt.figure(num=None, figsize=(12,4))
            
            plt.subplot(131)
            plt.title('sub(n)/sub(1)')
            if connected_samples_list:
                first_sam = os.path.join(subfiles_sfol,'_'.join(connected_samples_list[0][0].split('_')[:-2])+'_'+connected_samples_list[0][1].split('_')[-3]+'_sub.dat')
                first_sam_data = numpy.loadtxt(first_sam,dtype=numpy.float32)
                row_img_num = connected_samples_list[0][0].split('_')[-4][:2]
                fs_len=len(first_sam_data)-1
                y_max_t = []
                y_min_t = []
                for ooo in connected_samples_list:
                    con_sub_file=os.path.join(subfiles_sfol,'_'.join(ooo[0].split('_')[:-2])+'_'+ooo[1].split('_')[-3]+'_sub.dat')
                    hh = numpy.loadtxt(con_sub_file,dtype=numpy.float32)
                    hh_len=len(hh)-1
                    min_len=min(fs_len,hh_len)
                    hhd = hh[:min_len,1]/first_sam_data[:min_len,1]
                    hhe = hhd[numpy.isfinite(hhd)]
                    y_max_t.append(nlargest(min(25,int(len(hhe)/4)),hhe)[-1])
                    y_min_t.append(nsmallest(min(25,int(len(hhe)/4)),hhe)[-1])
                    plt.plot(hh[:min_len,0],hhd)
                dd_len=len(dd)-1
                min_len=min(dd_len,fs_len)    
                hhd = dd[:min_len,1]/first_sam_data[:min_len,1]
                hhe = hhd[numpy.isfinite(hhd)]
                y_max_t.append(nlargest(min(25,int(len(hhe)/4)),hhe)[-1])
                y_min_t.append(nsmallest(min(25,int(len(hhe)/4)),hhe)[-1])
                plt.plot(dd[:min_len,0],hhd)
                plt.ylim(0.97*min(y_min_t),1.03*max(y_max_t))
                
            else:
                plt.plot(dd[:,0],numpy.ones(len(dd[:,0])))
                row_img_num = sample_file.split('_')[-4][:2]
            plt.subplot(132)
            plt.title('Rg')
            plt.errorbar(numpy.arange(1,len(rg_sub)+1),rg_sub,yerr=rg_sub_del)
            plt.xlim(0,len(rg_sub)+1)
            plt.xticks(numpy.arange(0,len(rg_sub)+1,1))
            plt.ylim(0.97*(min(rg_sub)-max(rg_sub_del)),1.03*(max(rg_sub)+max(rg_sub_del)))
            
            
            plt.subplot(133)
            plt.title('I0')
            plt.errorbar(numpy.arange(1,len(i0_sub)+1),i0_sub,yerr=i0_sub_del)
            plt.xlim(0,len(i0_sub)+1)
            plt.xticks(numpy.arange(0,len(i0_sub)+1,1))
            plt.ylim(0.97*(min(i0_sub)-max(i0_sub_del)),1.03*(max(i0_sub)+max(i0_sub_del)))
            
            
            '_'.join(os.path.basename(sample_file).split('_')[:-2])+'_'+buffer_file.split('_')[-3]
            if bn != 0:
                row_an_gr = os.path.join(other_sfol,"_".join(os.path.basename(sample_file).split('_')[:-4])+'_'+row_img_num+'_'+buffer_file.split('_')[-3]+"_row_analysis_graphs.png")
            else:
                row_an_gr = os.path.join(other_sfol,"_".join(os.path.basename(sample_file).split('_')[:-4])+'_'+row_img_num+"_row_analysis_graphs.png")
            
            plt.tight_layout(pad=0.2)
            plt.savefig(row_an_gr)
            
            # reverse rg and i0 again before making html
            rg_sub.reverse()
            rg_sub_del.reverse()
            i0_sub.reverse()
            i0_sub_del.reverse()
    
    # if buffer-subtracted buffer
    if bn != 0 and os.path.basename(sample_file).split('_')[-3][0] == 'B':
        plt.figure(num=None, figsize=(4,4))
        plt.title('Buffers (log(i) vs. q)')
        plt.semilogy(buf_tot_data[:,0], buf_tot_data[:,1])
        plt.semilogy(sam_tot_data[:,0],sam_tot_data[:,1])
        row_an_gr_bf = os.path.join(other_sfol,"_".join(os.path.basename(sample_file).split('_')[:-4])+'_'+buffer_file.split('_')[-3]+"_row_analysis_graphs_buffer.png")
        plt.savefig(row_an_gr_bf)
    
    #
    ## make html
    #
    # get/set JQuery 
    if not os.path.isfile(os.path.join(other_sfol,'jquery.min.js')):
        try:
            urllib.urlretrieve("https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js",os.path.join(other_sfol,'jquery.min.js'))
            #jquery_line = "<script src=\"{}\"></script>".format(os.path.join(other_sfol,'jquery.min.js'))
            jquery_line =  "<script src=\"./other_files/jquery.min.js\"></script>"
        except:
            jquery_line = "<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>"
    else:
        jquery_line =  "<script src=\"./other_files/jquery.min.js\"></script>"
        
    
    
    # read the existing html file or start with the basic template
    os.chdir(results_fol)
    try:
        html_title = os.getcwd().split('/')[-3]+' SAXS Analysis'
    except:
        html_title = 'SAXS Analysis' 
    sastool_ver = subprocess.check_output(sastool).split(',')[0]
    atsas_ver = subprocess.check_output([autorg,'-v']).split('\n')[0].split(',')[-1]
    if os.path.isfile(res_html):
        if debug == 1:
            print('\nHtml file exists\n')
        html_base = file(res_html).read()
    else:
        html_base = """<!DOCTYPE html>
<html>
    <head>
        <title>{}</title>""".format(html_title)+"""
        <meta charset="UTF-8">
        <style media="screen" type="text/css">
                                
            table a:link {
                    color: #0000aa;
                    text-decoration:none;
                        }
            table a:visited {
                    color: #0000aa;
                    text-decoration:none;
                        }
            table a:active,
                    table a:hover {
                    color: #bd5a35;
                    text-decoration:underline;
                        }
            table tr:hover td {
                    background: #f2f2f2;  
                        }
            table, td, th {
                    border: 1px solid grey;
                    text-align: center;
                    font-family: arial;
                    font-size: 0.9em;
                    }
            td, th {
                    padding: 10px;
                    }
            thead {
                    font-size: 1.3em;
                    } 
            tfoot td{
                    font-size: 1.3em;
                    text-align: left;
                    }
            .foot_font {
                    font-size: 0.7em;
                }
            .num_font {
                    font-size: 1.6em;
                }
            thead th
                {
                position: sticky;
                top: 21px;
                background-color: #eeffff;
                }
            #divfix {
                top: 0;
                left: 30;
                position: fixed;
                z-index: 30;
                background-color: white;
                height: 21px;
                width: 100%;
                }
            .hidden { display: none }
</style>"""+"""
{}""".format(jquery_line)+"""

<script type="text/javascript">
$(function() {

    $("input:checkbox:not(:checked)").each(function() {
      var checkbox = $(this),
        header = $(checkbox.data("column")),
        table = header.closest("table"),
        index = header.index() + 1, // convert to CSS's 1-based indexing
        selector = "tbody tr td:nth-child(" + index + ")",
        column = table.find(selector).add(header);

      column.toggleClass("hidden");
    });



    $("input:checkbox").on("change", function(e) {
      var checkbox = $(this),
        header = $(checkbox.data("column")),
        table = header.closest("table"),
        index = header.index() + 1, // convert to CSS's 1-based indexing
        selector = "tbody tr td:nth-child(" + index + ")",
        column = table.find(selector).add(header);

      column.toggleClass("hidden");
       
     });  
})

</script>
    </head>
    <body>
<div id="divfix">
<input type="checkbox" data-column="#column-res" checked="checked" />results
<input type="checkbox" data-column="#column-time" checked="checked" />time
<input type="checkbox" data-column="#column-brsr" checked="checked" />tot/rej
<input type="checkbox" data-column="#column-guin" checked="checked" />Guinier
<input type="checkbox" data-column="#column-dgnom" checked="checked" />Datgnom
<input type="checkbox" data-column="#column-imgr"checked="checked" />Image graphs
<input type="checkbox" data-column="#column-dgr" checked="checked" />Data graphs
<input type="checkbox" data-column="#column-danal" checked="checked" />Data analysis
<input type="checkbox" data-column="#column-row" checked="checked" />Row analysis
</div>
        <table>
            <thead>
                <tr>
                    <th id="column-res"> Results </th>
                    <th id="column-time"> Time </th>
                    <th> Sub (or tot) file </th>
                    <th id="column-brsr"> Buffer (reject)<br>Sample (reject) </th>
                    <th id="column-guin"> Guinier R<sub>g</sub>&nbsp;&&nbsp;I<sub>0</sub> </th>
                    <th id="column-dgnom"> Datgnom <br> realspace R<sub>g</sub>&nbsp;&&nbsp;D<sub>max</sub></th>
                    <th id="column-imgr"> Image graphs </th>
                    <th id="column-dgr"> Data graphs </th>
                    <th id="column-danal"> Data analysis graphs </th>
                    <th id="column-row"> Row analysis </th>
                </tr>
            </thead>
            <tbody>
            </tbody>
             </table>"""
    
    # set html_end
           
    html_end = """
            </tbody>
             </table>
             <br>
           This information is meant for a quick data check only.  Please analyse your data carefully by hand!&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='foot_font'>Table generated by Saxspipe using {} and autorg and datgnom from {}</span>
       
    </body>
</html>
        """.format(sastool_ver,atsas_ver)


    # compile a new table row with  current sample
    
    if bn != 0 and os.path.basename(sample_file).split('_')[-3][0] != 'B':
        
        html_row = """
                <tr>
                    <td> <p title="Results">{}</p> </td>
                    <td> <p title="Acquisition time">{}</p></td>
                    <td> <a title="sub file S{}-B{}" href="{}"><span class='num_font'>{}</span></a> </td>
                    <td> <a title="buffer (reject)" href="{}"><span class='num_font'>{}&nbsp;({})</span></a><br><a title="sample (reject)" href="{}"><span class='num_font'>{}&nbsp;({})</span></a> </td>
                    <td> <a title="reciprocal Rg&I0" href="{}"><span class='num_font'>{}&plusmn;{}<br>{}&plusmn;{}</span><br>{}</a></td>
                    <td> <a title="datgnom realspace Rg&Dmax" href="{}"><span class='num_font'>{:.1f}<br>{:.1f}</span></a> </td>
                    <td> <a title="image graphs" href="{}"><img src='{}' alt='image graphs' height="200"></a> </td>
                    <td> <a title="data graphs" href="{}"><img src='{}' alt='data graphs' height="200"></a> </td>
                    <td> <a title="data analysis graphs" href="{}"><img src='{}' alt='analysis graphs' height="200"></a> </td>
                    <td> <a title="row analysis" href="{}"><img src='{}' alt='row analysis' height="200"></a>  </td>
                </tr>
        """.format(data_quality,\
        acq_time,\
        str(sn).zfill(3),str(bn).zfill(3),os.path.relpath(sub_file),'_'.join(os.path.basename(sub_file).split('_')[:-3]),\
        os.path.relpath(buffer_log),tot_bf,rej_bf,os.path.relpath(sample_log),tot_sf,rej_sf,\
        os.path.relpath(rg_file_name),rg_sub[0],rg_sub_del[0],i0_sub[0],i0_sub_del[0], rg_range,
        os.path.relpath(dg_file_name),gnom_rg_sub,dmax_sub,\
        os.path.relpath(img_gr),os.path.relpath(img_gr),\
        os.path.relpath(dat_gr_html), os.path.relpath(dat_gr),\
        os.path.relpath(dat_an_gr),os.path.relpath(dat_an_gr),\
        os.path.relpath(row_an_gr),os.path.relpath(row_an_gr)\
        )
        
        
    else:
        
        if bn != 0:
            data_gr_row = "<a title=\"data graphs\" href=\"{0}\"><img src='{1}' height=\"200px\"></a>".format(os.path.relpath(dat_gr_html), os.path.relpath(dat_gr))
            buffer_row = "<a title=\"row analysis\" href=\"{0}\"><img src='{0}' height=\"200px\"></a>".format(os.path.relpath(row_an_gr_bf))
        else:
            data_gr_row = 'No data graphs'
            buffer_row = 'No row analysis'
        html_row = """
                <tr align="center">
                    <td> <p title="Results"> -- </p> </td>
                    <td> <p title="Acquisition time">{}</p></td>
                    <td> <a title="tot file" href="{}"><span class='num_font'>{}</span></a> </td>
                    <td> <a title="sample (reject)" href="{}"><span class='num_font'>{}&nbsp;({})</span></a> </td>
                    <td> -- </td>
                    <td> -- </td>
                    <td> <a title="image graphs" href="{}"><img src='{}' alt='image graphs' height="200"></a> </td>
                    <td> {} </td>
                    <td> No data analysis graphs </td>
                    <td> {} </td>
                </tr>
        """.format(acq_time,\
        os.path.relpath(sam_tot),'_'.join(os.path.basename(sam_tot).split('_')[:-3]),\
        os.path.relpath(sample_log),tot_sf,rej_sf,\
        os.path.relpath(img_gr),os.path.relpath(img_gr),\
        data_gr_row,\
        buffer_row\
        )
    
   
    hh=open(res_html,'w')
    hh.write(html_base.split('</tbody>')[0]+html_row+html_end)
    hh.close()
    
    
    # move backup to the sample_list
    if bn != 0 and os.path.basename(sample_file).split('_')[-3][0] != 'B':
        if os.path.isfile(sample_backup):
            shutil.move(sample_backup,sample_list)  

#print("\n\nFinished in {} seconds.\n".format(time.time()-starttime))    
    ## TA-DAAAAA
    
